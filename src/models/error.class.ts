import { errorType } from '../enumerators'

export class Error {
  message: string
  status: errorType

  constructor (message, status = errorType.KO) {
    this.message = message,
    this.status = status
  }
}
