import { Injectable } from '@angular/core'
import { ApiService } from './api.service'
import { Observable } from 'rxjs'
import { Coords, DataPage, Response, Station } from '../models/eloaded-core.models'

@Injectable()
export class StationService {
  getStationDetailOnMap =
    (position: Coords, stationId: string): Promise<Response<Station>> =>
      this._api.getStationDetailOnMap(position, stationId)
      .toPromise<Response<Station>>()

  getStationDetail =
    (stationId: string): Promise<Response<Station>> =>
      this._api.getStationDetail(stationId)
      .toPromise<Response<Station>>()

  getStations =
    (ne: Coords, sw: Coords, filtered: string, remoteOnly: boolean, page: number): Promise<Response<DataPage<Station>>> =>
      this._api.getStationStart(ne, sw, filtered, remoteOnly, page)
      .toPromise<Response<DataPage<Station>>>()

  constructor (public _api: ApiService) {}

  getRoute (origin, destination, energy, fast): Observable<any> {
    return this._api.getRoute(origin, destination, energy, fast)
  }

  calculateRoute (form): Observable<any> {
    return this._api.calculateRoute(form)
  }

  getAQIs (ne, sw): Observable<any> {
    return this._api.getAQIOnMap(ne, sw)
  }

  weatherPoint (point, time): Observable<any> {
    return this._api.weatherPoint(point, time)
  }

  checkEvseIdStation(id: string): Observable<any> {
    return this._api.checkEvseIdStation(id)
  }

  startCharge(form): Observable<any> {
    return this._api.startCharge(form)
  }

  stopCharge(_id: string): Observable<any> {
    return this._api.stopCharge(_id)
  }

  getMyTransactions(): Observable<any> {
    return this._api.getMyTransactions()
  }

  getChargeDetail(id: string): Observable<any> {
    return this._api.getChargeDetail(id)
  }

  getClosestChargers (position): Observable<any> {
    return this._api.getClosestChargers(position)
  }

  addTrackingPoints (_id: string, points: any[]): Observable<any> {
    return this._api.addTrackingPoints(_id, { points })
  }

  setTrackingStatus (_id: string, status: string): Observable<any> {
    return this._api.setTrackingStatus(_id, { status })
  }
}
