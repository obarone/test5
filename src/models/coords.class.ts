export class Coords {
  lat: number
  lng: number
  accuracy: number
  timestamp: number

  constructor(c: any = {}) {
    this.lat = c.lat
    this.lng = c.lng
    this.accuracy = c.accuracy
    this.timestamp = c.timestamp
  }

  get valid(): boolean {
    return (this.lat !== undefined && this.lng !== undefined)
  }

  get invalid(): boolean {
    return !this.valid
  }

  isEqual (c: Coords): boolean {
    return this.lat === c.lat && this.lng === c.lng &&
      this.accuracy === c.accuracy && this.timestamp === c.timestamp
  }
}

export class Bounds {
  ne: Coords
  sw: Coords

  constructor (b: any) {
    this.ne = new Coords(b.ne)
    this.sw = new Coords(b.sw)
  }
}
