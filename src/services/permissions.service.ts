import { Injectable } from '@angular/core'
import { Diagnostic } from '@ionic-native/diagnostic/ngx'

@Injectable()
export class PermissionsService {
  constructor (private diagnostic: Diagnostic) { }
  hasLocationPermission () {
    this.diagnostic.isLocationAuthorized()
      .then((response) => console.log(`available ${response}`))
      .catch((error) => console.log(`not available ${error}`))
    return true
  }

  async requestLocationPermission () {
    try {
      const res = await this.diagnostic
        .requestLocationAuthorization()
      console.log(res)
    } catch (error) {
      console.log(error)
    }
  }
}
