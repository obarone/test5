import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core'
import { Filters } from '../../models/eloaded-app.models'

@Component({
  selector: 'filters-on-map',
  templateUrl: 'filters-on-map.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class FiltersOnMapComponent {

  @Input() userHasPlugs: boolean
  @Input() userHasFast: boolean
  @Input() filters: Filters
  @Output() filtersChange: EventEmitter<Filters> = new EventEmitter<Filters>()

  @Output() filtersUpdate: EventEmitter<any> = new EventEmitter<any>()

  open = false
  constructor (private cd: ChangeDetectorRef) {}

  public expandFilters () {
    this.open = true
    this.cd.markForCheck()
    this.cd.detectChanges()
  }

  public collapseFilters () {
    this.open = false
    this.cd.detectChanges()
  }

  remoteOnly (ro: boolean) {
    this.filters.remoteOnly = ro
    this.cd.detectChanges()
    this.filtersChange.emit(this.filters)
    this.filtersUpdate.emit({ remoteOnly: true })
  }

  mapStyleUpdate (style: string) {
    this.filters.mapStyle = style
    this.cd.detectChanges()
    this.filtersChange.emit(this.filters)
    this.filtersUpdate.emit({ mapStyle: true })
  }

  filteredUpdate (filtered: string) {
    this.filters.filtered = filtered
    this.cd.detectChanges()
    this.filtersChange.emit(this.filters)
    this.filtersUpdate.emit({ filtered: true })
  }

  trafficUpdate () {
    this.filters.traffic = !this.filters.traffic
    this.cd.detectChanges()
    this.filtersChange.emit(this.filters)
    this.filtersUpdate.emit({ traffic: true })
  }

  aqiUpdate () {
    this.filters.aqi = !this.filters.aqi
    this.cd.detectChanges()
    this.filtersChange.emit(this.filters)
    this.filtersUpdate.emit({ aqi: true })
  }
}
