import { Injectable } from '@angular/core'
import { ApiService } from './api.service'
import { Cart, Order, EloadedTransaction, Gun, Response } from '../models/eloaded-core.models'

@Injectable()
export class EchargeService {
  requestEchargeOrder = (evseid, targetSoC, current, soc: string, capacity): Promise<Response<Order>> => this._api
    .requestEchargeOrder({ evseid, targetSoC, current, soc: parseInt(soc, 10), capacity })
    .toPromise<Response<Order>>()

  requestEchargeOrderPreviews = (evseid: string, targetSoC: number, time?: number): Promise<Response<Cart>> => this._api
    .requestEchargeOrderPreviews({ evseid, targetSoC, time })
    .toPromise<Response<Cart>>()

  getEloadedChargeDetails = (_id: string): Promise<Response<any>> =>  this._api
    .getEloadedChargeDetail(_id)
    .toPromise<Response<any>>()

  stopEloadedCharge = (_id: string): Promise<Response<undefined>> => this._api
    .stopEloadedCharge(_id)
    .toPromise<Response<undefined>>()

  getEloadedTransactionHistory = (user: string): Promise<Response<EloadedTransaction[]>> => this._api
    .getEloadedTransactionHistory({ user })
    .toPromise<Response<EloadedTransaction[]>>()

  getStationGuns = (stationId: string): Promise<Response<Gun[]>> => this._api
    .getStationGuns(stationId)
    .toPromise<Response<Gun[]>>()

  constructor (private _api: ApiService) {}
}
