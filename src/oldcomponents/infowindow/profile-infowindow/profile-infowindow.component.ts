import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef } from '@angular/core'
import { EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Camera, CameraOptions } from '@ionic-native/camera/ngx'
import { StorageService } from '../../../services/storage.service'
import { ValidationRegex } from '../../../enumerators'
import { AlertService } from '../../../services/alert.service'
import { Address, User } from '../../../models/eloaded-core.models'
import { Infowindow, InfowindowTab } from '../../../models/eloaded-app.models'
import { InfowindowTabsHeaderComponent } from '../infowindow-tabs-header/infowindow-tabs-header.component'
import QRCode from 'qrcode'
import { AccountService } from '../../../services/account.service'

@Component({
  selector: 'profile-infowindow',
  templateUrl: 'profile-infowindow.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class ProfileInfowindowComponent extends Infowindow<User> implements OnChanges {
  @Input() user: User
  @Output() userChange: EventEmitter<User> = new EventEmitter<User>()

  @ViewChild(InfowindowTabsHeaderComponent) infowindowTabsHeader: InfowindowTabsHeaderComponent
  @ViewChild('qrCodeTab') qrCodeTab: ElementRef
  @ViewChild('profileTab') profileTab: ElementRef
  @ViewChild('changePasswordTab') changePasswordTab: ElementRef
  @ViewChild('confirmProfileTab') confirmProfileTab: ElementRef

  @Output() close: EventEmitter<any> = new EventEmitter<any>()
  @Output() logout: EventEmitter<any> = new EventEmitter<any>()
  @Output() changePassword: EventEmitter<any> = new EventEmitter()
  @Output() confirmProfile: EventEmitter<any> = new EventEmitter()
  @Output() newConfirmationCode: EventEmitter<any> = new EventEmitter()
  @Output() save: EventEmitter<any> = new EventEmitter<any>()

  tabList = [
    new InfowindowTab({ name: 'qrCode', condition: () => !!this.user, height: { min: 100, max: 300 } }),
    new InfowindowTab({ name: 'profile', condition: () => !!this.user, height: { min: 100, max: 300 } }),
    new InfowindowTab({
      name: 'changePassword',
      condition: () => this.showChangePasswordTab,
      onLeave: () => {
        this.changePasswordEditor.reset()
        this.showChangePasswordTab = false
      },
      height: { min: 100, max: 300 } }),
    new InfowindowTab({
      name: 'confirmProfile',
      condition: () => this.showConfirmProfileTab,
      onLeave: () => {
        this.confirmProfileEditor.reset()
        this.showConfirmProfileTab = false
      },
      height: { min: 100, max: 300 } })
  ]

  showChangePasswordTab = false
  showConfirmProfileTab = false

  editor: FormGroup = new FormGroup({
    firstname: new FormControl(undefined, Validators.compose([Validators.required, Validators.pattern(ValidationRegex.NAME)])),
    lastname: new FormControl(undefined, Validators.compose([Validators.required, Validators.pattern(ValidationRegex.NAME)])),
    authn: new FormGroup({
      email: new FormControl('', Validators.compose([Validators.required, Validators.pattern(ValidationRegex.EMAIL)])),
      _id: new FormControl('')
    }),
    birthday: new FormControl(''),
    genre: new FormControl(),
    address: new FormGroup({
      coords: new FormGroup({
        lat: new FormControl(),
        lng: new FormControl()
      }),
      country: new FormControl(''),
      city: new FormControl(''),
      zip: new FormControl(''),
      street: new FormControl(''),
      number: new FormControl(''),
      other: new FormControl('')
    }),
    newsletter: new FormControl(true),
    avatar: new FormControl(''),
    locale: new FormControl(''),
    automaticPayment: new FormControl(false),
    deleteAvatar: new FormControl(undefined)
  })

  changePasswordEditor: FormGroup = new FormGroup({
    oldPassword: new FormControl('', Validators.required),
    newPassword: new FormControl('', [Validators.required, Validators.minLength(5)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(5)])
  }, [this.checkPasswords, this.samePassword])

  confirmProfileEditor: FormGroup = new FormGroup({
    confirmationCode: new FormControl(undefined, Validators.required)
  })

  constructor (
    public _alert: AlertService,
    private _camera: Camera,
    public _storage: StorageService,
    public _account: AccountService,
    public cd: ChangeDetectorRef,
    public ref: ElementRef
    ) {
    super(cd, ref)
    this.editor.disable()
  }

  get bodyHeight (): { min: number, max: number } {
    const height = this.tabList[this.currentTab]
      ? { ...this.tabList[this.currentTab].height }
      : { min: 50, max: 50 }

    height.max = this[`${this.tabList[this.currentTab].name}Tab`] &&
      this[`${this.tabList[this.currentTab].name}Tab`].nativeElement
      ? this[`${this.tabList[this.currentTab].name}Tab`].nativeElement.offsetHeight
      : height.max

    if (height.max < height.min) { height.min = height.max }
    return height
  }

  get height (): { min: number, max: number } {
    const height = this.bodyHeight
    const header = this.infowindowTabsHeader
      ? this.infowindowTabsHeader.shown ? this.infowindowTabsHeader.height : 0
      : 0
    return { min: height.min + header, max: height.max + header }
  }

  get defaultAvatar (): string {
    if (this.user && this.user.genre) {
      return `./assets/img/default_avatars/avatar_${this.user.genre}.png`
    }
    return './assets/img/default_avatars/avatar_M.png'
  }

  get currentAvatar (): string {
    const { avatar } = this.editor.value
    return avatar ? `data:image/jpeg;base64,${avatar}` : undefined
  }

  get currentAddress (): string {
    const { address } = this.editor.value
    if (!address.coords.lat || !address.coords.lng) {
      return undefined
    }
    const addr = new Address(address)
    return addr.address
  }

  checkPasswords (group: FormGroup) {
    const pass = group.get('newPassword').value
    const confirmPass = group.get('confirmPassword').value

    return pass === confirmPass ? null : { passwordMismatch: true }
  }

  samePassword (group: FormGroup) {
    const pass = group.get('newPassword').value
    const confirmPass = group.get('oldPassword').value

    return pass !== confirmPass ? null : { samePassword: true }
  }

  removeAddress () {
    this.editor.get('address').reset()
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.user) {
      this.focus = changes.user.currentValue
      const currentUser = changes.user.currentValue
      if (currentUser.birthday) {
        const bday = new Date(currentUser.birthday)
        currentUser.birthday = `${bday.getFullYear()}-${bday.getMonth() + 1}-${bday.getDate()}`
      }
      this.editor.patchValue(currentUser)
      if (currentUser) {
        this.loadQrCode()
      }
    }
  }

  async loadQrCode () {
    try {
      const results = await this._account.getQr()
      if (results.message === 'OK') {
        QRCode.toCanvas(
          document.getElementById('qrcode'),
          results.data.qr,
          { width: window.innerWidth / 100 * 60 }
        )
      }
    } catch (error) {
      console.log('handle error', error)
    }
  }

  get maxBirthdate (): string {
    const date = new Date()
    return `${date.getFullYear() - 18}-${date.getMonth() + 1}-${date.getUTCDate()}`
  }

  setAddress (address) {
    this.editor.patchValue({ address: address.location })
    this.cd.detectChanges()
  }

  public editAvatar () {
    const buttons: Array<any> = [
      {
        text: 'ALERT.BUTTON.TAKE_PHOTO',
        handler: () => this.takePhoto()
      }, {
        text: 'ALERT.BUTTON.FROM_GALLERY',
        handler: () => this.takeFromGallery()
      }]
    if (this.editor.value.avatar) {
      buttons.push({
        text: 'ALERT.BUTTON.DELETE_AVATAR',
        handler: () => this.deleteAvatar()
      })
    }
    buttons.push({
      text: 'ALERT.BUTTON.CANCEL',
      role: 'cancel'
    })

    this._alert.createAlert(
      'ALERT.EDIT_AVATAR.TITLE',
      'ALERT.EDIT_AVATAR.MESSAGE',
      [],
      buttons
    )
  }

  private async takePhoto () {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this._camera.DestinationType.DATA_URL,
      encodingType: this._camera.EncodingType.JPEG,
      mediaType: this._camera.MediaType.PICTURE,
      correctOrientation: true
    }
    const image = await this._camera.getPicture(options)
    const base64Image = 'data:image/jpeg;base64,' + image
    this.updateAvatar(image)
  }

  private async takeFromGallery () {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this._camera.DestinationType.DATA_URL,
      encodingType: this._camera.EncodingType.JPEG,
      mediaType: this._camera.MediaType.PICTURE,
      correctOrientation: true
    }

    const image = await this._camera.getPicture({
      ...options,
      sourceType: this._camera.PictureSourceType.SAVEDPHOTOALBUM
    })
    const base64Image = 'data:image/jpeg;base64,' + image
    this.updateAvatar(image)
  }

  goToChangePasswordTab () {
    this.showChangePasswordTab = true
    const i = this.tabList.findIndex(({ name }) => name === 'changePassword')
    if (i >= 0) {
      this.jumpTo(i)
    }
  }

  goToConfirmProfileTab () {
    this.showConfirmProfileTab = true
    const i = this.tabList.findIndex(({ name }) => name === 'confirmProfile')
    if (i >= 0) {
      this.jumpTo(i)
    }
  }

  confirmChangePassword () {
    if (this.changePasswordEditor.invalid) {
      return this.markFormGroupTouched(this.changePasswordEditor)
    }
    this.changePassword.emit(this.changePasswordEditor.value)
    this.getTab('changePassword').loading = () => true
    this.cd.detectChanges()
  }

  changePasswordSuccess () {
    this.loadingImage = './assets/img/loading/done.gif'
    this.cd.detectChanges()
    setTimeout(() => {
      this.jumpTo(1)
      this.changePasswordEditor.reset()
      this.getTab('changePassword').loading = () => false
      this.cd.detectChanges()
      this.loadingImage = './assets/img/loading/infowindow.gif'
    }, 1000)
  }

  changePasswordError (errorMessage: string) {
    this.loadingImage = './assets/img/loading/error.gif'
    this.errorMessage = errorMessage
    this.cd.detectChanges()
    setTimeout(() => {
      this.getTab('changePassword').loading = () => false
      this.loadingImage = './assets/img/loading/infowindow.gif'
      this.errorMessage = ''
      this.cd.detectChanges()
    }, 1000)

  }

  submitConfirmProfile () {
    if (this.confirmProfileEditor.invalid) {
      return this.markFormGroupTouched(this.confirmProfileEditor)
    }
    this.confirmProfile.emit(this.confirmProfileEditor.value)
    this.getTab('confirmProfile').loading = () => true
  }

  sendConfirmationCode () {
    this.newConfirmationCode.emit()
    this.confirmProfileEditor.reset()
    this.getTab('confirmProfile').loading = () => true
    this.cd.detectChanges()
  }

  confirmationCodeSuccess () {
    this.loadingImage = './assets/img/loading/done.gif'
    setTimeout(() => {
      this.jumpTo(1)
      this.confirmProfileEditor.reset()
      this.getTab('confirmProfile').loading = () => false
      this.cd.detectChanges()
      this.loadingImage = './assets/img/loading/infowindow.gif'
    }, 1000)
  }

  sendConfirmationMailSuccess () {
    this.loadingImage = './assets/img/loading/done.gif'
    setTimeout(() => {
      this.getTab('confirmProfile').loading = () => false
      this.cd.detectChanges()
      this.loadingImage = './assets/img/loading/infowindow.gif'
    }, 1000)
  }

  confirmationCodeError (errorMessage: string) {
    this.loadingImage = './assets/img/loading/error.gif'
    this.errorMessage = errorMessage
    this.cd.detectChanges()
    setTimeout(() => {
      this.getTab('confirmProfile').loading = () => false
      this.cd.detectChanges()
      this.loadingImage = './assets/img/loading/infowindow.gif'
      this.errorMessage = ''
    }, 1000)
  }

  updateAvatar (avatar: string) {
    this.editor.patchValue({ avatar, deleteAvatar: undefined })
    this.cd.detectChanges()
  }

  deleteAvatar () {
    this.editor.patchValue({ avatar: undefined, deleteAvatar: true })
    this.cd.detectChanges()
  }

  onSave () {
    this.getTab('profile').loading = () => true
    this.editor.disable()
    this.cd.detectChanges()
    const user = this.editor.value
    if (user.birthday) {
      user.birthday = new Date(user.birthday).toISOString()
    }
    // change date format
    this.save.emit(user)
  }

  saveProfileSuccess () {
    this.loadingImage = './assets/img/loading/done.gif'
    setTimeout(() => {
      this.getTab('profile').loading = () => false
      this.cd.detectChanges()
      this.loadingImage = './assets/img/loading/infowindow.gif'
    }, 1000)
    this.cd.detectChanges()
  }

  saveProfileError (errorMessage: string) {
    this.loadingImage = './assets/img/loading/error.gif'
    this.errorMessage = errorMessage
    this.cd.detectChanges()
    setTimeout(() => {
      this.editor.enable()
      this.getTab('profile').loading = () => false
      this.cd.detectChanges()
      this.loadingImage = './assets/img/loading/infowindow.gif'
      this.errorMessage = ''
    }, 1000)
  }

  openEditor () {
    this.editor.enable()
    this.cd.detectChanges()
  }

  closeInfowindow () {
    this.editor.disable()
    this.currentTab = this.defaultTab
    this.close.emit()
  }

  public markFormGroupTouched (formGroup: FormGroup): void {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched()

      if (control.controls) {
        this.markFormGroupTouched(control)
      }
    })
    this.cd.detectChanges()
  }
}
