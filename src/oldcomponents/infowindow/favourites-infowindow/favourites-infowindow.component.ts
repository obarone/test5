import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { FavouriteLocation } from '../../../models/eloaded-core.models'
import { InfowindowTabsHeaderComponent } from '../infowindow-tabs-header/infowindow-tabs-header.component'
import { Infowindow, InfowindowTab } from '../../../models/eloaded-app.models'

@Component({
  selector: 'favourites-infowindow',
  templateUrl: 'favourites-infowindow.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class FavouritesInfowindowComponent extends Infowindow<FavouriteLocation> {
  @Input() favourites: FavouriteLocation[]
  @Output() favouritesChange: EventEmitter<FavouriteLocation[]> = new EventEmitter<FavouriteLocation[]>()

  @Output() save: EventEmitter<FavouriteLocation> = new EventEmitter<FavouriteLocation>()
  @Output() remove: EventEmitter<FavouriteLocation> = new EventEmitter<FavouriteLocation>()
  @Output() close: EventEmitter<any> = new EventEmitter<any>()

  @ViewChild('nameInput') nameInput: ElementRef
  @ViewChild('favouritesList') favouritesListTab: ElementRef
  @ViewChild('favouriteDetail') favouriteDetailTab: ElementRef
  @ViewChild(InfowindowTabsHeaderComponent) infowindowTabsHeader: InfowindowTabsHeaderComponent

  tabList = [
    new InfowindowTab({
      name: 'list',
      condition: () => !!this.favourites.length,
      loading: () => false,
      height: { min: 100, max: 500 }
    }),
    new InfowindowTab({
      name: 'info',
      condition: () => !!this.focus,
      loading: () => false,
      height: { min: 100, max: 500 }
    })
  ]

  editor = new FormGroup({ name: new FormControl('', Validators.required) })

  constructor (public cd: ChangeDetectorRef, public ref: ElementRef) { super(cd, ref) }

  get height (): { min: number, max: number } {
    const height = this.bodyHeight
    const header = this.infowindowTabsHeader
      ? this.infowindowTabsHeader.shown ? this.infowindowTabsHeader.height : 0
      : 0
    return { min: height.min + header, max: height.max + header }
  }

  get bodyHeight (): { min: number, max: number } {
    const height = { min: 50, max: 50 }
    switch (this.tabList[this.currentTab].name) {
      case 'list':
        height.max = this.favouritesListTab && this.favouritesListTab.nativeElement
          ? this.favouritesListTab.nativeElement.offsetHeight : height.min
        break
      case 'info':
        height.max = this.favouriteDetailTab && this.favouriteDetailTab.nativeElement
        ? this.favouriteDetailTab.nativeElement.offsetHeight : height.min
    }
    return height
  }

  public setFocus (favourite: FavouriteLocation) {
    this.focus = favourite
    this.editor.patchValue({ name: this.focus.name })
    this.currentTab = 1
    this.editor.disable()
    this.cd.detectChanges()
  }

  onSave () {
    this.getTab('info').loading = () => true
    this.focus = { ...this.focus, ...this.editor.value }
    this.favouritesChange.emit(this.favourites)
    this.cd.detectChanges()
    this.save.emit(this.focus)
  }

  saveSuccess () {
    this.editor.disable()
    this.jumpTo(0)
    this.editor.reset()
    this.focus = undefined
    this.getTab('info').loading = () => false
    this.cd.detectChanges()
  }

  saveError () {
    this.getTab('info').loading = () => false
    this.cd.detectChanges()
  }

  onRemove () {
    this.getTab('info').loading = () => true
    this.remove.emit(this.focus)
  }

  removeSuccess () {
    if (!this.favourites.length) {
      this.closeInfowindow()
    } else {
      this.jumpTo(0)
      this.editor.reset()
      this.focus = undefined
    }
    this.getTab('info').loading = () => false
  }

  removeError () {
    this.getTab('info').loading = () => false
  }

  public openEditor () {
    this.editor.enable()
    if (this.nameInput) {
      this.nameInput.nativeElement.focus()
    }
    this.cd.detectChanges()
  }

  swipeEvent (event) {
    super.swipeEvent(event)

    if (this.tabList[this.currentTab].name === 'info') {
      // this.setFavouriteLocationIntoInfowindow({ favourite: undefined, setZoom: true})
    } else if (this.tabList[this.currentTab].name === 'list') {
      // this.panToAllFavouriteLocations()
    }
    this.cd.detectChanges()
  }

  abort () {
    this.editor.disable()
    if (!this.focus._id) {
      this.closeInfowindow()
    } else {
      this.editor.patchValue({ name: this.focus.name })
    }
    this.cd.detectChanges()
  }

  closeInfowindow () {
    this.currentTab = this.defaultTab
    this.focus = undefined
    this.editor.reset()
    this.close.emit()
  }
}
