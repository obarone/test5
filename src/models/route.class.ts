import { Coords } from './coords.class'
import { UserCar } from './car.class'
import { Plug } from './plug.class'
import * as moment from 'moment'

export interface TextValueObject<T> {
  text: string
  value: T
}

interface RechargePlug {
  plug: Plug
  speed: number
}

export interface Recharge {
  actual: TextValueObject<number>
  target: TextValueObject<number>
  time: TextValueObject<number>
  plug: RechargePlug
}

export interface PlaceRoute {
  name: String
  coords: Coords
  type: String
  time: Date
  energy: Number

  _id: string
  weather?: any
  fast?: String
  recharge?: Recharge
}

export interface RouteInfo {
  consumption: TextValueObject<number>
  distance: TextValueObject<number>
  duration: TextValueObject<number>
  energy: TextValueObject<number>
}

interface RoutePolyline {
  points: string
}

export class SubRoute {
  copyrights: string
  summary: string
  bounds: any
  from: PlaceRoute
  to: PlaceRoute
  routeInfo: RouteInfo
  overview_polyline: RoutePolyline

  legs: any

  constructor (obj: any) {
    const properties = ['from', 'to', 'bounds', 'routeInfo', 'overview_polyline',
      'copyrights', 'summary', 'legs']

    Object.keys(obj).forEach(k => {
      if (!properties.find(p => p === k)) {
        console.log(k)
      }
    })

    if (properties.some(p => !Object.keys(obj).find(k => k === p))) {
      throw new Error('MISSING_FIELDS')
    }

    this.copyrights = obj.copyrights
    this.summary = obj.summary
    this.from = { ...obj.from, time: moment(obj.from.time) }
    this.to = { ...obj.to, time: moment(obj.to.time) }
    this.bounds = obj.bounds
    this.routeInfo = obj.routeInfo
    this.overview_polyline = obj.overview_polyline
    this.legs = obj.legs
  }
}

export class Route {
  from: PlaceRoute
  to: PlaceRoute
  routeInfo: RouteInfo
  car: UserCar
  routes: SubRoute[]

  elevation: any
  tracking: any

  constructor (obj: any) {
    const properties = ['from', 'to', 'routeInfo', 'car', 'routes']

    Object.keys(obj).forEach(k => {
      if (!properties.find(p => p === k)) {
        console.log(k)
      }
    })

    if (properties.some(p => !Object.keys(obj).find(k => k === p))) {
      throw new Error('MISSING_FIELDS')
    }

    this.from = { ...obj.from, time: moment(obj.from.time) }
    this.to = { ...obj.to, time: moment(obj.to.time) }
    this.routeInfo = obj.routeInfo
    this.routes = obj.routes.map(r => new SubRoute(r))
    this.car = new UserCar(obj.car)
    this.tracking = obj.tracking

    if (obj.elevation) {
      this.elevation = obj.elevation
    }
  }
}

// import { Coords } from './coords.class'
// // if we add geolib we can get the distance between 2 points and calculate inside the class
// // const geolib = require('geolib')
//
// export class Route {
//   elevation: Elevation
// }
//
//
// export class Elevation {
//   points: Array<ElevationPoint>
//   uphill: number
//   downhill: number
//
//   constructor (obj: any) {
//     this.points = (!Array.isArray(obj.points))
//       ? []
//       : obj.points.map(p => new ElevationPoint(p))
//     if (obj.uphill) {
//       this.uphill = obj.uphill
//     }
//     if (obj.downhill) {
//       this.downhill = obj.downhill
//     }
//   }
//
//   get maxAltitude(): number {
//     return Math.max(...this.points.map(point => point.elevation))
//   }
//
//   get minAltitude(): number {
//     return Math.min(...this.points.map(point => point.elevation))
//   }
//   // get downhill(): number {
//   //   return 0
//   // }
//   //
//   // get uphill(): number {
//   //   return 0
//   // }
// }
//
// export class ElevationPoint {
//   elevation: number
//   location: Coords
//   resolution: number
//
//   constructor (obj) {
//     const properties = ['elevation', 'location', 'resolution']
//
//     for (const property in obj) {
//       if (properties.find((p) => p === property)) {
//         this[property] = obj[property]
//       }
//     }
//   }
// }
//
// export class Aqi {
//   aqi: number
//   city: { geo: Coords, name: string }
//   time: Date
//
//   constructor(obj: any) {
//     this.aqi = obj.aqi
//     this.city = {
//       geo: new Coords(obj.city.geo),
//       name: obj.city.name
//     }
//   }
// }
