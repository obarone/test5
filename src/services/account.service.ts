import { Injectable } from '@angular/core'
import { ApiService } from './api.service'
import { FavouriteLocation, UserCar, Response, User } from '../models/eloaded-core.models'

@Injectable()
export class AccountService {
  registration = (form): Promise<any> => this._api
    .userRegistration(form).toPromise<any>()

  forgotPassword = (email): Promise<boolean> => this._api
    .forgotPassword(email).toPromise<boolean>()

  resetPassword = (email: string, token: string, newPassword: string): Promise<boolean> => this._api
    .resetPassword({ email, token, newPassword }).toPromise<boolean>()

  deleteProfile = (): Promise<any> => this._api
    .deleteProfile().toPromise<any>()

  requestParking = (spot): Promise<any> => this._api
    .requestParking(spot).toPromise<any>()

  bookParking = (spot, date): Promise<boolean> => this._api
    .bookParking({ spot, date }).toPromise<boolean>()

  deleteParking = (spot): Promise<any> => this._api
    .deleteParking(spot).toPromise<any>()

  login = (form): Promise<Response<string>> => this._api
    .login(form).toPromise<Response<string>>()

  getProfile = (): Promise<Response<User>> => this._api
    .getProfile().toPromise<Response<User>>()

  updateProfile = (account): Promise<Response<User>> => this._api
    .setProfile(account).toPromise<Response<User>>()

  changePassword = (oldPassword: string, newPassword: string): Promise<Response<any>> => this._api
    .changePassword({ oldPassword, newPassword }).toPromise<Response<any>>()

  resendConfirmCode = (): Promise<Response<any>> => this._api
    .sendCode().toPromise<Response<any>>()

  confirmProfile = (token: string): Promise<Response<any>> => this._api
    .confirmation(token).toPromise<Response<any>>()

  // FavouriteStations routes
  getFavouriteStations = (): Promise<Response<any[]>> => this._api
    .getUserFavouriteStations().toPromise<Response<any[]>>()

  addFavouriteStation = (station: any): Promise<Response<any[]>> => this._api
    .addUserFavouriteStation(station._id).toPromise<Response<any[]>>()

  removeFavouriteStation = (station: any): Promise<Response<any[]>> => this._api
    .removeUserFavouriteStation(station._id).toPromise<Response<any[]>>()

  // Favouritelocations routes
  getFavouriteLocations = (): Promise<Response<FavouriteLocation[]>> => this._api
    .getUserFavouriteLocations().toPromise<Response<FavouriteLocation[]>>()

  addFavouriteLocation = (location: FavouriteLocation): Promise<Response<FavouriteLocation[]>> => this._api
    .addUserFavouriteLocation(location).toPromise<Response<FavouriteLocation[]>>()

  patchFavouriteLocation = (location: FavouriteLocation): Promise<Response<FavouriteLocation[]>> => this._api
    .patchUserFavouriteLocation(location).toPromise<Response<FavouriteLocation[]>>()

  removeFavouriteLocation = (location: FavouriteLocation): Promise<Response<FavouriteLocation[]>> => this._api
    .removeUserFavouriteLocation(location._id).toPromise<Response<FavouriteLocation[]>>()

  // Cars routes
  getCars = (): Promise<Response<UserCar[]>> => this._api
    .getUserCars().toPromise<Response<UserCar[]>>()

  addCar = (car: UserCar): Promise<Response<UserCar[]>> => this._api
    .addUserCar(car.toAddDTO()).toPromise<Response<UserCar[]>>()

  patchCar = (car: UserCar): Promise<Response<UserCar[]>> => this._api
    .patchUserCar(car.toUpdateDTO()).toPromise<Response<UserCar[]>>()

  removeCar = (car: UserCar): Promise<Response<UserCar[]>> => this._api
    .removeUserCar(car._id).toPromise<Response<UserCar[]>>()

  setDefaultCar = (car: UserCar): Promise<Response<UserCar[]>> => this._api
    .setDefaultCar(car._id).toPromise<Response<UserCar[]>>()

  getQr = (): Promise<Response<any>> => this._api
    .getQrCode().toPromise<Response<any>>()

  getShoppingHistory = (): Promise<Response<any>> => this._api
    .getShoppingHistory().toPromise<Response<any>>()

  constructor (public _api: ApiService) {}
}
