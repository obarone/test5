import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { LogService } from './log.service'

@Injectable()
export class ConnectorService {
  constructor (public httpClient: HttpClient, public _logger: LogService) {}

  public httpGet<T> (url: string): Observable<T> {
    this._logger.info(`GET ${url}`)
    return this.httpClient.get<T>(url)
  }

  public httpPost<T> (url: string, form: any): Observable<T> {
    this._logger.info(`POST ${url}`)
    this._logger.debug(form)
    return this.httpClient.post<T>(url, form)
  }

  public httpPut<T> (url: string, form: any): Observable<T> {
    this._logger.info(`PUT ${url}`)
    this._logger.debug(form)
    return this.httpClient.put<T>(url, form)
  }

  public httpPatch<T> (url: string, form: any): Observable<T> {
    this._logger.info(`PATCH ${url}`)
    this._logger.debug(form)
    return this.httpClient.patch<T>(url, form)
  }

  public httpDelete<T> (url: string): Observable<T> {
    this._logger.info(`DELETE ${url}`)
    return this.httpClient.delete<T>(url)
  }
}
