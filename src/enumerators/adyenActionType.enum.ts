export enum AdyenActionType {
  VOUCHER = 'voucher',
  REDIRECT = 'redirect',
  QRCODE = 'qrCode',
  AWAIT = 'await',
  SDK = 'sdk',
  THREEDS2FINGERPRINT = 'threeDS2Fingerprint',
  THREEDS2CHALLENGE = 'threeDS2Challenge'
}
