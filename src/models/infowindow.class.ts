import { ChangeDetectorRef, ElementRef } from '@angular/core'

export class Infowindow<T> {
  tabList: Array<InfowindowTab> = []
  focus: T
  _currentTab = 0
  defaultTab = 0
  autoHide = false
  loadingImage = './assets/img/loading/infowindow.gif'
  errorMessage = ''

  constructor (public cd: ChangeDetectorRef, public ref: ElementRef) {}

  get currentTab (): number {
    return this._currentTab
  }

  get realCurrentTab (): number {
    if (!this.tabList || !this.tabList.length) {
      return
    }
    const realIndex = this.validTabs.findIndex(({ name }) =>
      name === this.tabList[this.currentTab].name)
    return realIndex < 0 ? 0 : realIndex
  }

  set currentTab (i: number) {
    if (this.tabList[this.currentTab] && this.tabList[this.currentTab].onLeave) {
      this.tabList[this.currentTab].onLeave()
    }
    this._currentTab = i
    if (this.tabList[this.currentTab] && this.tabList[this.currentTab].onEnter) {
      this.tabList[this.currentTab].onEnter()
    }
  }

  get validTabs (): Array<any> {
    return this.tabList.filter(({ condition }) => condition())
  }

  get height (): { min: number, max: number } {
    return this.tabList.length && this.tabList[this.currentTab]
      ? this.tabList[this.currentTab].height
      : undefined
  }

  get scroll (): number {
    const page = this.getPageElement()
    return page ? page.scrollTop : undefined
  }

  getTab(tabName: string): InfowindowTab {
    return this.tabList.find(({ name }) => name === tabName)
  }

  private getPageElement (): any {
    let actualRef = this.ref.nativeElement
    while (!actualRef.classList.contains('ion-page') && actualRef) {
      actualRef = actualRef.parentElement
    }
    return actualRef
  }

  swipeEvent(event) {
    if (!this.tabList.length) { return }
    if (!event || !event.deltaX) { return }
    const tabs = this.tabList.filter(({ condition }) => condition())
    if (!tabs || tabs.length <= 1) { return }
    if (event.deltaX > 100) {
      if (this.currentTab > 0) {
        const prev = this.prevTab()
        if (prev === this.currentTab) {
          return
        } else {
          this.currentTab = prev
        }
      } else {
        return
      }
    } else if (event.deltaX < -100) {
      if (this.currentTab + 1 < tabs.length) {
        const next = this.nextTab()
        if (next === this.currentTab) {
          return
        } else {
          this.currentTab = next
        }
      } else {
        return
      }
    }
    this.cd.detectChanges()
  }

  prevTab (): number {
    let found = -1
    let i = this.currentTab - 1

    while (found < 0 && i >= 0) {
      if (this.tabList[i].condition()) {
        found = i
      } else {
        i--
      }
    }
    return found
  }

  nextTab (): number {
    let found = -1
    let i = this.currentTab + 1

    while (found < 0 && i < this.tabList.length) {
      if (this.tabList[i].condition()) {
        found = i
      } else {
        i++
      }
    }
    return found
  }

  jumpTo (i: number) {
    if (this.currentTab === i) {
      this.cd.detectChanges()
      return
    }
    this.currentTab = i
    this.cd.detectChanges()
  }

  closeInfowindow() {
    this.currentTab = this.defaultTab
    this.focus = undefined
    this.cd.detectChanges()
  }
}

export class InfowindowTab {
  name: string
  default: boolean
  current: boolean
  height: { min: number, max: number }
  condition: () => boolean = () => true
  loading: () => boolean = () => false
  onEnter: () => void = () => undefined
  onLeave: () => void = () => undefined

  constructor (obj: any) {
    if (!obj) {
      throw new Error('Infowindow tab not created')
    }
    if (obj.name) { this.name = obj.name }
    if (obj.condition) { this.condition = obj.condition }
    if (obj.loading) { this.loading = obj.loading }
    if (obj.onEnter) { this.onEnter = obj.onEnter }
    if (obj.onLeave) { this.onLeave = obj.onLeave }
    if (obj.height) { this.height = obj.height }
    if (obj.default !== undefined) { this.default = obj.default }
    if (obj.current !== undefined) { this.current = obj.current }
  }
}
