import { LogLevel } from '../enumerators'

export class LogEntry {
  entryDate: Date = new Date()
  message = ''
  level: LogLevel = LogLevel.ALL
  extraInfo: any
  logWithDate = true

  buildLogString (): Object {
    let value = {}

    if (this.logWithDate) {
      value = { date: new Date() }
    }
    value ['type'] = LogLevel[this.level]
    value ['message'] = this.message
    if (this.extraInfo.length === 1) {
      value ['info'] = this.extraInfo[0]
        // = this.formatParams(this.extraInfo)
    } else if (this.extraInfo.length) {
      value ['info'] = this.extraInfo[0]
    }
    return value
  }

  private formatParams (params: any[]): string {
    let ret: string = params.join(',')

    if (params.some(p => typeof p === 'object')) {
      ret = ''
      for (const item of params) {
        ret += JSON.stringify(item) + ','
      }
    }

    return ret
  }
}
