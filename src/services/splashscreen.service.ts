import { SplashScreen } from '@ionic-native/splash-screen/ngx'
import { Injectable } from '@angular/core'
import { Events } from './events.service'
import { Platform } from '@ionic/angular'

@Injectable()
export class SplashscreenService {
  constructor (
    private splashscreen: SplashScreen,
    private events: Events,
    private platform: Platform
  ) {
    this.events.subscribe('splashscreen:hide', () => {
      this.platform.ready().then(() => {
        this.splashscreen.hide()
        this.events.publish('app:ready')
      })
    })
  }
}
