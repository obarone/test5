import { Injectable } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { ToastController } from '@ionic/angular'

@Injectable()
export class ToastService {
  activeToast: any
  defaultOptions = {
    duration: 3000,
    position: 'bottom',
    showCloseButton: false,
    closeButtonText: 'ALERT.BUTTON.OK'
  }
  loadingImage: './assets/img/loading/general.gif'
  constructor (
    private toastCtrl: ToastController, private translate: TranslateService
  ) {}

  // position = top || middle || bottom
  // duration = n milliseconds (0 for unlimited time)
  // loader = true if you want a loader; false otherwise
  // closeButtonText
  async toast (message, options?) {
    let unlimited = false
    if (this.activeToast) {
      return
    }

    if (options && options.duration === 0) {
      unlimited = true
    }

    options = (options)
      ? {
        message: this.translate.instant(message),
        duration: (options.duration !== undefined)
          ? options.duration
          : this.defaultOptions.duration,
        position: (options.position)
          ? options.position
          : this.defaultOptions.position,
        showCloseButton: (options.closeButtonText) ? true : false,
        closeButtonText: (options.closeButtonText)
          ? this.translate.instant(options.closeButtonText)
          : this.translate.instant(this.defaultOptions.closeButtonText)
      }
      : { message: this.translate.instant(message), ...this.defaultOptions }

    const toast = await this.toastCtrl
      .create({ ...options, duration: (unlimited) ? undefined : options.duration })

    toast.onDidDismiss().then(() => this.activeToast = null)

    toast.present()
      .then(() => {
        if (options.loader) {
          const toastMessage = document.getElementsByClassName('toast-message')[0]
          toastMessage.innerHTML =
            `<span><img class="toast-loader" src="${this.loadingImage}"/></span>` +
            this.translate.instant(message)
          toastMessage.className += ' toast-message-loader'
        }
      })
    this.activeToast = toast

    return toast
  }

  dismissAll () {
    this.activeToast = null
  }
}
