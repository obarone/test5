import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core'
import { Address, FavouriteLocation, Car, UserCar, Order, Plug, User, EloadedTransaction } from '../../models/eloaded-core.models'
import { CarsInfowindowComponent } from './cars-infowindow/cars-infowindow.component'
import { ChargeInfowindowComponent } from './charge-infowindow/charge-infowindow.component'
import { FavouritesInfowindowComponent } from './favourites-infowindow/favourites-infowindow.component'
import { InfowindowTabsHeaderComponent } from './infowindow-tabs-header/infowindow-tabs-header.component'
import { Infowindow } from '../../models/eloaded-app.models'
import { LocationInfowindowComponent } from './location-infowindow/location-infowindow.component'
import { OrderHistoryInfowindowComponent } from './orderhistory-infowindow/orderhistory-infowindow.component'
import { ProfileInfowindowComponent } from './profile-infowindow/profile-infowindow.component'
import { StationInfowindowComponent } from './station-infowindow/station-infowindow.component'

@Component({
  selector: 'infowindow',
  templateUrl: 'infowindow.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class InfowindowComponent {
  @Input() activeTransaction: string
  @Input() orders: Order[]
  @Input() systemCars: Car[]
  @Input() systemPlugs: Plug[]
  @Input() stateOfCharge: any

  @Input() user: User
  @Output() userChange: EventEmitter<User> = new EventEmitter<User>()

  // Infowindow control
  @Output() close: EventEmitter<any> = new EventEmitter<any>()
  @Output() collapse: EventEmitter<any> = new EventEmitter()
  @Output() toggleKeyboard: EventEmitter<any> = new EventEmitter()

  // Cars events
  @Output() removeCar: EventEmitter<any> = new EventEmitter()
  @Output() saveCar: EventEmitter<any> = new EventEmitter()
  @Output() setDefaultCar: EventEmitter<any> = new EventEmitter()

  // Favourites events
  @Output() removeFavouriteLocation: EventEmitter<any> = new EventEmitter()
  @Output() saveFavouriteLocation: EventEmitter<any> = new EventEmitter()

  // Station events
  @Output() getStationGuns: EventEmitter<any> = new EventEmitter()
  @Output() removeFavouriteStation: EventEmitter<any> = new EventEmitter()
  @Output() saveFavouriteStation: EventEmitter<any> = new EventEmitter()

  // Profile events
  @Output() changePassword: EventEmitter<any> = new EventEmitter()
  @Output() confirmProfile: EventEmitter<any> = new EventEmitter()
  @Output() logout: EventEmitter<any> = new EventEmitter()
  @Output() newConfirmationCode: EventEmitter<any> = new EventEmitter()
  @Output() updateUser: EventEmitter<any> = new EventEmitter()

  // Echarge events
  @Output() getChargeDetail: EventEmitter<any> = new EventEmitter()
  @Output() requestEchargeOrder: EventEmitter<any> = new EventEmitter()
  @Output() payOrder: EventEmitter<any> = new EventEmitter()
  @Output() requestStopCharge: EventEmitter<any> = new EventEmitter()
  @Output() payOrderWebDropIn: EventEmitter<any> = new EventEmitter()
  @Output() payOrderWithTerminal: EventEmitter<any> = new EventEmitter()

  @ViewChild(InfowindowTabsHeaderComponent) infowindowTabsHeader: InfowindowTabsHeaderComponent

  @ViewChild(FavouritesInfowindowComponent) favouriteInfowindowComponent: FavouritesInfowindowComponent
  @ViewChild(LocationInfowindowComponent) locationInfowindowComponent: LocationInfowindowComponent
  @ViewChild(StationInfowindowComponent) stationInfowindowComponent: StationInfowindowComponent
  @ViewChild(CarsInfowindowComponent) carsInfowindowComponent: CarsInfowindowComponent
  @ViewChild(ChargeInfowindowComponent) chargeInfowindowComponent: ChargeInfowindowComponent
  @ViewChild(OrderHistoryInfowindowComponent) orderHistoryInfowindowComponent: OrderHistoryInfowindowComponent
  @ViewChild(ProfileInfowindowComponent) profileInfowindowComponent: ProfileInfowindowComponent

  _type = 'cars'
  constructor (private cd: ChangeDetectorRef) {}

  get activeInfowindow(): Infowindow<any> {
    return this[`${this._type}InfowindowComponent`]
  }

  get tabList(): Array<any> {
    return this.activeInfowindow.tabList
  }

  get currentTab(): number {
    return this.activeInfowindow.currentTab
  }

  get autoHide(): boolean {
    return this.activeInfowindow.autoHide
  }

  set type (iwt: string) {
    if (this.activeInfowindow) {
      this.activeInfowindow.closeInfowindow()
    }
    this._type = iwt
  }

  get type (): string {
    return this._type
  }

  get height (): { min: number, max: number } {
    this.cd.detectChanges()
    return this.activeInfowindow.height
  }

  focusOnLocation (location: Address) {
    this.type = 'location';
    (this.activeInfowindow as LocationInfowindowComponent)
      .setFocus(location)
  }

  focusOnFavourite (favourite: FavouriteLocation) {
    this.type = 'favourite';
    (this.activeInfowindow as FavouritesInfowindowComponent)
      .setFocus(favourite)
  }

  focusOnStation (station: any) {
    this.type = 'station';
    (this.activeInfowindow as StationInfowindowComponent)
      .setFocus(station)
  }

  focusOnCar (car: UserCar) {
    this.type = 'cars';
    (this.activeInfowindow as CarsInfowindowComponent)
      .setFocus(car)
  }

  pushLocationToFavourites (location: Address) {
    this.type = 'favourite'
    this.favouriteInfowindowComponent.setFocus(new FavouriteLocation(location))
    this.favouriteInfowindowComponent.openEditor()
    this.cd.detectChanges()
  }

  pushEvseToCharge (evseid: string) {
    this.type = 'charge';
    (this.activeInfowindow as ChargeInfowindowComponent)
      .setFocus(evseid)
    this.cd.detectChanges()
  }


  closeInfowindow () {
    this._type = 'cars'
    this.close.emit()
  }
}
