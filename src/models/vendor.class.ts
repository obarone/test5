export class Vendor {
  id: string
  name: string
  description: string

  constructor (obj: any = {}) {
    const properties = ['id', 'name', 'description']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
  }

  equals (b: Vendor): boolean {
    return this.id === b.id
  }
}
