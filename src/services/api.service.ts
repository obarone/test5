import { Injectable } from '@angular/core'
import { HttpResponse } from '@angular/common/http'

import { Observable, throwError } from 'rxjs'
import { catchError, map } from 'rxjs/operators'

import { ConnectorService } from './connector.service'

import { Error } from '../models/eloaded-app.models'
import { errorType } from '../enumerators'
import { Coords, AddUserCarDTO, Car, UpdateUserCarDTO, UserCar, DataPage, Cart } from '../models/eloaded-core.models'
import { FavouriteLocation, Response, User, Plug, Order, Station, ActiveTransaction, EloadedTransaction, Gun } from '../models/eloaded-core.models'

@Injectable()
export class ApiService {
  public baseUrl = 'https://gateway.web-dev.eloaded.eu' // develop server
  // public baseUrl = `https://arch-dev.eloaded.eu` // develop server
  // public baseUrl = 'http://localhost:5000' // local server
  // public baseUrl = 'http://192.168.178.24:5000' // dev office server
  accountContext = `/account`
  authnContext = `/authns`
  solarContext = `/solar`
  stationContext = `/station`
  userContext = `/user`
  nameContext = `/mobile`
  echargeContext = `/echarge`
  orderContext = `/order`

  handleError  = (error: any) => throwError(new Error(
      error.status === 0
        ? 'ERROR.STATUS.0'
        : error.error && error.error.message
          ? error.error.message : error.message,
      error.status in errorType ? error.status : errorType.KO
    ))

  forgotPassword = (form): Observable<any> => this._connector
    .httpGet(`${this.baseUrl}${this.authnContext}/password/${form.email}`)
    .pipe(catchError(error => this.handleError(error)))

  resetPassword = (form): Observable<any> => this._connector
    .httpPost(`${this.baseUrl}${this.authnContext}/password`, form)
    .pipe(catchError(error => this.handleError(error)))

  deleteProfile = (): Observable<any> => this._connector
    .httpDelete(`${this.baseUrl}${this.authnContext}${this.userContext}`)
    .pipe(catchError(error => this.handleError(error)))

  userRegistration = (form): Observable<HttpResponse<any>> => this._connector
    .httpPost<HttpResponse<any>>(`${this.baseUrl}${this.authnContext}${this.userContext}`, this.multipart(form))
    .pipe(catchError(error => this.handleError(error)))

  getShoppingHistoryPdf = (): Observable<HttpResponse<any>> => this._connector
    .httpGet<HttpResponse<any>>(`${this.baseUrl}${this.accountContext}/payments/pdf`)
    .pipe(catchError(error => this.handleError(error)))

  getRoute = (origin, destination, energy, fast): Observable<HttpResponse<any>> => this._connector
    .httpGet<HttpResponse<any>>(`${this.baseUrl}${this.stationContext}/route/${origin.lat},${origin.lng}/${destination.lat},${destination.lng}/${energy}/${fast}`)
    .pipe(catchError(error => this.handleError(error)))

  calculateRoute = (form): Observable<HttpResponse<any>> => this._connector
    .httpPost<HttpResponse<any>>(`${this.baseUrl}${this.stationContext}/route/`, form)
    .pipe(catchError(error => this.handleError(error)))

  requestParking = (spot): Observable<HttpResponse<any>> => this._connector
    .httpGet<HttpResponse<any>>(`${this.baseUrl}${this.stationContext}/parking/${spot}`)
    .pipe(catchError(error => this.handleError(error)))

  bookParking = (form): Observable<any> => this._connector
    .httpPost(`${this.baseUrl}${this.stationContext}/parking`, form)
    .pipe(catchError(error => this.handleError(error)))

  deleteParking = (spot): Observable<any> => this._connector
    .httpDelete(`${this.baseUrl}${this.stationContext}/parking/${spot}`)
    .pipe(catchError(error => this.handleError(error)))

  contactUs = (form): Observable<any> => this._connector
    .httpPost(`${this.baseUrl}/mail/contact-us`, { ...form, type: 'contact-us' })
    .pipe(catchError(error => this.handleError(error)))

  solarCharge = (form): Observable<any> => this._connector
    .httpPost(`${this.baseUrl}${this.solarContext}`, form)
    .pipe(catchError(error => this.handleError(error)))

  solarPreview = (form): Observable<any> => this._connector
    .httpPost(`${this.baseUrl}${this.solarContext}/preview`, form)
    .pipe(catchError(error => this.handleError(error)))

  createSolarSite = (site): Observable<any> => this._connector
    .httpPost(`${this.baseUrl}${this.solarContext}/site`, site)
    .pipe(catchError((error) => this.handleError(error)))

  updateSolarSite = (site): Observable<any> => this._connector
    .httpPatch(`${this.baseUrl}${this.solarContext}/site`, site)
    .pipe(catchError((error) => this.handleError(error)))

  searchSolarSite = (site): Observable<any> => this._connector
    .httpPost(`${this.baseUrl}${this.solarContext}/site/search`, site)
    .pipe(catchError((error) => this.handleError(error)))

  getSolarSite = (_id): Observable<any> => this._connector
    .httpGet(`${this.baseUrl}${this.solarContext}/site/${_id}`)
    .pipe(catchError((error) => this.handleError(error)))

  deleteSolarSite = (_id): Observable<any> => this._connector
    .httpDelete(`${this.baseUrl}${this.solarContext}/site/${_id}`)
    .pipe(catchError((error) => this.handleError(error)))

  weatherPoint = (point, time) => this._connector
    .httpGet(`${this.baseUrl}${this.stationContext}/weatherPoint/${point.lat},${point.lng}/${time}`)
    .pipe(catchError(error => this.handleError(error)))

  getAQIOnMap  = (ne, sw) => this._connector
    .httpGet(`${this.baseUrl}${this.stationContext}/aqi/${ne.lat()},${ne.lng()}/${sw.lat()},${sw.lng()}`)
    .pipe(catchError(error => this.handleError(error)))

  checkEvseIdStation = (id) => this._connector
    .httpGet(`${this.baseUrl}${this.echargeContext}/charge/getStationDetails?evse=${id}`)
    .pipe(catchError(error => this.handleError(error)))

  startCharge = (evse) => this._connector
    .httpPost(`${this.baseUrl}${this.echargeContext}/charge`, { evse })
    .pipe(catchError(error => this.handleError(error)))

  stopCharge = (_id) => this._connector
    .httpDelete(`${this.baseUrl}${this.echargeContext}/charge/${_id}`)
    .pipe(catchError(error => this.handleError(error)))

  getMyTransactions = () => this._connector
    .httpGet(`${this.baseUrl}${this.echargeContext}/charge/all`)
    .pipe(catchError(error => this.handleError(error)))

  getChargeDetail = (id) => this._connector
    .httpGet(`${this.baseUrl}${this.echargeContext}/charge/getTransactions?userID=${id}`)
    .pipe(catchError(error => this.handleError(error)))

  getClosestChargers = (position) => this._connector
    .httpGet(`${this.baseUrl}${this.echargeContext}/stations/${position}`)
    .pipe(catchError((error) => this.handleError(error)))

  addTrackingPoints = (_id: string, params): Observable<any> => this._connector
    .httpPatch(`${this.baseUrl}${this.stationContext}/tracking/${_id}`, params)
    .pipe(catchError(error => this.handleError(error)))

  setTrackingStatus = (_id: string, params): Observable<any> => this._connector
    .httpPatch(`${this.baseUrl}${this.stationContext}/tracking/status/${_id}`, params)
    .pipe(catchError(error => this.handleError(error)))

  // REWORKED ROUTES
  createOrder = (createOrderForm: any): Observable<Response<Order>> => this._connector
    .httpPost(`${this.baseUrl}${this.orderContext}/create`, createOrderForm)
    .pipe(map(({ message, data }) => new Response<Order>(message, new Order(data))),
      catchError(error => this.handleError(error)))

  getEloadedChargeDetail = (_id: string): Observable<Response<ActiveTransaction>> => this._connector
    .httpGet<Response<ActiveTransaction>>(`${this.baseUrl}${this.echargeContext}/detail/${_id}`)
    .pipe(map(({ message, data }) => new Response<ActiveTransaction>(message, new ActiveTransaction(data))),
      catchError(error => this.handleError(error)))

  stopEloadedCharge = (_id: string): Observable<Response<undefined>> => this._connector
    .httpGet<Response<undefined>>(`${this.baseUrl}${this.echargeContext}/stop/${_id}`)
    .pipe(map(({ message }) => new Response<undefined>(message, undefined)),
      catchError(error => this.handleError(error)))

  getEloadedTransactionHistory = (form: any): Observable<Response<EloadedTransaction[]>> => this._connector
    .httpPost<Response<EloadedTransaction[]>>(`${this.baseUrl}${this.echargeContext}/transaction/search`, form)
    .pipe(map(({ message, data }) => new Response<EloadedTransaction[]>(
        message, data ? data.map(o => new EloadedTransaction(o)) : [])),
      catchError(error => this.handleError(error)))

  requestEchargeOrder = (form: any): Observable<Response<Order>> => this._connector
    .httpPost<Response<Order>>(`${this.baseUrl}/echarge/order`, form)
    .pipe(map(({ message, data }) => new Response<Order>(message, new Order(data))),
      catchError(error => this.handleError(error)))

  requestEchargeOrderPreviews = (form: any): Observable<Response<Cart>> => this._connector
    .httpPost<Response<Cart>>(`${this.baseUrl}/order/charge`, form)
    .pipe(map(({ message, data }) => new Response<Cart>(message, new Cart(data))),
      catchError(error => this.handleError(error)))

  getOrderHistory = (): Observable<Response<Order[]>> => this._connector
    .httpGet<Response<Order[]>>(`${this.baseUrl}/order/myorders`)
    .pipe(map(({ message, data }) =>
        new Response<Order[]>(message, data ? data.map(o => new Order(o)) : [])),
      catchError(error => this.handleError(error)))

  getAllPlugs = (): Observable<Response<Plug[]>> => this._connector
    .httpGet<Response<Plug[]>>(`${this.baseUrl}/plug/all`)
    .pipe(map(({ message, data }) => new Response<Plug[]>(message, data.map(p => new Plug(p)))),
      catchError(error => this.handleError(error)))

  getAllCars = (): Observable<Response<Car[]>> => this._connector
    .httpGet<Response<Car[]>>(`${this.baseUrl}/car/all`)
    .pipe(map(({ message, data }) => new Response<Car[]>(message, data.map(c => new Car(c)))),
      catchError(error => this.handleError(error)))

  getStationStart = (ne: Coords, sw: Coords, filtered: string, remoteOnly, page): Observable<Response<DataPage<Station>>> => this._connector
    .httpPost<Response<DataPage<any>>>(`${this.baseUrl}${this.stationContext}/map`, { ne, sw, filtered, page, remoteOnly })
    .pipe(map(({ message, data }) =>
        new Response<DataPage<any>>(
          message,
          new DataPage<Station>({
            ...data,
            data: data.data ? data.data.map(d => new Station(d)) : []
          }
        ))),
      catchError(error => this.handleError(error)))

  getStationDetail = (id: string): Observable<Response<Station>> => this._connector
    .httpGet<Response<Station>>(`${this.baseUrl}${this.stationContext}/${id}`)
    .pipe(map(({ message, data }) => new Response<Station>(message, new Station(data))),
      catchError(error => this.handleError(error)))

  getStationDetailOnMap = (position: Coords, id: string): Observable<Response<Station>>  => this._connector
    .httpGet<Response<Station>>(`${this.baseUrl}${this.stationContext}/detail/${position.lat},${position.lng}/${id}`)
    .pipe(map(({ message, data }) => new Response<Station>(message, new Station(data))),
      catchError(error => this.handleError(error)))

  getStationGuns = (stationId: string): Observable<Response<Gun[]>> => this._connector
    .httpGet<Response<Gun[]>>(`${this.baseUrl}${this.echargeContext}/guns/${stationId}`)
    .pipe(map(({ message, data }) =>
        new Response<Gun[]>(message, data ? data.map(g => new Gun(g)) : [])),
      catchError(error => this.handleError(error)))

  login = (form): Observable<Response<string>> => this._connector
    .httpPut<Response<string>>(`${this.baseUrl}${this.authnContext}`, form)
    .pipe(map(({ message, data }) => new Response<string>(message, data)),
      catchError(error => this.handleError(error)))

  getProfile = (): Observable<Response<User>> => this._connector
    .httpGet<Response<User>>(`${this.baseUrl}${this.accountContext}${this.userContext}`)
    .pipe(map(({ message, data }) => new Response<User>(message, new User(data))),
      catchError(error => this.handleError(error)))

  setProfile = (form): Observable<Response<User>> => this._connector
    .httpPatch<Response<User>>(`${this.baseUrl}${this.accountContext}${this.userContext}`, this.multipart(form))
    .pipe(map(({ message, data }) => new Response<User>(message, new User(data))),
      catchError(error => this.handleError(error)))

  getQrCode = (): Observable<Response<any>>  => this._connector
    .httpGet<Response<any>>(`${this.baseUrl}${this.accountContext}/payment/qr`)
    .pipe(map(({ message, data }) => new Response<any>(message, data)),
      catchError(error => this.handleError(error)))

  getShoppingHistory = (): Observable<Response<any>> => this._connector
    .httpGet<Response<any>>(`${this.baseUrl}${this.accountContext}/payment`)
    .pipe(map(({ message, data }) => new Response<any>(message, data)),
      catchError(error => this.handleError(error)))

  changePassword = (form): Observable<Response<any>> => this._connector
    .httpPatch<Response<any>>(`${this.baseUrl}${this.authnContext}/password`, form)
    .pipe(map(({ message, data }) => new Response<any>(message, data)),
      catchError(error => this.handleError(error)))

  sendCode = (): Observable<Response<any>> => this._connector
    .httpGet<Response<any>>(`${this.baseUrl}${this.authnContext}/confirmation`)
    .pipe(map(({ message, data }) => new Response<any>(message, data)),
      catchError(error => this.handleError(error)))

  confirmation = (form): Observable<Response<any>> => this._connector
    .httpPost<Response<any>>(`${this.baseUrl}${this.authnContext}/confirmation`, form)
    .pipe(map(({ message, data }) => new Response<any>(message, data)),
      catchError(error => this.handleError(error)))

  // User's favouriteStations routes
  getUserFavouriteStations = (): Observable<Response<any[]>> => this._connector
    .httpGet<Response<any[]>>(`${this.baseUrl}${this.accountContext}/user/favourite/stations`)
    .pipe(map(({ message, data }) => new Response<any[]>(message, data)),
      catchError(error => this.handleError(error)))

  addUserFavouriteStation = (stationId: string): Observable<Response<any[]>> => this._connector
    .httpPost<Response<any[]>>(`${this.baseUrl}${this.accountContext}/user/favourite/station`, { station: stationId })
    .pipe(map(({ message, data }) => new Response<any[]>(message, data)),
      catchError(error => this.handleError(error)))

  removeUserFavouriteStation = (stationId: string): Observable<Response<any[]>> => this._connector
    .httpDelete<Response<any[]>>(`${this.baseUrl}${this.accountContext}/user/favourite/station/${stationId}`)
    .pipe(map(({ message, data }) => new Response<any[]>(message, data)),
      catchError(error => this.handleError(error)))

  // User's favouriteLocations routes
  getUserFavouriteLocations = (): Observable<Response<FavouriteLocation[]>>  => this._connector
    .httpGet<Response<FavouriteLocation[]>>(`${this.baseUrl}${this.accountContext}/user/favourite/locations`)
    .pipe(map(({ message, data }) => new Response<FavouriteLocation[]>(
        message, data ? data.map((c: any) => new FavouriteLocation(c)) : undefined)),
      catchError(error => this.handleError(error)))

  addUserFavouriteLocation = (location: FavouriteLocation): Observable<Response<FavouriteLocation[]>> => this._connector
    .httpPost<Response<FavouriteLocation[]>>(`${this.baseUrl}${this.accountContext}/user/favourite/location`, { location })
    .pipe(map(({ message, data }) => new Response<FavouriteLocation[]>(
        message, data ? data.map((c: any) => new FavouriteLocation(c)) : undefined)),
      catchError(error => this.handleError(error)))

  patchUserFavouriteLocation = (location: FavouriteLocation): Observable<Response<FavouriteLocation[]>> => this._connector
    .httpPatch<Response<FavouriteLocation[]>>(`${this.baseUrl}${this.accountContext}/user/favourite/location`, { location })
    .pipe(map(({ message, data }) => new Response<FavouriteLocation[]>(
          message, data ? data.map((c: any) => new FavouriteLocation(c)) : undefined)),
      catchError(error => this.handleError(error)))

  removeUserFavouriteLocation = (locationId: string): Observable<Response<FavouriteLocation[]>> => this._connector
    .httpDelete<Response<FavouriteLocation[]>>(`${this.baseUrl}${this.accountContext}/user/favourite/location/${locationId}`)
    .pipe(map(({ message, data }) => new Response<FavouriteLocation[]>(
        message, data ? data.map((c: any) => new FavouriteLocation(c)) : undefined)),
      catchError(error => this.handleError(error)))

  // User's cars routes
  getUserCars = (): Observable<Response<UserCar[]>> => this._connector
    .httpGet<Response<UserCar[]>>(`${this.baseUrl}${this.accountContext}/user/cars`)
    .pipe(map(({ message, data }) => new Response<UserCar[]>(
        message, data ? data.map((c: any) => new UserCar(c)) : undefined)),
      catchError(error => this.handleError(error)))

  addUserCar = (car: AddUserCarDTO): Observable<Response<UserCar[]>> => this._connector
    .httpPost<Response<UserCar[]>>(`${this.baseUrl}${this.accountContext}/user/car`, { car })
    .pipe(map(({ message, data }) => new Response<UserCar[]>(
        message, data ? data.map((c: any) => new UserCar(c)) : undefined)),
      catchError(error => this.handleError(error)))

  patchUserCar = (car: UpdateUserCarDTO): Observable<Response<UserCar[]>> => this._connector
    .httpPatch<Response<UserCar[]>>(`${this.baseUrl}${this.accountContext}/user/car`, { car })
    .pipe(map(({ message, data }) => new Response<UserCar[]>(
        message, data ? data.map((c: any) => new UserCar(c)) : undefined)),
      catchError(error => this.handleError(error)))

  removeUserCar = (carId: string): Observable<Response<UserCar[]>> => this._connector
    .httpDelete<Response<UserCar[]>>(`${this.baseUrl}${this.accountContext}/user/car/${carId}`)
    .pipe(map(({ message, data }) => new Response<UserCar[]>(
        message, data ? data.map((c: any) => new UserCar(c)) : undefined)),
      catchError(error => this.handleError(error)))

  setDefaultCar = (carId: string): Observable<Response<UserCar[]>> => this._connector
    .httpPost<Response<UserCar[]>>(`${this.baseUrl}${this.accountContext}/user/defaultcar`, { car: carId })
    .pipe(map(({ message, data }) => new Response<UserCar[]>(
        message, data ? data.map((c: any) => new UserCar(c)) : undefined)),
      catchError(error => this.handleError(error)))

  getPaymentMethods = (paymentReference: string): Observable<any> => this._connector
    .httpGet<any>(`${this.baseUrl}${this.orderContext}/payment/methods?paymentReference=${paymentReference}`)
    .pipe(catchError(error => this.handleError(error)))

  payOrder = (paymentReference: string, adyenData: any): Observable<any> => this._connector
    .httpPost<any>(`${this.baseUrl}${this.orderContext}/payment/pay?paymentReference=${paymentReference}`, adyenData)
    .pipe(catchError(error => this.handleError(error)))

  payOrderWithTerminal = (form: any): Observable<any> => this._connector
    .httpPost<any>(`${this.baseUrl}${this.orderContext}/payment/terminal`, form)
    .pipe(catchError(error => this.handleError(error)))

  constructor (public _connector: ConnectorService) { }

  public multipart (obj, form?, namespace?): FormData {
    const formData = form || new FormData()

    Object.keys(obj)
      .filter(p => obj[p] !== null && obj[p] !== undefined)
      .forEach(property => {
        const el = obj[property]
        const type = typeof el

        if (type !== 'string' || (type === 'string' && el.trim() !== '')) {
          const field = namespace ? `${namespace}[${property}]` : property

          if (type === 'object' && !(el instanceof File || el instanceof Blob)) {
            this.multipart(el, formData, field)
          } else {
            formData.append(field, el)
          }
        }
      })

    for (const property in obj) {
      if (obj[property] === undefined || obj[property] === null) {
        continue
      }
      const el = obj[property]
      const type = typeof obj[property]

      if (type !== 'string' || (type === 'string' && el.trim() !== '')) {
        const field = namespace ? `${namespace}[${property}]` : property

        if (type === 'object' && !(el instanceof File || el instanceof Blob)) {
          this.multipart(el, formData, field)
        } else {
          formData.append(field, el)
        }
      }
    }
    return formData
  }
}
