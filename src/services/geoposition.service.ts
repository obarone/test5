import { Injectable, OnDestroy } from '@angular/core'
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx'

import { Events } from './events.service'

import { Coords } from '../models/eloaded-core.models'

@Injectable()
export class GeopositionService implements OnDestroy {
  position: Coords = new Coords()
  watchId: any
  geolocationOptions = {
    maximumAge: 5000,
    timeout: 10000,
    enableHighAccuracy: true
  }

  constructor (private events: Events, private geolocation: Geolocation) {
    const sub = this.events.subscribe('app:ready', () => {
      sub.unsubscribe()

      this.watchId = this.geolocation
        .watchPosition(this.geolocationOptions)
        .subscribe((data: Geoposition) => {
          console.log(`positionFound`, data)
          this.position = new Coords({
            lat: data.coords.latitude,
            lng: data.coords.longitude,
            accuracy: data.coords.accuracy,
            timestamp: data.timestamp
          })
          this.events.publish('position:found', this.position)
        },
        error => console.log(error))
    })
  }

  getPosition (): Coords {
    return this.position
  }

  ngOnDestroy () {
    this.watchId.unsubscribe()
  }
}
