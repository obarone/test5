// cannot use enum because is not allowed to use regexp as value
export class ValidationRegex {
  public static EMAIL = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/
  public static NAME = /^[-'a-zA-Z \u00C0-\u00D6\u00D8-\u00F6\u00F8-\u024F]+$/
}
