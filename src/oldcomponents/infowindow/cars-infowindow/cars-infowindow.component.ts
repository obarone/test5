import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core'
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { Car, Plug, UserCar } from '../../../models/eloaded-core.models'
import { Infowindow, InfowindowTab } from '../../../models/eloaded-app.models'
import { InfowindowTabsHeaderComponent } from '../infowindow-tabs-header/infowindow-tabs-header.component'

@Component({
  selector: 'cars-infowindow',
  templateUrl: 'cars-infowindow.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class CarsInfowindowComponent extends Infowindow<UserCar> {
  @Input() stateOfCharge: number
  @Input() cars: UserCar[]
  @Input() systemCars: Car[]
  @Input() systemPlugs: Plug[]
  @ViewChild('defaultCarRow') defaultCarRow: ElementRef
  @ViewChild(InfowindowTabsHeaderComponent) infowindowTabsHeader: InfowindowTabsHeaderComponent
  @ViewChild('carsListPage') carsListPage: ElementRef
  @ViewChild('carEditorPage') carEditorPage: ElementRef

  @Output() carsChange: EventEmitter<UserCar[]> = new EventEmitter<UserCar[]>()
  @Output() stateOfChargeClick: EventEmitter<any> = new EventEmitter<any>()
  @Output() setAsDefault: EventEmitter<UserCar> = new EventEmitter<UserCar>()
  @Output() save: EventEmitter<UserCar> = new EventEmitter<UserCar>()
  @Output() remove: EventEmitter<UserCar> = new EventEmitter<UserCar>()
  @Output() collapse: EventEmitter<any> = new EventEmitter<any>()
  tabList = [
    new InfowindowTab({
      name: 'list',
      condition: () => true,
      height: { min: 100, max: 100 }
    }),
    new InfowindowTab({
      name: 'editor',
      loading: () => false,
      condition: () => !!this.focus,
      height: { min: 100, max: 100 }
    })
  ]

  editorDetails = false

  editor = new FormGroup({
    _id: new FormControl(),
    default: new FormControl(),
    plate: new FormControl('', Validators.required),
    capacity: new FormControl(undefined),
    consumption: new FormControl(),
    fast: new FormControl(0),
    model: new FormControl(undefined, [Validators.required]),
    plugs: new FormArray([]),
    consumptionDetails: new FormGroup({
      cold: new FormGroup({
        city: new FormControl(undefined, Validators.min(1)),
        combined: new FormControl(undefined, Validators.min(1)),
        highway: new FormControl(undefined, Validators.min(1))
      }),
      mild: new FormGroup({
        city: new FormControl(undefined, Validators.min(1)),
        combined: new FormControl(undefined, Validators.min(1)),
        highway: new FormControl(undefined, Validators.min(1))
      })
    })
  })

  constructor (public cd: ChangeDetectorRef, public ref: ElementRef) {
    super(cd, ref)
  }

  get bodyHeight (): { min: number, max: number } {
    let height
    switch (this.tabList[this.currentTab].name) {
      case 'list':
        height = this.carsListHeight
        break
      case 'editor':
        height = this.carsEditorHeight
        break
    }
    return height
  }

  // Used from parent
  get height (): { min: number, max: number } {
    const height = this.bodyHeight
    const header = this.infowindowTabsHeader
      ? this.infowindowTabsHeader.shown ? this.infowindowTabsHeader.height : 0
      : 0
    return { min: height.min + header, max: height.max + header }
  }

  get carsListHeight (): { min: number, max: number } {
    let defaultHeight = 0

    const carsListPageHeight = this.carsListPage && this.carsListPage.nativeElement
      ? this.carsListPage.nativeElement.offsetHeight : defaultHeight

    if (this.defaultCar) {
      defaultHeight = this.defaultCarRow && this.defaultCarRow.nativeElement
        ? this.defaultCarRow.nativeElement.offsetHeight : 0
    } else {
      defaultHeight = carsListPageHeight
    }
    return { min: defaultHeight, max: carsListPageHeight }
  }

  get carsEditorHeight (): { min: number, max: number } {
    const carEditorPage = this.carEditorPage && this.carEditorPage.nativeElement
      ? this.carEditorPage.nativeElement.offsetHeight : 0

    return { min: 0, max: carEditorPage }
  }

  public capacityExists (): boolean {
    const capacity = this.editor.get('capacity').value
    return !this.editorModel
      ? capacity : capacity || this.editorModel.capacity
  }

  public capacityValid (): boolean {
    const capacity = this.editor.get('capacity').value
    return capacity !== undefined && capacity !== null
      ? capacity > 0 : this.editorModel.capacity > 0
  }

  public consumptionExists(): boolean {
    const consumption = this.editor.get('consumption').value
    return !this.editorModel
      ? consumption : consumption || this.editorModel.consumption
  }

  public consumptionValid (): boolean {
    const consumption = this.editor.get('consumption').value
    return consumption !== undefined && consumption !== null
      ? consumption > 0 : this.editorModel.consumption > 0
  }

  get plugValidator (): FormGroup {
    return new FormGroup({
      plug: new FormControl(undefined, Validators.required),
      speed: new FormControl(0, [Validators.required, Validators.min(1)])
    })
  }

  minPlugs () {
    const control = this.editor.get('plugs') as FormArray
    return control.controls.length < 1
  }

  invalidPlugs () {
    const control = this.editor.get('plugs') as FormArray
    if (!control.controls.length) {
      return null
    }
    const atLeastOneInvalid = control.controls.some(control => !control.valid)
    return atLeastOneInvalid
  }

  get defaultCar (): UserCar {
    if (!this.cars || !this.cars.length) { return }
    return this.cars.find(car => car.default)
  }

  get otherCars (): UserCar[] {
    if (!this.cars || !this.cars.length) { return [] }
    if (!this.defaultCar) {
      return this.cars
    }
    return this.cars.filter(car => car._id !== this.defaultCar._id)
  }

  get editorModel (): Car {
    if (!this.editor.value.model) { return }
    return this.systemCars.find(car => car._id === this.editor.value.model)
  }

  get socColor (): string {
    if (this.stateOfCharge > 75) {
      return 'green'
    } else if (this.stateOfCharge > 30) {
      return 'yellow'
    } else {
      return 'red'
    }
  }

  get kmRange (): number {
    const consumption = this.defaultCar.consumption || this.defaultCar.model.consumption
    const capacity = this.defaultCar.capacity || this.defaultCar.model.capacity
    return Math.floor((capacity * this.stateOfCharge / 100) / consumption * 100)
  }

  toggleEditorDetails () {
    this.editorDetails = !this.editorDetails
    this.cd.detectChanges()
  }

  public maxChargeSpeed(): number {
    const control = this.editor.get('plugs') as FormArray
    if (!control.controls.length) {
      return null
    }

    let max = 0
    control.controls.forEach(control => {
      if (control.value.speed > max) {
        max = control.value.speed
      }
    })
    return max
  }

  public editorFast(): number {
    const carForm = this.editor.value
    return carForm.fast
  }

  openInfowindow () {
    switch (this.tabList[this.currentTab].name) {
      case 'list':
        this.carsListPage.nativeElement.scrollIntoView({ behavior: 'smooth' })
        break
      case 'editor':
        this.carEditorPage.nativeElement.scrollIntoView({ behavior: 'smooth' })
        break
    }
  }

  collapseInfowindow () {
    this.collapse.emit()
  }

  getPlugImage (_id: string) {
    const plug = this.systemPlugs.find(p => p._id === _id)
    return plug.image
  }

  toggleKeyboard () {
    this.stateOfChargeClick.emit()
  }

  closeInfowindow () {
    this.currentTab = this.defaultTab
    this.editorDetails = false
    this.focus = undefined
    this.editor.reset()
    this.cd.detectChanges()
  }

  openEditor (c?: UserCar) {
    this.emptyEditor()

    if (c) {
      this.setFocus(c)
      this.editor.disable()
      this.cd.detectChanges()
    } else {
      this.focus = new UserCar({ model: new Car() })
      this.cd.detectChanges()
      while ((this.editor.controls.plugs as FormArray).length) {
        (this.editor.controls.plugs as FormArray).removeAt(0)
      }
      (this.editor.get('plugs') as FormArray).push(this.plugValidator)
      this.editor.reset()
      this.editor.enable()
    }
    this.currentTab = 1
    this.cd.detectChanges()
  }

  setDefaultCar (car: UserCar) {
    this.setAsDefault.emit(car)
    this.currentTab = 0
    this.emptyEditor()
    this.focus = undefined
  }

  setDefaultCarFromEditor () {
    this.currentTab = 0
    this.setAsDefault.emit(this.focus)
    this.emptyEditor()
    this.focus = undefined
  }

  public availablePlugsOnCar(plugIndex: number): Array<Plug> {
    const car = this.editor.value
    return this.systemPlugs.filter(p =>
      !car.plugs.find((cp, i) => cp.plug === p._id && i !== plugIndex))
  }

  removePlug (index: number) {
    (this.editor.controls.plugs as FormArray).removeAt(index)
    this.cd.detectChanges()
  }

  addPlugToEditor () {
    (this.editor.controls.plugs as FormArray).push(this.plugValidator)
    this.cd.detectChanges()
  }

  enableEditor () {
    this.editor.enable()
    this.cd.detectChanges()
  }

  setFocus (car: UserCar) {
    this.focus = car

    while ((this.editor.controls.plugs as FormArray).length) {
      (this.editor.controls.plugs as FormArray).removeAt(0)
    }
    car.plugs.forEach(plug => {
      (this.editor.controls.plugs as FormArray).push(this.plugValidator)
    })
    this.editor.patchValue({
      ...car,
      model: car.model._id,
      plugs: car.plugs.map(pl => ({ plug: pl.plug._id, speed: pl.speed }))
    })
    this.editor.disable()
  }

  onSave () {
    if (this.editor.invalid || !this.capacityValid() || !this.consumptionValid()) {
      return this.markFormGroupTouched(this.editor)
    }
    this.getTab('editor').loading = () => true
    this.editor.disable()
    const value = this.editor.value
    this.focus = new UserCar({
      ...value,
      model: this.systemCars.find(car => car._id === value.model),
      plugs: value.plugs.map(cp => {
        const plug = this.systemPlugs.find(p => p._id === cp.plug)
        return { plug, speed: cp.speed }
      }),
    })
    if (!this.cars.length || this.cars.every(c => !c.default)) {
      this.focus.default = true
    }
    this.carsChange.emit(this.cars)
    this.cd.detectChanges()
    this.save.emit(this.focus)
  }

  saveCarSuccess () {
    this.jumpTo(0)
    this.focus = undefined
    this.getTab('editor').loading = () => false
  }

  saveCarError () {
    this.editor.enable()
    this.getTab('editor').loading = () => false
  }

  onRemove () {
    this.getTab('editor').loading = () => true
    this.cd.detectChanges()
    this.remove.emit(this.focus)
  }

  removeCarSuccess () {
    this.currentTab = 0
    this.focus = undefined
    this.emptyEditor()
    this.getTab('editor').loading = () => false
    this.cd.detectChanges()
  }

  removeCarError () {
    this.getTab('editor').loading = () => false
    this.cd.detectChanges()
  }

  abort () {
    this.emptyEditor()
    if (this.focus._id) {
      this.setFocus(this.focus)
    } else {
      this.focus = undefined
      this.currentTab = 0
    }
    this.editor.disable()
    this.cd.detectChanges()
  }


  emptyEditor () {
    while ((this.editor.controls.plugs as FormArray).controls.length) {
      (this.editor.controls.plugs as FormArray).removeAt(0)
    }
    this.editor.reset()
  }

  plugsControls (): FormArray {
    return this.editor.get('plugs') as FormArray
  }

  public markFormGroupTouched (formGroup: FormGroup): void {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched()

      if (control.controls) {
        this.markFormGroupTouched(control)
      }
    })
    this.cd.detectChanges()
  }
}
