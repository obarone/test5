import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core'
import { ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { QRScanner } from '@ionic-native/qr-scanner/ngx'
import { InfowindowTabsHeaderComponent } from '../infowindow-tabs-header/infowindow-tabs-header.component'
import { Infowindow, InfowindowTab } from '../../../models/eloaded-app.models'
import { ActiveTransaction, Cart, Order } from '../../../models/eloaded-core.models'
import { PaymentService } from '../../../services/payment.service'

declare var AdyenCheckout: any

@Component({
  selector: 'charge-infowindow',
  templateUrl: 'charge-infowindow.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class ChargeInfowindowComponent extends Infowindow<any> implements OnChanges {
  @Input() activeTransaction: string
  @Output() close: EventEmitter<any> = new EventEmitter<any>()
  @Output() requestEchargeOrder: EventEmitter<any> = new EventEmitter<any>()
  @Output() payOrder: EventEmitter<any> = new EventEmitter<any>()
  @Output() payOrderWebDropIn: EventEmitter<any> = new EventEmitter<any>()
  @Output() payOrderWithTerminal: EventEmitter<any> = new EventEmitter<any>()
  @Output() requestStopCharge: EventEmitter<any> = new EventEmitter<any>()
  @Output() getChargeDetail: EventEmitter<any> = new EventEmitter<any>()

  @ViewChild(InfowindowTabsHeaderComponent) infowindowTabsHeader: InfowindowTabsHeaderComponent
  @ViewChild('chargeForm') chargeFormTab: ElementRef
  @ViewChild('orderPreview') orderPreviewTab: ElementRef
  @ViewChild('chargeStatus') chargeStatusTab: ElementRef

  tabList = [
    new InfowindowTab({
      name: 'chargeForm',
      condition: () => !this.activeCharge,
      height: { min: 100, max: 500 }
    }),
    new InfowindowTab({
      name: 'orderPreview',
      condition: () => this.cart && !this.activeCharge,
      onEnter: () => this.chargingForm.enable(),
      onLeave: () => {
        this.order = undefined
        this.chargingForm.enable()
        if (this.currentTab === 1) {
          this._currentTab = 0
        }
      },
      height: { min: 100, max: 500 }
    }),
    new InfowindowTab({
      name: 'chargeStatus',
      condition: () => this.activeCharge,
      onLeave: () => {
        clearInterval(this.chargeTimeInterval)
        this.chargeTimeInterval = undefined
      },
      height: { min: 100, max: 500 }
    })
  ]
  chargingForm = new FormGroup({
    evseid: new FormControl('', Validators.required),
    current: new FormControl(50, Validators.required),
    targetSoC: new FormControl(100)
  })

  orderPreviewForm = new FormGroup({
    selected: new FormControl('', Validators.required),
    time: new FormControl('00:00'),
    hours: new FormControl(1),
    minutes: new FormControl(0)
  })
  chargeInterval: any

  _chargeTime: string
  chargeTimeInterval: any

  order: Order
  cart: Cart
  activeCharge: ActiveTransaction

  constructor (
    public cd: ChangeDetectorRef,
    public _payment: PaymentService,
    public qr: QRScanner,
    public ref: ElementRef) { super(cd, ref) }

  get chargeTime (): string {
    if (this.chargeTimeInterval) {
      return this._chargeTime
    } else if (!this.chargeTimeInterval) {
      const startingSeconds = new Date().getTime() - this.activeCharge.startDate.getTime()
      let acc = startingSeconds / 1000
      const startingDate = new Date('2000-01-01T00:00')
      startingDate.setTime(startingDate.getTime() + startingSeconds)
      this.chargeTimeInterval = setInterval(() => {
        startingDate.setTime(startingDate.getTime() + 1000)
        const ct = ++acc < 3600 ? `` : `${startingDate.getHours()}:`.padStart(3, '0')
        this._chargeTime = `${ct}`
          + `${startingDate.getMinutes()}:`.padStart(3, '0')
          + `${startingDate.getSeconds()}`.padStart(2, '0')
        this.cd.detectChanges()
      }, 1000)
    }
    return
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.activeTransaction) {
      if (changes.activeTransaction.currentValue !== changes.activeTransaction.previousValue || !this.chargeInterval) {
        this.updateActiveTransactionValue()
      }
    }
  }

  getChargeDetailsSuccess (chargeUpdate: ActiveTransaction) {
    if (chargeUpdate.stored) {
      if (chargeUpdate.status !== 'STOPPED' && chargeUpdate.status !== 'FINISHED') {
        chargeUpdate.status = 'ERROR'
      }
      console.log('stored')
      clearInterval(this.chargeInterval)
      clearInterval(this.chargeTimeInterval)
      this.chargeTimeInterval = undefined
      this.chargeInterval = undefined
    }
    const circle = document.getElementById('progress-circle')
    if (circle) {
      circle.setAttribute('data-progress', `${chargeUpdate.soc || 0}`)
    }
    const prev: boolean = !!this.activeCharge
    if (chargeUpdate.status !== 'CREATED') {
      this.activeCharge = chargeUpdate
    }
    if (!prev && this.activeCharge) {
      this.cart = undefined
      this.orderPreviewForm.reset()
      this.jumpTo(2)
    }

    this.cd.detectChanges()
  }

  dismissStoredTransaction (): boolean {
    if (this.activeCharge) {
      this.activeCharge = undefined
      this.jumpTo(0)
      this.cd.detectChanges()
      return true
    }
    return false
  }

  async updateActiveTransactionValue () {
    if (this.chargeInterval) {
      clearInterval(this.chargeInterval)
      this.chargeInterval = undefined
    }
    if (this.activeTransaction) {
      this.getChargeDetail.emit(this.activeTransaction)

      this.chargeInterval = setInterval(async () =>
        this.getChargeDetail.emit(this.activeTransaction) , 5000)
      this.cd.detectChanges()
    }
  }

  get bodyHeight (): { min: number, max: number } {
    const height = this.tabList[this.currentTab]
      ? { ...this.tabList[this.currentTab].height }
      : { min: 50, max: 50 }

    height.max = this[`${this.tabList[this.currentTab].name}Tab`] &&
      this[`${this.tabList[this.currentTab].name}Tab`].nativeElement
      ? this[`${this.tabList[this.currentTab].name}Tab`].nativeElement.offsetHeight
      : height.max

    if (height.max < height.min) { height.min = height.max }
    return height
  }

  get height (): { min: number, max: number } {
    const height = this.bodyHeight
    const header = this.infowindowTabsHeader
      ? this.infowindowTabsHeader.shown ? this.infowindowTabsHeader.height : 0
      : 0
    return { min: height.min + header, max: height.max + header }
  }

  requestChargingTime () {
    const { hours, minutes } = this.orderPreviewForm.value
    const time = hours * 60 + minutes
    const { evseid, targetSoc } = this.chargingForm.value
    this.requestEchargeOrder.emit({
      ...this.chargingForm.value,
      targetSoc: targetSoc || 100,
      evseid: evseid.toUpperCase(),
      time })
  }

  setSelectedOrderPreview (selected: string) {
    this.orderPreviewForm.patchValue({ selected })
    this.cd.detectChanges()
  }

  requestOrder () {
    if (this.chargingForm.invalid) {
      return
    }
    this.chargingForm.disable()
    this.getTab('chargeForm').loading = () => true
    const { evseid, targetSoC } = this.chargingForm.value
    this.requestEchargeOrder.emit({
      ...this.chargingForm.value,
      targetSoC: targetSoC || 100,
      evseid: evseid.toUpperCase() })
    this.cd.detectChanges()
  }

  async requestOrderSuccess (cart: Cart) {
    try {
      this.cart = cart
      this.jumpTo(1)
      this.getTab('chargeForm').loading = () => false
      if (this.cart && this.cart.orderPreviews && this.cart.orderPreviews.length) {
        const middle = Math.floor(this.cart.orderPreviews.length / 2)
        this.orderPreviewForm.patchValue({ selected: this.cart.orderPreviews[middle].id })
      }
      this.cd.detectChanges()
    } catch (error) {
      console.log(error)
    }
  }

  async requestCustomOrderPreviewSuccess (cart: Cart) {
    this.cart = cart
    this.orderPreviewForm.patchValue({
      time: '00:00',
      selected: this.cart.orderPreviews[this.cart.orderPreviews.length - 1].id
    })
    this.cd.detectChanges()
  }

  requestOrderPreviewError () {
    this.chargingForm.enable()
    this.getTab('chargeForm').loading = () => false
    this.cd.detectChanges()
  }

  showCamera() {
    (window.document.querySelector('ion-app') as HTMLElement)
      .classList.add('cameraView')
    this.cd.detectChanges()
  }

  hideCamera() {
    (window.document.querySelector('ion-app') as HTMLElement)
      .classList.remove('cameraView')
    this.cd.detectChanges()
  }

  cameraActive(): boolean {
    return (window.document.querySelector('ion-app') as HTMLElement)
      .classList.contains('cameraView')
  }

  async scan () {
    try {
      const status = await this.qr.prepare()
      if (status.denied) {
        alert('NO_PERMISSION')
        return
      }
      if (status.authorized) {
        this.showCamera()
        this.qr.show()
        this.cd.detectChanges()
        const scanSub = this.qr.scan().subscribe((text: string) => {
          const match = new RegExp(/^(https:\/\/statusecharge.eloaded.eu\/).{5}/)
            .test(text)
          if (!match) {
            alert('NOT ELOADED CHARGER CODE:' + text)
          } else {
            const parts = text.split('/')
            this.chargingForm.patchValue({ evseid: parts[parts.length - 1] })
          }
          this.hideCamera()

          this.qr.hide() // hide camera preview
          scanSub.unsubscribe() // stop scanning
        })
      } else {
        alert('denied temporarly')
        // permission was denied, but not permanently. You can ask for permission again at a later time.
      }
    } catch (e) {
      console.log('error', e)
    }
  }

  requestPayOrder () {
    console.log(this.orderPreviewForm.value)
    this.getTab('orderPreview').loading = () => true
    this.cd.detectChanges()
    this.payOrderWebDropIn.emit(this.orderPreviewForm.value.selected)
  }

  requestPayOrderWithTerminal () {
    this.payOrderWithTerminal.emit({
      orderPreview: this.orderPreviewForm.value.selected,
      terminal: 'IN01'
    })
  }

  async infowindowPayOrder () {
    console.log('infowindowPayOrder')
    // const configuration = await this._payment
    //   .getPaymentConfiguration(this.order.paymentConfig)

    // const checkout = new AdyenCheckout({
    //   ...configuration,
    //   onChange: (state, component) => {
    //     this.cd.detectChanges()
    //   },
    //   onError: (error) => console.log('error', error),
    //   onSubmit: (state, component) => {
    //     if (state.invalid) {
    //       console.log('state', state)
    //       console.log('component', component)
    //     } else {
    //       this.getTab('orderPreview').loading = () => true
    //       this.payOrder.emit({
    //         paymentReference: this.order.paymentReference,
    //         adyenData: state.data
    //       })
    //       this.cd.detectChanges()
    //     }
    //   },
    //   onAdditionalDetails: (state, component) => {
    //     console.log('state', state)
    //     console.log('component', component)
    //   }
    // })
    // checkout.create('dropin').mount('#adyen')
    // setTimeout(() => this.cd.detectChanges(), 3000)
  }

  orderPaymentSuccess () {
    this.getTab('orderPreview').loading = () => false
    this.cart = undefined
    this.cd.detectChanges()
  }

  orderPaymentAbort () {
    this.getTab('orderPreview').loading = () => false
    this.cd.detectChanges()
    // @TODO: handle various payment errors
  }

  orderPaymentError () {
    this.getTab('orderPreview').loading = () => false
    this.cd.detectChanges()
    // @TODO: handle various payment errors
  }

  async stopCharge () {
    if (!this.activeCharge) { return }
    this.requestStopCharge.emit(this.activeCharge._id)
  }

  setFocus (evseid) {
    this.focus = evseid
    this.chargingForm.patchValue({ evseid })
    this.cd.detectChanges()
    console.log('focus set')
  }

  closeInfowindow () {
    this.focus = undefined
    this.order = undefined
    this.close.emit()
  }
}
