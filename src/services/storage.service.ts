import { Injectable } from '@angular/core'
import { Storage } from '@ionic/storage-angular'
import { LogService } from './log.service'
import { Error, Filters } from '../models/eloaded-app.models'
import { errorType } from '../enumerators'

@Injectable()
export class StorageService {
  private _storage: Storage | null = null
  constructor (private storage: Storage, private _logger: LogService) { this.init() }

  getLoginInfo = async (): Promise<any> => this.get('loginInfo')
  setLoginInfo = async (loginInfo: any): Promise<void> => this.set('loginInfo', loginInfo)
  deleteLoginInfo = async (): Promise<void> => this.delete('loginInfo')

  getToken = async (): Promise<string> => this.get('token')
  setToken = async (token: string): Promise<void> => this.set('token', token)
  deleteToken = async (): Promise<void> => this.delete('token')

  getFilters = async (): Promise<Filters> => this.get('filters')
  setFilters = async (filters: Filters): Promise<void> => this.set('filters', filters)
  deleteFilters = async (): Promise<void> => this.delete('filters')

  async init () {
    this._storage = await this.storage.create(); 
  }
  async set (key: string, obj: any) {
    try {
      this._logger.info('call set() - StorageService')
      this._logger.info('with params: ', key, obj)
      await this._storage?.set(key, obj)
      if (key === 'token') {
        localStorage.setItem(key, obj)
      }
      this._logger.debug(`${key} added into storage`)
    } catch (error) {
      throw new Error('ERROR.MESSAGE.NO_CONFIGURATION', errorType.STORAGE)
    }
  }

  async get (key: string) {
    try {
      this._logger.info('call get() - StorageService')
      const obj = await this._storage?.get(key)
      this._logger.debug(`${key} taken from storage: `, obj)
      return obj
    } catch (error) {
      throw new Error('ERROR.MESSAGE.NO_CONFIGURATION', errorType.STORAGE)
    }
  }

  async delete (key: string) {
    try {
      this._logger.info('call delete() - StorageService')
      this._logger.info('with params: ', key)
      await this._storage?.remove(key)
      if (key === 'token') {
        localStorage.removeItem(key)
      }
      this._logger.debug(`${key} deleted from storage`)
    } catch (error) {
      throw new Error('ERROR.MESSAGE.NO_CONFIGURATION', errorType.STORAGE)
    }
  }

  async clear () {
    try {
      this._logger.info('call clear() - StorageService')
      await this._storage?.clear()
      this._logger.debug(`storage clear`)
    } catch (error) {
      throw new Error('ERROR.MESSAGE.NO_CONFIGURATION', errorType.STORAGE)
    }
  }
}
