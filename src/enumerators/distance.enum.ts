export enum meterToMile {
  mi = 1609.344,
  km = 1000,
  meter = 1
}
