import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Address, FavouriteLocation } from 'src/models/address.class';
import { Coords } from 'src/models/coords.class';
import { MapService } from 'src/services/map.service';

@Component({
  selector: 'searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent {
  @Input() favouriteLocations: FavouriteLocation[] = []
  @Input() favouriteStations: any[] = []
  @Input() showFavouritesLabels: boolean

  @Output() favouriteCallback = new EventEmitter<{ favourite: FavouriteLocation, setZoom: boolean }>()
  @Output() favouriteStationCallback = new EventEmitter<any>()
  @Output() locationCallback = new EventEmitter<{ location: Address, bounds: any }>()

  autocomplete = ''
  suggestions = { places: [], favourites: [] }
  constructor (
    private _map: MapService,
    private cd: ChangeDetectorRef) {}

  async updateSearch () {
    try {
      if (!this.autocomplete) {
        this.suggestions = { places: [], favourites: [] }
        return this.cd.detectChanges()
      }

      const predictions = await this._map
        .getPlacePredictions(this.autocomplete)
      this.suggestions.places = predictions
        .map(({ description }) => description)

      this.suggestions.favourites = this
        .searchfavourites(this.autocomplete)

      this.cd.detectChanges()
    } catch (error) {
      console.log(error)
    }
  }

  searchfavourites (query: string) {
    let response = []
    query.split(/ +/).forEach(subQuery => {
      if (!subQuery) {
        return
      }
      const tmp = this.favouriteLocations.filter(location => location.toString()
        .toLocaleLowerCase().includes(subQuery.toLocaleLowerCase()))
      // const tmp = this.favouriteLocations.filter(({ country, city, name, number, street, zip }) =>
      //   (country ? (country.toLowerCase().startsWith(subQuery.toLowerCase())) : false) ||
      //   (city ? (city.toLowerCase().startsWith(subQuery.toLowerCase())) : false) ||
      //   (name ? (name.toLowerCase().startsWith(subQuery.toLowerCase())) : false) ||
      //   (number ? (number.toLowerCase().startsWith(subQuery.toLowerCase())) : false) ||
      //   (street ? (street.toLowerCase().startsWith(subQuery.toLowerCase())) : false) ||
      //   (zip ? (zip.toLowerCase().startsWith(subQuery.toLowerCase())) : false))
      if (!tmp.length) {
        response = []
        return
      }
      tmp.forEach(res => {
        if (!response.includes(res)) {
          response.push(res)
        }
      })
    })
    return response
  }

  chooseFavouriteLocation (favourite: FavouriteLocation) {
    this.autocomplete = ``
    this.suggestions = { favourites: [], places: [] }
    this.cd.detectChanges()
    this.favouriteCallback.emit({ favourite, setZoom: true })
  }

  chooseFavouriteStation (favourite: any) {
    this.favouriteStationCallback.emit(favourite)
  }

  async chooseLocation (address: string) {
    try {
      const geodecoded = await this._map.geodecode(address)
      const { geometry } = geodecoded

      const location = new Address()

      geodecoded.address_components.forEach((field) => {
        switch (field.types[0]) {
          case 'country':
            location.country = field.long_name
            break
          case 'locality':
            location.city = field.long_name
            break
          case 'postal_code':
            location.zip = field.long_name
            break
          case 'route':
            location.street = field.long_name
            break
          case 'street_number':
            location.number = field.long_name
            break
        }
      })

      location.coords = new Coords({
        lat: geometry.location.lat(),
        lng: geometry.location.lng()
      })

      this.autocomplete = ``
      this.suggestions = { favourites: [], places: [] }
      this.cd.detectChanges()

      this.locationCallback
        .emit({ location, bounds: geometry.bounds || geometry.viewport })
    } catch (error) {
      console.log(error)
    }
  }
}
