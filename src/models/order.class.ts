import { OrderProduct } from './orderProduct.class'
import { PaymentConfiguration } from './paymentConfiguration.class'
import { User } from './user.class'

export class Order {
  _id: string
  user: User
  vendor: any
  orderNumber: number
  products: Array<OrderProduct> = []
  status: Array<OrderStatus> = []
  transactionId: string
  creationDate: Date
  paymentConfig: PaymentConfiguration

  constructor (obj: any = {}) {
    const properties = ['_id', 'orderNumber', 'vendor', 'transactionId']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.user) {
      this.user = new User(obj.user)
    }
    if (obj.products) {
      this.products = obj.products.map(op => new OrderProduct(op))
    }
    if (obj.status) {
      this.status = obj.status.map(os => new OrderStatus(os))
    }

    if (obj.creationDate) {
      this.creationDate = new Date(obj.creationDate)
    }

    if (obj.paymentConfig) {
      this.paymentConfig = new PaymentConfiguration(obj.paymentConfig)
    }
  }

  get currentStatus (): string {
    if (!this.status.length) { return }
    return this.status[this.status.length - 1].description
  }

  get totalPrice (): number {
    return Math.floor(this.products.reduce((acc, el) => acc += el.price.decimalValue, 0) * 100) / 100
  }

  get totalPriceString (): string {
    const el = `${Math.floor(this.products.reduce((acc, el) => acc += el.price.decimalValue, 0) * 100) / 100}`
    if (el.split('.')[1]) {
      const sp = el.split('.')[1].padEnd(2, '0')
      return `${el.split('.')[0]}.${sp}`
    }
    return el
  }

  get name (): string {
    if (!this.products.length) {
      return
    }
    return this.products.reduce((acc, el, i) =>
      acc = i !== 0 ? `${acc},${el.product}` : `${el.product}`, '')
  }

  get creationDateString(): string {
    return `${this.creationDate.getDate()}-`.padStart(3, '0')
    + `${this.creationDate.getMonth() + 1}-`.padStart(3, '0')
    + `${this.creationDate.getFullYear()} `
    + `${this.creationDate.getHours()}:`.padStart(3, '0')
    + `${this.creationDate.getMinutes()}`.padStart(2, '0')
  }
}

class OrderStatus {
  description: string

  constructor (os: any) {
    this.description = os.description
  }
}

