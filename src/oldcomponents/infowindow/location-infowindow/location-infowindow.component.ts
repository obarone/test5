import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core'
import { Address } from '../../../models/eloaded-core.models'
import { InfowindowTabsHeaderComponent } from '../infowindow-tabs-header/infowindow-tabs-header.component'
import { Infowindow, InfowindowTab } from '../../../models/eloaded-app.models'

@Component({
  selector: 'location-infowindow',
  templateUrl: 'location-infowindow.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class LocationInfowindowComponent extends Infowindow<Address> {
  @ViewChild(InfowindowTabsHeaderComponent) infowindowTabsHeader: InfowindowTabsHeaderComponent
  @ViewChild('locationInfoTab') locationInfoTab: ElementRef

  @Output() close: EventEmitter<any> = new EventEmitter<any>()
  @Output() setAsFavourite: EventEmitter<Address> = new EventEmitter<Address>()

  tabList = [new InfowindowTab({ name: 'info', condition: () => !!this.focus, height: { min: 100, max: 300 } })]
  constructor (public cd: ChangeDetectorRef, public ref: ElementRef) { super(cd, ref) }

  get bodyHeight (): { min: number, max: number } {
    const height = { min: 50, max: 50 }
    switch (this.tabList[this.currentTab].name) {
      case 'info':
        height.max = this.locationInfoTab && this.locationInfoTab.nativeElement
          ? this.locationInfoTab.nativeElement.offsetHeight : height.min
        break
    }
    return height
  }

  get height (): { min: number, max: number } {
    const height = this.bodyHeight
    const header = this.infowindowTabsHeader
      ? this.infowindowTabsHeader.shown ? this.infowindowTabsHeader.height : 0
      : 0
    return { min: height.min + header, max: height.max + header }
  }

  setFocus (location: Address) {
    this.focus = location
    this.cd.detectChanges()
  }

  addLocationToFavourites () {
    this.setAsFavourite.emit(this.focus)
  }

  closeInfowindow () {
    this.currentTab = this.defaultTab
    this.close.emit()
  }
}
