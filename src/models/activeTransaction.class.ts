export class ActiveTransaction {
  _id: string
  data: TransactionData[]
  echarger: string
  gun: number
  status: string
  voltage: number
  current: number
  maxPower: number
  evccid: string
  soc: number
  energy: number
  order: string
  startDate: Date
  stored: boolean

  constructor(obj: any = {}) {
    const properties = ['_id', 'echarger', 'gun', 'status', 'voltage', 'current',
      'maxPower', 'evccid', 'soc', 'energy', 'order', 'stored']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
    if (obj.data) {
      this.data = obj.data.map(d => new TransactionData(d))
    }
    if (obj.startDate) {
      this.startDate = new Date(obj.startDate)
    }

  }

  get presentCurrent (): number {
    if (this.data && this.data.length) {
      const currentList = this.data
        .map(({ info }) => info.presentCurrent)
        .filter(e => !!e)
      if (currentList.length) {
        return currentList[currentList.length - 1]
      }
      return 0
    }
    return 0
  }

  get presentVoltage (): number {
    if (this.data && this.data.length) {
      const voltageList = this.data
        .map(({ info }) => info.presentVoltage)
        .filter(e => !!e)
      if (voltageList.length) {
        return voltageList[voltageList.length - 1]
      }
      return 0
    }
    return 0
  }

  get presentKiloWatt (): number {
    if (this.presentCurrent && this.presentVoltage) {
      return Math.floor(this.presentCurrent * this.presentVoltage / 1000)
    }
    return 0
  }

  get chargeStatus (): number {
    if (this.data && this.data.length) {
      const statusList = this.data
        .map(({ info }) => info.chargeStatus)
        .filter(e => !!e)
      if (statusList.length) {
        return statusList[statusList.length - 1]
      }
    }
  }

}

export class TransactionData {
  info: TransactionDataInfo

  constructor(obj: any = {}) {
    this.info = new TransactionDataInfo(obj.info)
  }
}

export class TransactionDataInfo {
  value: any
  cid2: any
  gunNo: any
  user: any
  evccid: any
  chargeStatus: any
  sysMaxCurrent: any
  sysMaxVoltage: any
  evMaxCurrent: any
  evMaxVoltage: any
  connectorStatus: string
  presentCurrent: number
  presentVoltage: number
  soc: number

  constructor(obj: any = {}) {
    const properties = ['chargeStatus', 'sysMaxCurrent', 'sysMaxVoltage',
      'evMaxCurrent', 'evMaxVoltage', 'connectorStatus', 'presentCurrent', 'presentVoltage', 'soc']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
  }
}
