import { TransactionData } from './activeTransaction.class'

export class EloadedTransaction {
  _id: string
  data: TransactionData[]
  echarger: any
  gun: number
  startDate: Date
  status: string
  stopDate: Date

  constructor (obj: any = {}) {
    const properties = ['_id', 'echarger', 'gun', 'status']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.data && Array.isArray(obj.data)) {
      this.data = obj.data.map(d => new TransactionData(d))
    }
    if (obj.startDate) { this.startDate = new Date(obj.startDate) }
    if (obj.stopDate) { this.stopDate = new Date(obj.stopDate) }
  }

  get evseid (): string {
    return `${this.echarger && this.echarger.code}${this.gun}`
  }

  get socDifference (): number {
    if (!this.data) {
      return 0
    }
    const allsoc = this.data.map(({ info }) =>
      info && info.soc && info.chargeStatus !== 'Finished' ? info.soc : 0)
      .filter(e => !!e)
    if (!allsoc.length) {
      return 0
    }
    return Math.max(...allsoc) - Math.min(...allsoc)
  }

  get startDateString(): string {
    return `${this.startDate.getDate()}-`.padStart(3, '0')
    + `${this.startDate.getMonth() + 1}-`.padStart(3, '0')
    + `${this.startDate.getFullYear()} `
    + `${this.startDate.getHours()}:`.padStart(3, '0')
    + `${this.startDate.getMinutes()}`.padStart(2, '0')
  }

  get transactionDurationString (): string {
    const timediff = this.stopDate.getTime() - this.startDate.getTime()
    const date = new Date('2000-01-01T00:00')
    date.setTime(date.getTime() + timediff)
    return `${date.getHours()}:`.padStart(3, '0')
      + `${date.getMinutes()}:`.padStart(3, '0')
      + `${date.getSeconds()}`.padStart(2, '0')
  }
}
