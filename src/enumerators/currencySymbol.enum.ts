export enum CurrencySymbol {
  EUR = '€',
  GBP = '£',
  USD = '$'
}
