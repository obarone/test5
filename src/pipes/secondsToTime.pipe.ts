import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'secondsToTime' })
export class SecondsToTimePipe implements PipeTransform {
  times = {
    year: 31557600,
    month: 2629746,
    day: 86400,
    hour: 3600,
    minute: 60,
    second: 1
  }

  transform (seconds: number): string {
    let timeString = ''
    const conversion = (key) => Math.floor(seconds / this.times[key])

    for (const key in this.times) {
      if (conversion(key) > 0 && (key !== 'second' || timeString.length === 0)) {
        let timeUnit
        switch (key.toString()) {
          case 'second': timeUnit = 's'; break
          case 'minute': timeUnit = 'min'; break
          case 'hour': timeUnit = 'h'; break
          case 'day': timeUnit = 'd'; break
          case 'year': timeUnit = 'y'
        }
        timeString += conversion(key) + ' ' + timeUnit + ' '
        seconds = seconds - this.times[key] * conversion(key)
      }
    }

    return timeString.trim()
  }
}
