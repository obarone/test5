export class PaymentConfiguration {
  webDropIn: string
  id: string

  constructor (obj: any = {}) {
    if (obj.webDropIn) {
      this.webDropIn = obj.webDropIn
    }
    if (obj.id) {
      this.id = obj.id
    }
  }
}
