import { Plug } from './plug.class'

export class Gun {
  evseid: string
  plug: Plug
  power: number
  phases: number
  status: string

  constructor (obj: any = {}) {
    const properties = ['evseid', 'power', 'phases', 'status']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
    if (obj.plug) {
      this.plug = new Plug(obj.plug)
    }
  }
}
