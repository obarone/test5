import { Injectable, OnDestroy } from '@angular/core'
import { LogPublishersService } from './log-publishers.service'
import { LogPublisher } from '../models/log-publishers'
import { LogLevel } from '../enumerators'
import { LogEntry } from '../models/log-entry'
import { Subscription } from 'rxjs'

@Injectable()
export class LogService implements OnDestroy {
  publishers: LogPublisher[]
  level: LogLevel = LogLevel.OFF
  logWithDate = false
  public subscriptions: Subscription[] = []
  constructor (private publishersService: LogPublishersService) {
    this.publishers = this.publishersService.publishers
  }

  debug (msg: string, ...optionalParams: any[]) {
    this.writeToLog(msg, LogLevel.DEBUG, optionalParams)
  }

  info (msg: string, ...optionalParams: any[]) {
    this.writeToLog(msg, LogLevel.INFO, optionalParams)
  }

  warn (msg: string, ...optionalParams: any[]) {
    this.writeToLog(msg, LogLevel.WARN, optionalParams)
  }

  error (msg: string, ...optionalParams: any[]) {
    this.writeToLog(msg, LogLevel.ERROR, optionalParams)
  }

  fatal (msg: string, ...optionalParams: any[]) {
    this.writeToLog(msg, LogLevel.FATAL, optionalParams)
  }

  log (msg: string, ...optionalParams: any[]) {
    this.writeToLog(msg, LogLevel.ALL, optionalParams)
  }

  clear (): void {
    let subscription
    for (const logger of this.publishers) {
      subscription = logger.clear()
        .subscribe(response => console.log(response))
      this.subscriptions.push(subscription)
    }
  }

  ngOnDestroy () {
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }

  private shouldLog (level: LogLevel): boolean {
    let ret = false

    if ((level >= this.level &&
      level !== LogLevel.OFF) ||
      this.level === LogLevel.ALL) {
      ret = true
    }

    return ret
  }

  private writeToLog (msg: string, level: LogLevel, params: any[]) {
    if (this.shouldLog(level)) {
      const entry: LogEntry = new LogEntry()

      entry.message = msg
      entry.level = level
      entry.extraInfo = params
      entry.logWithDate = this.logWithDate
      let subscription
      for (const logger of this.publishers) {
        subscription = logger.log(entry)
          .subscribe(response => console.log(response))
        this.subscriptions.push(subscription)
      }
    }
  }

}
