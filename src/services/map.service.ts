import { Injectable, OnDestroy } from '@angular/core'
import { Events } from './events.service'
import { StorageService } from './storage.service'
import { NetworkService } from './network.service'
import { StationService } from './station.service'
import { Subscription, Observable } from 'rxjs'
import { Error, Filters } from '../models/eloaded-app.models'
import { errorType } from '../enumerators'

declare var google: any
declare var MarkerClusterer: any

@Injectable()
export class MapService implements OnDestroy {
  mapStd = {
    center: { lat: 47.268530, lng: 11.393250 },
    zoom: 10
  }
  markerIcons: any = {
    dynamicRed: (text) => 'https://mts.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-b.png&text=' +
      text + '&psize=16&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48',
    dynamicGreen: (text) => 'https://mts.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-a.png&text=' +
      text + '&psize=16&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48',
    yellow: { sunny: 'https://www.google.com/intl/en_us/mapfiles/ms/icons/yellow-dot.png', moon: 'https://www.google.com/intl/en_us/mapfiles/ms/icons/yellow-dot.png' },
    blue: 'http://www.google.com/intl/en_us/mapfiles/ms/icons/blue-dot.png',
    red: 'http://www.google.com/intl/en_us/mapfiles/ms/icons/red-dot.png',
    aqiRed: 'assets/img/general/aqi/red.png',
    aqiYellow: 'assets/img/general/aqi/yellow.png',
    aqiGreen: 'assets/img/general/aqi/green.png',
    aqiGrey: 'assets/img/general/aqi/grey.png',
    favourite: 'assets/img/pins/favourite/location.png',
    favouriteLocations: { sunny: 'assets/img/pins/favourite/location.png', moon: 'assets/img/pins/favourite/location.png' },
    positionMarker : { sunny: 'assets/img/pins/my_position_icon_blue.svg', moon: 'assets/img/pins/my_position_icon_blue.svg' }
  }
  autocompleteService: any = new google.maps.places.AutocompleteService()
  trafficLayer: any = new google.maps.TrafficLayer()
  longPress: any
  startPress: any
  aqis = []
  defaultPlugImage = 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAx' +
    'NS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcg' +
    'MS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkViZW5lXzEiIHhtbG5z' +
    'PSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0i' +
    'MzIuNXB4IiBoZWlnaHQ9IjQwcHgiIHZpZXdCb3g9IjAgMCAzMi41IDQwIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAzMi41IDQwIiB4bWw6c3BhY2U9InByZXNlcnZl' +
    'Ij4NCjxnPg0KCTxkZWZzPg0KCQk8cmVjdCBpZD0iU1ZHSURfMV8iIHk9IjUuNDIyIiB3aWR0aD0iMzIuNSIgaGVpZ2h0PSIyOS4xNTYiLz4NCgk8L2RlZnM+DQoJPGNsaXBQ' +
    'YXRoIGlkPSJTVkdJRF8yXyI+DQoJCTx1c2UgeGxpbms6aHJlZj0iI1NWR0lEXzFfIiAgb3ZlcmZsb3c9InZpc2libGUiLz4NCgk8L2NsaXBQYXRoPg0KCTxwYXRoIGNsaXAt' +
    'cGF0aD0idXJsKCNTVkdJRF8yXykiIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzhCQzUzRiIgc3Ryb2tlLXdpZHRoPSIyLjE1NTciIGQ9Ik0yNy45MzMsNy45ODUNCgkJYy0yLjE3' +
    'OS0yLjAzOS03LjczNy0xLjM4OS0xMS42ODMtMS4zNzljLTQuNzA0LDAuMDEyLTkuNTA0LTAuNjYtMTEuNjgyLDEuMzc5Yy0zLjQwNSwzLjE4Ny0zLjk1LDEwLjYwNC0zLjE4' +
    'NiwxMy42NjENCgkJYzIuOTc1LDExLjkxLDEzLjQ5MywxMS45MDEsMTQuODY4LDExLjg0N2MxLjM3NiwwLjA1NSwxMS44OTMsMC4wNjMsMTQuODY5LTExLjg0N0MzMS44ODMs' +
    'MTguNTg4LDMxLjMzNywxMS4xNzIsMjcuOTMzLDcuOTg1eiIvPg0KCTxwYXRoIGNsaXAtcGF0aD0idXJsKCNTVkdJRF8yXykiIGZpbGw9IiM4QkM1M0YiIGQ9Ik0yNS4yMjks' +
    'MTguNzg2YzAsMS4yOTYtMS4wMzksMi4zNDctMi4zMTgsMi4zNDcNCgkJYy0xLjI4MSwwLTIuMzItMS4wNTEtMi4zMi0yLjM0N2MwLTEuMjk2LDEuMDM5LTIuMzQ3LDIuMzIt' +
    'Mi4zNDdDMjQuMTg5LDE2LjQzOSwyNS4yMjksMTcuNDksMjUuMjI5LDE4Ljc4NiBNMjMuODEsMTIuMzQxDQoJCWMwLDAtMC40MjEsMC44OTItMC44MDUsMS4zNDRjLTAuMzI1' +
    'LDAuMzgzLTEuMTkzLDAuODg3LTEuNjg4LDAuODg3aC0zLjk1MXY5LjY2NmgtMi4yM3YtOS42NjZoLTMuODQxDQoJCWMtMC44NjgsMC0xLjQ2NC0wLjQzLTEuODQzLTAuODg3' +
    'Yy0wLjM4LTAuNDU3LTAuNzYtMS4zNDQtMC43Ni0xLjM0NEgyMy44MXogTTkuNzE0LDE2LjQzOWMxLjI4LDAsMi4zMTgsMS4wNTEsMi4zMTgsMi4zNDcNCgkJYzAsMS4yOTYt' +
    'MS4wMzksMi4zNDctMi4zMTgsMi4zNDdjLTEuMjgxLDAtMi4zMi0xLjA1MS0yLjMyLTIuMzQ3QzcuMzk1LDE3LjQ5LDguNDMzLDE2LjQzOSw5LjcxNCwxNi40MzkgTTkuMTM4' +
    'LDI0LjU0OA0KCQljMC0xLjI5OCwxLjAzNy0yLjM0OSwyLjMxOC0yLjM0OXMyLjMyLDEuMDUxLDIuMzIsMi4zNDljMCwxLjI5Ni0xLjAzOSwyLjM0Ni0yLjMyLDIuMzQ2Uzku' +
    'MTM4LDI1Ljg0NCw5LjEzOCwyNC41NDggTTE4Ljg0OSwyNC41NDgNCgkJYzAtMS4yOTgsMS4wMzctMi4zNDksMi4zMTgtMi4zNDljMS4yOCwwLDIuMzE5LDEuMDUxLDIuMzE5' +
    'LDIuMzQ5YzAsMS4yOTYtMS4wMzksMi4zNDYtMi4zMTksMi4zNDYNCgkJQzE5Ljg4NiwyNi44OTQsMTguODQ5LDI1Ljg0NCwxOC44NDksMjQuNTQ4IE0yNS4zNzMsMTAuODc1' +
    'Yy0xLjcwMS0xLjU0OC02LjA0My0xLjA1NC05LjEyMy0xLjA0Ng0KCQljLTMuNjczLDAuMDA5LTcuNDIyLTAuNTAxLTkuMTIzLDEuMDQ2Yy0yLjY1OSwyLjQyMS0zLjA4Myw4' +
    'LjA1My0yLjQ4OCwxMC4zNzVjMi4zMjQsOS4wNDQsMTAuNTM4LDkuMDM3LDExLjYxMSw4Ljk5Ng0KCQljMS4wNzQsMC4wNDEsOS4yODYsMC4wNDgsMTEuNjEtOC45OTZDMjgu' +
    'NDU4LDE4LjkyOCwyOC4wMzIsMTMuMjk2LDI1LjM3MywxMC44NzUiLz4NCjwvZz4NCjwvc3ZnPg0K'

  public subscriptions: Subscription [] = []
  constructor (
    public _network: NetworkService,
    public _station: StationService,
    public _storage: StorageService,
    private events: Events) {}

  async createMarker(map, options) {
    const filters = await this._storage.getFilters() || new Filters()
    const icon = filters.mapStyle
      ? options.icon[filters.mapStyle]
      : options.icon.sunny
    const scaledSize = options.icon.scaledSize
      ? new google.maps.Size(options.icon.scaledSize.height, options.icon.scaledSize.width)
      : undefined

    const marker = new google.maps.Marker({
      ...options,
      icon: { url: icon, scaledSize },
      map,
      listeners: undefined,
      events: undefined
    })

    if (!marker) {
      throw new Error('ERROR.MESSAGE.MARKER_ERROR', errorType.WARNING)
    }

    if (options.listeners && Object.keys(options.listeners).length) {
      Object.keys(options.listeners).forEach(listener =>
        marker.addListener(listener, () =>
          options.listeners[listener](marker)))
    }

    if (options.events && Object.keys(options.events).length) {
      Object.keys(options.events).forEach(event =>
        this.events.subscribe(event, function () {
          options.events[event](marker, ...arguments)
        }))
    }
    return marker
  }

  async getAQIs (token: string, ne, sw): Promise<any[]> {
    try {
      const result = await this._station.getAQIs(ne, sw).toPromise()

      if (result.message !== 'OK') {
        throw new Error(result.message, errorType.KO)
      }
      return result.data
    } catch (error) {
      console.log(error)
    }
  }

  addAQIonMap (map, newAQIs) {
    newAQIs.forEach(aqi => {
      const position = aqi.coords || new google.maps.LatLng(aqi.lat, aqi.lon)
      const icon = this.markerIcons.aqiYellow
      const marker = new google.maps.Marker({ icon, map, label: aqi.aqi })
      marker.setPosition(position)
      this.aqis[aqi.uid] = marker
    })
  }

  getMarkerCluster (map, markers, options) {
    return new MarkerClusterer(
      map,
      markers,
      { imagePath: 'assets/img/pins/clusters/m', ...options, maxZoom: 14 })
  }

  /* getRoute request at _stationService the route and give the elaborated response
    Params:
    token: Account Token
    origin: { name: string, coords: { lat, lng } }
    destination: { name: string, coords: { lat, lng } }
      - Usually if destination is a station destination.name = station.provider
    automony: number
    fast: boolean
    station: Station
  */
  getRoute (origin, destination, autonomy, fast, station?): Observable<any> {
    return Observable.create((observer) => {
      const routeSubscription = this._station
        .getRoute(origin.coords, destination.coords, autonomy, fast)
        .subscribe(
          ({ message, data }) => {
            if (message !== 'OK') {
              observer.error(new Error(message, errorType.KO))
              return
            }
            const route = { ...data, startPoint: origin, endPoint: destination }
            if (station) {
              const last = route.routes.length - 1
              route.routes[last].station = station
              route.routes[last].station['weather'] = route.routes[last].endingWeather
            }
            observer.next(route)
          },
          error => observer.error(error))

      this.subscriptions.push(routeSubscription)
    })
  }

  calculateRoute (from, to, energy, residual, fast, time, tracking?) {
    return this._station
      .calculateRoute({ from, to, energy, residual, fast, time, tracking })
  }

  /* Take input a LatLng object and return an Address */
  geocode (location) {
    return new Promise((resolve, reject) => {
      const geocoder = new google.maps.Geocoder()
      geocoder.geocode({ location }, (result, status) => {
        if (status === 'OK') {
          resolve(result[0].formatted_address)
        } else {
          reject(result)
        }
      })
    })
  }

  /* Take input an address and return a LatLng object */
  geodecode (address): Promise<any> {
    return new Promise((resolve, reject) => {
      const geodecoder = new google.maps.Geocoder()
      geodecoder.geocode({ address }, (result, status) => {
        if (status === 'OK') {
          resolve(result[0])
        } else {
          reject(new Error(status, errorType.WARNING))
        }
      })
    })
  }

  getMapStyle (mode) {
    let style = null
    if (mode === 'sunny') {
      style = null
    } else {
      // style = [
      //   {
      //     'elementType': 'geometry',
      //     'stylers': [
      //       {
      //         'color': '#212121'
      //       }
      //     ]
      //   },
      //   {
      //     'elementType': 'labels.icon',
      //     'stylers': [
      //       {
      //         'visibility': 'off'
      //       }
      //     ]
      //   },
      //   {
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#757575'
      //       }
      //     ]
      //   },
      //   {
      //     'elementType': 'labels.text.stroke',
      //     'stylers': [
      //       {
      //         'color': '#212121'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'administrative',
      //     'elementType': 'geometry',
      //     'stylers': [
      //       {
      //         'color': '#757575'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'administrative.country',
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#9e9e9e'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'administrative.land_parcel',
      //     'elementType': 'labels',
      //     'stylers': [
      //       {
      //         'visibility': 'off'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'administrative.locality',
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#bdbdbd'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi',
      //     'stylers': [
      //       {
      //         'visibility': 'simplified'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi',
      //     'elementType': 'labels.icon',
      //     'stylers': [
      //       {
      //         'visibility': 'simplified'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi',
      //     'elementType': 'labels.text',
      //     'stylers': [
      //       {
      //         'visibility': 'off'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi',
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#757575'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi.business',
      //     'stylers': [
      //       {
      //         'visibility': 'off'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi.park',
      //     'stylers': [
      //       {
      //         'visibility': 'off'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi.park',
      //     'elementType': 'geometry',
      //     'stylers': [
      //       {
      //         'color': '#181818'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi.park',
      //     'elementType': 'labels.text',
      //     'stylers': [
      //       {
      //         'visibility': 'off'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi.park',
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#616161'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'poi.park',
      //     'elementType': 'labels.text.stroke',
      //     'stylers': [
      //       {
      //         'color': '#1b1b1b'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'road',
      //     'elementType': 'geometry.fill',
      //     'stylers': [
      //       {
      //         'color': '#2c2c2c'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'road',
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#8a8a8a'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'road.arterial',
      //     'elementType': 'geometry',
      //     'stylers': [
      //       {
      //         'color': '#373737'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'road.highway',
      //     'elementType': 'geometry',
      //     'stylers': [
      //       {
      //         'color': '#3c3c3c'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'road.highway.controlled_access',
      //     'elementType': 'geometry',
      //     'stylers': [
      //       {
      //         'color': '#4e4e4e'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'road.local',
      //     'elementType': 'labels',
      //     'stylers': [
      //       {
      //         'visibility': 'off'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'road.local',
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#616161'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'transit',
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#757575'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'transit.station.bus',
      //     'stylers': [
      //       {
      //         'visibility': 'simplified'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'transit.station.bus',
      //     'elementType': 'geometry',
      //     'stylers': [
      //       {
      //         'visibility': 'simplified'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'transit.station.bus',
      //     'elementType': 'labels',
      //     'stylers': [
      //       {
      //         'visibility': 'simplified'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'water',
      //     'elementType': 'geometry',
      //     'stylers': [
      //       {
      //         'color': '#000000'
      //       }
      //     ]
      //   },
      //   {
      //     'featureType': 'water',
      //     'elementType': 'labels.text.fill',
      //     'stylers': [
      //       {
      //         'color': '#3d3d3d'
      //       }
      //     ]
      //   }
      // ]




      style = [
        {
          elementType: 'geometry',
          stylers: [{ color: '#242f3e' }]
        },
        {
          elementType: 'labels.text.stroke',
          stylers: [{ color: '#242f3e' }]
        },
        {
          elementType: 'labels.text.fill',
          stylers: [{ color: '#746855' }]
        },
        {
          featureType: 'administrative.locality',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#d59563' }]
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#d59563' }]
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{ color: '#263c3f' }]
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#6b9a76' }]
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{ color: '#38414e' }]
        },
        {
          featureType: 'road',
          elementType: 'geometry.stroke',
          stylers: [{ color: '#212a37' }]
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#9ca5b3' }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [{ color: '#746855' }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry.stroke',
          stylers: [{ color: '#1f2835' }]
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#f3d19c' }]
        },
        {
          featureType: 'transit',
          elementType: 'geometry',
          stylers: [{ color: '#2f3948' }]
        },
        {
          featureType: 'transit.station',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#d59563' }]
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [{ color: '#17263c' }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [{ color: '#515c6d' }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [{ color: '#17263c' }]
        }
      ]
    }
    return style
  }

  setMapStyle (map, style: string) {
    map.setOptions({ styles: this.getMapStyle(style) })
  }

  setTrafficLayer (map, on: boolean) {
    this.trafficLayer.setMap(on ? map : null)
  }

  async toggleTrafficMode (map) {
    try {
      const filters: any = await this._storage.getFilters()
      filters.traffic = !filters.traffic
      this.trafficLayer.setMap(filters.traffic ? map : null)
      await this._storage.setFilters(filters)
    } catch (error) {
      console.log(error)
    }
  }

  getPlacePredictions (address, types?): Promise<any[]> {
    return new Promise((resolve, reject) => {
      if (!this._network.isConnected()) {
        reject(new Error('ERROR.MESSAGE.NO_CONNECTION', errorType.NETWORK))
      } else {
        const query = (types) ? { input: address } : { input: address, types }

        this.autocompleteService
          .getPlacePredictions(query, (predictions, status) => {
            if (status === 'OK') {
              resolve(predictions)
            } else {
              resolve([])
            }
          }
        )
      }
    })
  }

  ngOnDestroy () {
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }

  // New functions, indipendent from map.service variables
  async initMap (divId: string, mapOptions, buttons: Array<any>) {
    const styles = this.getMapStyle(mapOptions.style)

    const options = {
      ...mapOptions,
      center: mapOptions.center || this.mapStd.center,
      zoom: mapOptions.zoom || this.mapStd.zoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles,
      clickableIcons: false,
      fullscreenControl: false
    }

    if (!this.trafficLayer) {
      this.trafficLayer = new google.maps.TrafficLayer()
    }

    const element: HTMLElement = document.getElementById(divId)
    const map = new google.maps.Map(element, options)

    if (mapOptions.traffic) {
      this.trafficLayer.setMap(map)
    }

    if (mapOptions.listeners && Object.keys(mapOptions.listeners).length) {
      Object.keys(mapOptions.listeners).forEach(key => {
        map.addListener(key, () => mapOptions.listeners[key]())
      })
    }
    return map
  }

  getBoundsFromLatLng (coords) {
    const bounds = new google.maps.LatLngBounds()
    bounds.extend(coords)
    return bounds
  }
}
