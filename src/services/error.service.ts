import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'
import { Error } from '../models/eloaded-app.models'
import { AlertService } from './alert.service'
import { ToastService } from './toast.service'
import { LogService } from './log.service'
import { errorType } from '../enumerators'

@Injectable()
export class ErrorService {

  public errors: Subject<Error> = new Subject()

  constructor (
      private _alert: AlertService, private _toast: ToastService,
      private _logger: LogService) {

    this.errors.subscribe((e: any) => {
      if (e.status === errorType.NETWORK) {
        if (e.message) {
          this._toast.toast(e.message)
        } else {
          this._toast.toast('ERROR.GENERIC')
        }
      } else {
        if (!e.message) { e.message = 'ERROR.GENERIC' }
        this._alert.basicAlert('ERROR.STATUS.' + e.status,
          e.status < 100
            ? e.message
            : e.message.toUpperCase().replace(/ /g, '_'),
          'ALERT.BUTTON.OK')
      }
    })
  }

  public showError (e) {
    this._logger.error(e)
    this.errors.next(e)
  }
}
