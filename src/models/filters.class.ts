import { Coords } from './coords.class'

export class Filters {
  mapStyle = 'sunny'
  filtered = 'all'
  traffic = false
  mapPosition: Coords
  charge: any = 100
  aqi = false
  remoteOnly = true

  constructor (obj: any = {}) {
    const properties = ['mapStyle', 'filtered', 'traffic', 'charge']
    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.mapPosition) {
      this.mapPosition = new Coords(obj.mapPosition)
    }
  }
}
