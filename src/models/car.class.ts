export class ConsumptionDetails {
  cold: { city: number, highway: number, combined: number }
  mild: { city: number, highway: number, combined: number }
  constructor (obj) {
    if (obj.cold) {
      if (obj.cold.city) { this.cold.city = obj.cold.city }
      if (obj.cold.combined) { this.cold.combined = obj.cold.combined }
      if (obj.cold.highway) { this.cold.highway = obj.cold.highway }
    } else {
      this.cold = { city: 0, highway: 0, combined: 0 }
    }
    if (obj.mild) {
      if (obj.mild.city) { this.mild.city = obj.mild.city }
      if (obj.mild.combined) { this.mild.combined = obj.mild.combined }
      if (obj.mild.highway) { this.mild.highway = obj.mild.highway }
    } else {
      this.mild = { city: 0, highway: 0, combined: 0 }
    }
  }
}

export class CarPlug {
  plug: any
  speed: number

  constructor(obj: any) {
    if (obj.plug) {
      this.plug = obj.plug
    }
    if (obj.speed) {
      this.speed = obj.speed
    }
  }

  public static isEqual(a: CarPlug, b: CarPlug): boolean {
    return ((a.plug._id || a.plug) === (b.plug._id || b.plug)) &&
      (a.speed === b.speed)
  }

  public isValid(): boolean {
    return !!(this.plug && this.speed)
  }
}

export class Car {
  _id: string
  manufacturer: string
  name: string
  capacity: number
  consumption: number
  maxSpeed: number
  active: boolean
  image: string
  creationDate: Date
  updateDate: Date
  consumptionDetails: {
    cold: { city: number, highway: number, combined: number }
    mild: { city: number, highway: number, combined: number }
  }

  constructor(obj: any = {}) {
    const properties = ['_id', 'manufacturer', 'name', 'capacity',
      'consumption', 'maxSpeed', 'active']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.creationDate) {
      this.creationDate = new Date(obj.creationDate)
    }

    if (obj.updateDate) {
      this.updateDate = new Date(obj.updateDate)
    }

    this.consumptionDetails = {
      cold: Object.assign({}, { city: undefined, highway: undefined, combined: undefined }),
      mild: { city: undefined, highway: undefined, combined: undefined }
    }

    if (obj.consumptionDetails) {
      if (obj.consumptionDetails.cold) {
        this.consumptionDetails.cold.city = Number(obj.consumptionDetails.cold.city) || undefined
        this.consumptionDetails.cold.highway = Number(obj.consumptionDetails.cold.highway) || undefined
        this.consumptionDetails.cold.combined = Number(obj.consumptionDetails.cold.combined) || undefined
      }
      if (obj.consumptionDetails.mild) {
        this.consumptionDetails.mild.city = Number(obj.consumptionDetails.mild.city) || undefined
        this.consumptionDetails.mild.highway = Number(obj.consumptionDetails.mild.highway) || undefined
        this.consumptionDetails.mild.combined = Number(obj.consumptionDetails.mild.combined) || undefined
      }
    }
  }

  get modelName (): string {
    return this.name
      .replace(new RegExp(`^${this.manufacturer}`, 'ig'), '').trim()
  }


  public static isEqual(a: Car, b: Car): boolean {
    const properties = ['_id', 'manufacturer', 'name', 'capacity',
      'consumption', 'maxSpeed', 'active']

    if (!properties.every(p => a[p] === b[p])) {
      return false
    }
    if ((a.creationDate && !b.creationDate) || (!a.creationDate && b.creationDate)) {
      return false
    }
    if (a.creationDate && b.creationDate) {
      if (a.creationDate.getTime() !== b.creationDate.getTime()) {
        return false
      }
    }
    if ((a.updateDate && !b.updateDate) || (!a.updateDate && b.updateDate)) {
      return false
    }
    if (a.updateDate && b.updateDate) {
      if (a.updateDate.getTime() !== b.updateDate.getTime()) {
        return false
      }
    }
    return true
  }

  public toString(): string {
    return `${this.manufacturer} ${this.name.replace(this.manufacturer, '').trim()}`
  }
}

export class UserCar {
  _id: string
  plate: string
  capacity: number
  consumption: number
  fast = 0
  default = false
  _model: Car
  plugs: CarPlug[] = []
  consumptionDetails: {
    cold: { city: number, highway: number, combined: number }
    mild: { city: number, highway: number, combined: number }
  }

  constructor(obj: any = {}) {
    const properties = ['plate', 'capacity', 'consumption', 'fast',
      'default', '_id']

    for (const property in obj) {
      if (properties.find((p) => p === property) && obj[property]) {
        this[property] = obj[property]
      }
    }

    if (obj.model) {
      this._model = new Car(obj.model)
    }

    if (obj.plugs && Array.isArray(obj.plugs)) {
      this.plugs = obj.plugs.map(p => new CarPlug(p))
    }

    this.consumptionDetails = {
      cold: Object.assign({}, { city: undefined, highway: undefined, combined: undefined }),
      mild: { city: undefined, highway: undefined, combined: undefined }
    }

    if (obj.consumptionDetails) {
      if (obj.consumptionDetails.cold) {
        this.consumptionDetails.cold.city = Number(obj.consumptionDetails.cold.city) || undefined
        this.consumptionDetails.cold.highway = Number(obj.consumptionDetails.cold.highway) || undefined
        this.consumptionDetails.cold.combined = Number(obj.consumptionDetails.cold.combined) || undefined
      }
      if (obj.consumptionDetails.mild) {
        this.consumptionDetails.mild.city = Number(obj.consumptionDetails.mild.city) || undefined
        this.consumptionDetails.mild.highway = Number(obj.consumptionDetails.mild.highway) || undefined
        this.consumptionDetails.mild.combined = Number(obj.consumptionDetails.mild.combined) || undefined
      }
    }
  }

  get model(): Car {
    return this._model
  }

  set model(model: Car) {
    this._model = new Car(model)
  }

  get maxChargeSpeed(): number {
    const t = (!this.plugs.length)
    ? 0
    : isNaN((Math.max(...this.plugs.map(({ speed }) => speed))))
      ? 0
      : (Math.max(...this.plugs.map(({ speed }) => speed)))

    return t
  }

  public static isEqual(a: UserCar, b: UserCar): boolean {
    const properties = ['plate', 'capacity', 'consumption', 'fast',
      'default']

    if (!properties.every(p => (!a[p] && !b[p]) || (a[p] === b[p]))) {
      return false
    }
    if (!Car.isEqual(a.model, b.model)) {
      return false
    }

    const aPlugs = a.plugs
    const bPlugs = b.plugs
    aPlugs.sort()
    bPlugs.sort()

    if (aPlugs.length !== bPlugs.length) {
      return false
    }
    if (!aPlugs.every((apl, i) => CarPlug.isEqual(apl, bPlugs[i]))) {
      return false
    }

    const acd = a.consumptionDetails
    const bcd = b.consumptionDetails

    if (Object.keys(acd).length !== Object.keys(bcd).length) {
      return false
    }
    if (Object.keys(acd).length) {
      if (Object.keys(acd.cold).length !== Object.keys(bcd.cold).length) {
        return false
      }
      if (acd.cold.city !== bcd.cold.city) { return false }
      if (acd.cold.highway !== bcd.cold.highway) { return false }
      if (acd.cold.city !== bcd.cold.city) { return false }

      if (Object.keys(acd.mild).length !== Object.keys(bcd.mild).length) {
        return false
      }
      if (acd.mild.city !== bcd.mild.city) { return false }
      if (acd.mild.highway !== bcd.mild.highway) { return false }
      if (acd.mild.city !== bcd.mild.city) { return false }
    }

    return true
  }

  toUpdateDTO (): UpdateUserCarDTO {
    return new UpdateUserCarDTO({
      _id: this._id,
      plate: this.plate,
      capacity: this.capacity,
      consumption: this.consumption,
      fast: this.fast,
      default: this.default,
      model: this._model._id,
      plugs: this.plugs.map((p: CarPlug) => ({ plug: p.plug._id, speed: p.speed })),
      consumptionDetails: this.fetchConsumptionDetails(this)
    })
  }

  toAddDTO (): AddUserCarDTO {
    return new AddUserCarDTO({
      plate: this.plate,
      capacity: this.capacity,
      consumption: this.consumption,
      fast: this.fast,
      default: this.default,
      model: this._model._id,
      plugs: this.plugs.map((p: CarPlug) => ({ plug: p.plug._id, speed: p.speed })),
      consumptionDetails: this.fetchConsumptionDetails(this)
    })
  }

  public fetchConsumptionDetails (car: UserCar) {
    const ret: any = new Object()
    if (!car.consumptionDetails || !Object.keys(car.consumptionDetails).length) {
      return undefined
    }
    const { cold, mild } = car.consumptionDetails
    if (!Object.keys(cold) && !Object.keys(mild)) {
      return undefined
    }
    ret.cold = {}
    if (cold && cold.city) { Object.assign(ret.cold, { city: cold.city })}
    if (cold && cold.highway) { Object.assign(ret.cold, { highway: cold.highway })}
    if (cold && cold.combined) { Object.assign(ret.cold, { combined: cold.combined })}
    if (!Object.keys(ret.cold).length) { delete ret.cold }

    ret.mild = {}
    if (mild && mild.city) { Object.assign(ret.mild, { city: mild.city })}
    if (mild && mild.highway) { Object.assign(ret.mild, { highway: mild.highway })}
    if (mild && mild.combined) { Object.assign(ret.mild, { combined: mild.combined })}
    if (!Object.keys(ret.mild).length) { delete ret.mild }

    return (Object.keys(ret).length) ? ret : undefined
  }
}


export class AddUserCarDTO {
  plate: string
  capacity: number
  consumption: number
  fast = 0
  default = false
  model: string
  plugs: Array<{ plug: string, speed: number }> = []
  consumptionDetails: ConsumptionDetails

  constructor(obj: any = {}) {
    const properties = ['plate', 'capacity', 'consumption', 'fast',
      'default', 'model']

    for (const property in obj) {
      if (properties.find((p) => p === property) && obj[property]) {
        this[property] = obj[property]
      }
    }

    if (obj.plugs && Array.isArray(obj.plugs)) {
      this.plugs = obj.plugs
    }

    if (obj.consumptionDetails) {
      if (obj.consumptionDetails.cold) {
        this.consumptionDetails.cold.city = Number(obj.consumptionDetails.cold.city) || undefined
        this.consumptionDetails.cold.highway = Number(obj.consumptionDetails.cold.highway) || undefined
        this.consumptionDetails.cold.combined = Number(obj.consumptionDetails.cold.combined) || undefined
      }
      if (obj.consumptionDetails.mild) {
        this.consumptionDetails.mild.city = Number(obj.consumptionDetails.mild.city) || undefined
        this.consumptionDetails.mild.highway = Number(obj.consumptionDetails.mild.highway) || undefined
        this.consumptionDetails.mild.combined = Number(obj.consumptionDetails.mild.combined) || undefined
      }
    }
  }
}

export class UpdateUserCarDTO {
  _id: string
  plate: string
  capacity: number
  consumption: number
  fast = 0
  default = false
  model: string
  plugs: Array<{ plug: string, speed: number }> = []
  consumptionDetails: ConsumptionDetails

  constructor(obj: any = {}) {
    const properties = ['plate', 'capacity', 'consumption', 'fast',
      'default', '_id', 'model']

    for (const property in obj) {
      if (properties.find((p) => p === property) && obj[property]) {
        this[property] = obj[property]
      }
    }

    if (obj.plugs && Array.isArray(obj.plugs)) {
      this.plugs = obj.plugs
    }

    if (obj.consumptionDetails) {
      if (obj.consumptionDetails.cold) {
        this.consumptionDetails.cold.city = Number(obj.consumptionDetails.cold.city) || undefined
        this.consumptionDetails.cold.highway = Number(obj.consumptionDetails.cold.highway) || undefined
        this.consumptionDetails.cold.combined = Number(obj.consumptionDetails.cold.combined) || undefined
      }
      if (obj.consumptionDetails.mild) {
        this.consumptionDetails.mild.city = Number(obj.consumptionDetails.mild.city) || undefined
        this.consumptionDetails.mild.highway = Number(obj.consumptionDetails.mild.highway) || undefined
        this.consumptionDetails.mild.combined = Number(obj.consumptionDetails.mild.combined) || undefined
      }
    }
  }
}
