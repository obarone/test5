export class Calendar {
  static MONTHS: string[] =
    ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY',
      'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER']

  static days: string[] =
    ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
}

export enum timeToSeconds {
  year = 31557600,
  month = 2629746,
  day = 86400,
  hour = 3600,
  minute = 60
}
