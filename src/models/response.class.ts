export class Response<T> {
  message: string
  data: T

  constructor(message: string, data: T) {
    this.message = message,
    this.data = data
  }

  get valid (): boolean {
    return this.message === 'OK'
  }

  get invalid (): boolean {
    return this.message !== 'OK'
  }
}
