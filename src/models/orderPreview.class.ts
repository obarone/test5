import { CurrencySymbol } from '../enumerators'
import { Amount } from './amount.class'
import { OrderProduct } from './orderProduct.class'
import { User } from './user.class'
import { Vendor } from './vendor.class'

export class OrderPreview {
  id: string
  amount: Amount
  user: User
  products: OrderProduct[]
  vendor: Vendor
  transactionId: string
  evccid: string
  evseid: string
  countryCode: string

  constructor (obj: any = {}) {
    const properties = ['id', 'transactionId', 'evccid', 'evseid', 'contryCode']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.amount) {
      this.amount = new Amount(obj.amount)
    }
    if (obj.user) {
      this.user = new User(obj.user)
    }

    if (obj.products && Array.isArray(obj.products)) {
      this.products = obj.products.map(p => new OrderProduct(p))
    }

    if (obj.vendor) {
      this.vendor = new Vendor(obj.vendor)
    }
  }

  equals (b: OrderPreview): boolean {
    return this.id === b.id
  }
}
