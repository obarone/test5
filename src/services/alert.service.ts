import { Injectable } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { AlertController } from '@ionic/angular'

@Injectable()
export class AlertService {
  public static CANCEL_BUTTON: Object = {
    text: 'ALERT.BUTTON.CANCEL',
    role: 'cancel'
  }

  loading: any
  buttons = new Map()

  constructor (public _translate: TranslateService, public alertCtrl: AlertController) {
    const buttonsPath = 'assets/img/buttons'
    this.buttons.set('ALERT.BUTTON.OK', `${buttonsPath}/icon_third_button.svg`)
    this.buttons.set('ALERT.BUTTON.CANCEL', `${buttonsPath}/icon_first_button.svg`)
    this.buttons.set('ALERT.BUTTON.SELECT/DESELECT_ALL', `${buttonsPath}/icon_second_button.svg`)
    this.buttons.set('ALERT.BUTTON.START', `${buttonsPath}/icon_start.svg`)
  }

  async basicAlert (title: string, subTitle: string, button: string) {
    const alert = await this.alertCtrl.create({
      header: this._translate.instant(title),
      message: this._translate.instant(subTitle),
      buttons: [this.buttons.has(button) ? button : this._translate.instant(button)],
      backdropDismiss: false
    })

    const present = await alert.present()
    this.substituteButtons()
    return present
  }

  async createAlert (title, subTitle, inputs, buttons) {
    if (buttons) {
      buttons = buttons.map(button =>
        Object.assign(
          Object.assign({}, button),
          button.text
            ? { text: this.buttons.has(button.text)
              ? button.text
              : this._translate.instant(button.text) }
            : {}
        )
      )
    }

    if (inputs) {
      inputs.forEach(input => {
        if (input.placeholder) {
          input.placeholder = this._translate.instant(input.placeholder)
        }
        if (input.label) {
          input.label = this._translate.instant(input.label)
        }
      })
    }

    const alert = await this.alertCtrl.create({
      header: this._translate.instant(title),
      message: this._translate.instant(subTitle),
      inputs: inputs,
      buttons: buttons,
      backdropDismiss: false
    })

    alert.present().then(() => this.substituteButtons())

    return alert
  }

  private substituteButtons () {
    Array.prototype.forEach.call(
      document.getElementsByClassName('alert-button'),
      (button) => {
        const buttonInner = button.getElementsByClassName('button-inner')[0]

        if (!buttonInner || !this.buttons.has(buttonInner.textContent)) { return }

        const img = document.createElement('img')
        img.setAttribute('src', this.buttons.get(buttonInner.textContent))

        buttonInner.textContent = ''
        buttonInner.appendChild(img)
      }
    )
  }
}
