import { ChangeDetectionStrategy, Component } from '@angular/core'
import { ChangeDetectorRef, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core'
import { EloadedTransaction, Order } from '../../../models/eloaded-core.models'
import { Infowindow, InfowindowTab } from '../../../models/eloaded-app.models'
import { InfowindowTabsHeaderComponent } from '../infowindow-tabs-header/infowindow-tabs-header.component'

@Component({
  selector: 'orderhistory-infowindow',
  templateUrl: 'orderhistory-infowindow.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class OrderHistoryInfowindowComponent extends Infowindow<any> {
  @Input() orders: Order[]

  @ViewChild(InfowindowTabsHeaderComponent) infowindowTabsHeader: InfowindowTabsHeaderComponent
  @ViewChild('orderHistoryTab') orderHistoryTab: ElementRef

  @Output() close: EventEmitter<any> = new EventEmitter<any>()

  tabList = [
    new InfowindowTab({
      name: 'orderHistory',
      condition: () => this.orders,
      height: { min: 100, max: 300 }
    })
  ]

  constructor (public cd: ChangeDetectorRef, public ref: ElementRef) {
    super(cd, ref)
  }

  get bodyHeight (): { min: number, max: number } {
    const height = this.tabList[this.currentTab]
      ? { ...this.tabList[this.currentTab].height }
      : { min: 50, max: 50 }

    height.max = this[`${this.tabList[this.currentTab].name}Tab`] &&
      this[`${this.tabList[this.currentTab].name}Tab`].nativeElement
      ? this[`${this.tabList[this.currentTab].name}Tab`].nativeElement.offsetHeight
      : height.max

    height.max = 500
    if (height.max < height.min) { height.min = height.max }
    return height
  }

  get height (): { min: number, max: number } {
    const height = this.bodyHeight
    const header = this.infowindowTabsHeader
      ? this.infowindowTabsHeader.shown ? this.infowindowTabsHeader.height : 0
      : 0
    return { min: height.min + header, max: height.max + header }
  }

  closeInfowindow () {
    this.jumpTo(this.defaultTab)
    this.close.emit()
  }
}
