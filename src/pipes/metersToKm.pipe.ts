import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'metersToKm' })
export class MetersToKmPipe implements PipeTransform {
  km = 1000

  transform  = (meters: number): string => Math.floor(meters / this.km) > 0
      ? (meters / this.km).toFixed(2) + ' km'
      : meters + ' m'
}
