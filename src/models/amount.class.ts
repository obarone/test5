import { CurrencySymbol } from '../enumerators'

export class Amount {
  value: number
  currency: string

  constructor (obj: any = {}) {
    if (obj.value) {
      this.value = obj.value
    }
    if (obj.currency) {
      this.currency = obj.currency
    }
  }

  get decimalValue (): number {
    return this.value / 100
  }

  equals (b: Amount): boolean {
    return b.currency === this.currency && b.value === this.value
  }

  toString (): string {
    const stringValue = this.value ? `${this.decimalValue}` : `0.00`
    if (stringValue.split('.')[1]) {
      const sp = stringValue.split('.')[1].padEnd(2, '0')
      return `${stringValue.split('.')[0]}.${sp} ${CurrencySymbol[this.currency]}`
    }
    return `${stringValue} ${CurrencySymbol[this.currency]}`
  }
}
