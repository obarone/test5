import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core'
import { StationService } from '../../../services/station.service'
import { StorageService } from '../../../services/storage.service'
import { MapService } from '../../../services/map.service'
import { Infowindow, InfowindowTab } from '../../../models/eloaded-app.models'
import { InfowindowTabsHeaderComponent } from '../infowindow-tabs-header/infowindow-tabs-header.component'
import { Gun, Station } from '../../../models/eloaded-core.models'

@Component({
  selector: 'station-infowindow',
  templateUrl: 'station-infowindow.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class StationInfowindowComponent extends Infowindow<Station> {
  @Input() canUserCharge: boolean
  @Output() getStationGuns: EventEmitter<any> = new EventEmitter<any>()
  @Output() save: EventEmitter<any> = new EventEmitter<any>()
  @Output() remove: EventEmitter<any> = new EventEmitter<any>()
  @Output() close: EventEmitter<any> = new EventEmitter<any>()
  @Output() selectCharger: EventEmitter<string> = new EventEmitter<string>()
  @ViewChild(InfowindowTabsHeaderComponent) infowindowTabsHeader: InfowindowTabsHeaderComponent
  @ViewChild('stationGeneral') stationGeneralTab: ElementRef
  @ViewChild('stationChargers') stationChargersTab: ElementRef

  tabList = [
    new InfowindowTab({
      name: 'general',
      condition: () => true,
      loading: () => !this.focus,
      height: { min: 100, max: 500 }
    }),
    new InfowindowTab({
      name: 'chargers',
      condition: () => this.focus && this.focus.remoteControl,
      onEnter: () => {
        if (this.focus && !this.focus.chargers) {
          this.getStationGuns.emit(this.focus._id)
        }
      },
      loading: () => !this.focus || !this.focus.chargers,
      height: { min: 100, max: 500 }
    })
  ]
  constructor (
    public cd: ChangeDetectorRef,
    public ref: ElementRef,
    public _map: MapService) { super(cd, ref) }

  get height (): { min: number, max: number } {
    const height = this.bodyHeight
    const header = this.infowindowTabsHeader
      ? this.infowindowTabsHeader.shown ? this.infowindowTabsHeader.height : 0
      : 0
    return { min: height.min + header, max: height.max + header }
  }

  get bodyHeight (): { min: number, max: number } {
    const height = { min: 50, max: 50 }
    switch (this.tabList[this.currentTab].name) {
      case 'general':
        height.max = this.stationGeneralTab && this.stationGeneralTab.nativeElement
          ? this.stationGeneralTab.nativeElement.offsetHeight : height.min
        break
      case 'chargers':
        height.max = this.stationChargersTab && this.stationChargersTab.nativeElement
        ? this.stationChargersTab.nativeElement.offsetHeight : height.min
    }
    if (this.tabList[this.currentTab].loading()) {
      height.min = height.max
    }
    return height
  }

  // swipeEvent (event) {
  //   super.swipeEvent(event)

  //   const askChargers = this.tabList[this.currentTab].name === 'chargers'
  //   this.getStationGuns.emit(this.focus._id)
  //   this.cd.detectChanges()
  // }

  // jumpTo (i: number) {
  //   super.jumpTo(i)

  //   const askChargers = this.tabList[this.currentTab].name === 'chargers'
  //   this.getStationGuns.emit(this.focus._id)
  //   this.cd.detectChanges()
  // }

  getStationGunsSuccess (guns: Gun[]) {
    this.focus.chargers = guns
    this.cd.detectChanges()
  }

  getStationGunsError () {
    console.log('error')
  }

  saveFavourite () {
    this.getTab('general').loading = () => true
    this.save.emit(this.focus)
    this.cd.detectChanges()
  }

  saveFavouriteSuccess () {
    this.focus.favourite = true
    this.getTab('general').loading = () => !this.focus
    this.cd.detectChanges()
  }

  saveFavouriteError () {
    this.getTab('general').loading = () => !this.focus
    this.cd.detectChanges()
  }

  removeFavourite () {
    this.getTab('general').loading = () => true
    this.remove.emit(this.focus)
    this.cd.detectChanges()
  }

  removeFavouriteSuccess () {
    this.focus.favourite = false
    this.getTab('general').loading = () => !this.focus
    this.cd.detectChanges()
  }

  removeFavouriteError () {
    this.getTab('general').loading = () => !this.focus
    this.cd.detectChanges()
  }

  setChargerFocus (charger?: Gun) {
    this.selectCharger.emit(charger ? charger.evseid : '')
    this.cd.detectChanges()
  }

  allKnownChargers (): boolean {
    if (!this.focus || !this.focus.chargers) {
      return false
    }
    return this.focus.chargers.every(c => c.plug)
  }

  public setFocus (station: Station) {
    this.focus = station
  }

  closeInfowindow () {
    this.focus = undefined
    this.currentTab = this.defaultTab
    this.close.emit()
  }
}
