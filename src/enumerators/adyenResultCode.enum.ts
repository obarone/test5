// https://docs.adyen.com/online-payments/components-web#step-6-present-payment-result
export enum AdyenResultCode {
  AUTHORISED = 'Authorised',
  ERROR = 'Error',
  PENDING = 'Pending',
  PRESENTTOSHOPPER = 'PresentToShopper',
  REFUSED = 'Refused',
  RECEIVED = 'Received'
}
