import { Injectable } from '@angular/core'

@Injectable()
export class UtilityService {
  constructor () { }

  /**
  * Convert a base64 string in a Blob according to the data and contentType.
  *
  * @param b64Data {String} Pure base64 string without contentType
  * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
  * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
  * @return Blob
  */
  public static base64ToBlob (b64Data: string, contentType: string): Blob {
    const byteCharacters = atob(b64Data)
    const byteArrays = []

    for (let offset = 0; offset < byteCharacters.length; offset += 512) {
      const slice = byteCharacters.slice(offset, offset + 512)

      const byteNumbers = new Array(slice.length)
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i)
      }

      const byteArray = new Uint8Array(byteNumbers)
      byteArrays.push(byteArray)
    }

    return new Blob(byteArrays, { type: contentType })
  }

  static getDay = (): string  =>
    `${new Date().getDate()}`.padStart(2, '0')

  static getMonth = (): string  =>
    `${new Date().getMonth() + 1}`.padStart(2, '0')

  static getYear = (): string => `${new Date().getFullYear()}`

  static getHour = (): string => `${new Date().getHours()}`.padStart(2, '0')

  static getMinute = (): string => `${new Date().getMinutes()}`.padStart(2, '0')

  static getActualDatetimeString = (): string =>
    `${UtilityService.getYear()}-${UtilityService.getMonth()}-${UtilityService.getDay()}T`
      + `${UtilityService.getHour()}:${UtilityService.getMinute()}`

  shiftElementArray (array: any[], index: number, position): any[] {
    array = array.concat(array.splice(position, index))
    return array
  }

  base64ToArrayBuffer (base64) {
    const binaryString = window.atob(base64)
    const binaryLen = binaryString.length
    const bytes = new Uint8Array(binaryLen)
    for (let i = 0; i < binaryLen; i++) {
      const ascii = binaryString.charCodeAt(i)
      bytes[i] = ascii
    }
    return bytes
  }

  getPdfFileFromArrayBuffer (name: string, byte) {
    const blob = new Blob([byte])
    const link = document.createElement('a')
    link.href = window.URL.createObjectURL(blob)
    const fileName = name + '.pdf'
    return fileName
  }

  parseDatetimeFromString (date) {
    const split1 = date.split('-')
    const split2 = split1[2].split('T')
    const split3 = split2[1].split(':')

    return {
      year: split1[0],
      month: split1[1] - 1,
      day: split2[0],
      hour: split3[0],
      minute: split3[1]
    }
  }

  parseStringToDate (date) {
    const parsedDatatime = this.parseDatetimeFromString(date)
    return new Date(parsedDatatime.year, parsedDatatime.month,
      parsedDatatime.day, parsedDatatime.hour, parsedDatatime.minute)
  }

  formatDatetime (date): string {
    return `${('0' + date.day).slice(-2)}.`
      + `${('0' + (date.month + 1)).slice(-2)}.`
      + `${date.year} ${('0' + date.hour).slice(-2)}:`
      + `${('0' + date.minute).slice(-2)}`
  }

//   // mode:
//   //   ScreenOrientation.ORIENTATIONS:
//   //     PORTRAIT
//   //     PORTRAIT-PRIMARY
//   //     PORTRAIT-SECONDARY
//   //     LANDSCAPE
//   //     LANDSCAPRE-PRIMARY
//   //     LANDSCAPE-SECONDARY
//   // example:
//   //   this._utility.screenOrientations.PORTRAIT
//   lockScreenOrientation (mode): void {
//     if (this.device.platform === 'Android' || this.device.platform === 'iOS') {
//       this.screenOrientation.lock(mode)
//     }
//   }

//   unlockScreenOrientation (): void {
//     if (this.device.platform === 'Android' || this.device.platform === 'iOS') {
//       this.screenOrientation.unlock()
//     }
//   }
}
