import { Injectable, OnDestroy } from '@angular/core'

import { LoadingController } from '@ionic/angular'

import { Events } from './events.service'

@Injectable()
export class LoaderService implements OnDestroy {
  loading: HTMLIonLoadingElement
  active = false
  private loadingStartSub
  private loadingStopSub
  constructor (public loadingCtrl: LoadingController, private events: Events) {
    this.loadingStartSub = this.events
      .subscribe('loading:start', (duration: number) => this.load(duration))
    this.loadingStopSub = this.events
      .subscribe('loading:stop', () => this.stopLoad())
  }

  get isLoading () {
    return this.active
  }

  async load (duration?: number) {
    if (duration) {
      this.loading = await this.loadingCtrl.create({
        cssClass: 'loading-page',
        // content: '<img src="./assets/img/loading/general.gif" />',
        spinner: null,
        duration: duration * 1000
      })
    } else {
      this.loading = await this.loadingCtrl.create({
        cssClass: 'loading-page',
        // content: '<img src="./assets/img/loading/general.gif" />',
        spinner: null
      })
    }
    this.active = true
    this.loading.present()
      .then(() => this.events.publish('loading:done'))
      .catch(() => this.events.publish('loading:error'))
  }

  stopLoad () {
    if (this.active) {
      this.active = false
      this.loading.dismiss()
    }
  }

  ngOnDestroy () {
    this.loadingStartSub.unsubscribe()
    this.loadingStopSub.unsubscribe()
  }
}
