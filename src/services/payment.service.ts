import { Injectable } from '@angular/core'
import { ApiService } from './api.service'
import { Observable } from 'rxjs'
import { Response, Order } from '../models/eloaded-core.models'

@Injectable()
export class PaymentService {
  getOrderHistory = (): Promise<Response<Order[]>> => this._api
    .getOrderHistory().toPromise<Response<Order[]>>()

  createOrder = (orderPreviewId: string): Promise<Response<Order>> => this._api
    .createOrder({ orderPreview: orderPreviewId }).toPromise<Response<Order>>()

  constructor (public _api: ApiService) {}

  getShoppingHistory (): Observable<any> {
    return this._api.getShoppingHistory()
  }

  getShoppingHistoryPdf (): Observable<any> {
    return this._api.getShoppingHistoryPdf()
  }

  getPaymentMethods (paymentReference: string): Promise<any> {
    return this._api.getPaymentMethods(paymentReference).toPromise()
  }

  payOrder (paymentReference: string, adyenData: any): Promise<any> {
    return this._api.payOrder(paymentReference, adyenData).toPromise()
  }

  payOrderWithTerminal (payment: string, terminal: string): Promise<any> {
    return this._api.payOrderWithTerminal({ payment, terminal }).toPromise()
  }

  async getPaymentConfiguration (paymentReference: string) {
    const paymentMethodsResponse = await this
      .getPaymentMethods(paymentReference)

    console.log(paymentMethodsResponse)
    const configuration = {
      paymentMethodsResponse,
      clientKey: 'test_OLIE5IJGUBCFZOQ2CTMC2OJPNMEB4H3M',
      openFirstPaymentMethod: false,
      openFirstStoredPaymentMethod: true,
      showPaymentMethods: true,
      showStoredPaymentMethods: true,
      showPayButton: true,
      enableStoreDetails: true,
      locale: 'en-EN',
      countryCode: 'de',
      environment: 'test',
      amount: { currency: 'EUR', value: 10000 },
      paymentMethodsConfiguration: {
        card: { hideCVC: false },
        paywithgoogle: { // Example required configuration for Google Pay
          environment: '{{PAY_WITH_GOOGLE_ENVIRONMENT}}',
          amount: { currency: 'EUR', value: 10000 },
          configuration: {
            gatewayMerchantId: '{{PAY_WITH_GOOGLE_GATEWAY_MERCHANT_ID}}', // Your Adyen merchant or company account name
            merchantName: '{{PAY_WITH_GOOGLE_MERCHANT_NAME}}' // Optional. The name that appears in the payment sheet.
          }
        }
      }
    }

    return configuration
  }
}
