import { OrderPreview } from './orderPreview.class'

export class Cart {
  user: string
  orderPreviews: OrderPreview[]

  constructor (obj: any = {}) {
    if (obj.user) {
      this.user = obj.user
    }

    if (obj.orderPreviews && Array.isArray(obj.orderPreviews)) {
      this.orderPreviews = obj.orderPreviews.map(op => new OrderPreview(op))
    }
  }
}
