export class Plug {
  _id: string
  dataSmall: string
  dataLong: string
  image: string

  constructor (obj = {}) {
    const properties = ['_id', 'dataSmall', 'dataLong', 'image']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
  }
}
