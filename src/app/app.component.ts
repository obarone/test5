import { Component } from '@angular/core';
import { MenuOptionModel } from 'src/oldcomponents/side-menu-content/models/menu-option-model';
import { SideMenuSettings } from 'src/oldcomponents/side-menu-content/models/side-menu-settings';
import { Events } from 'src/services/events.service';
import { FolderPage } from './folder/folder.page';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Inbox', url: '/folder/Inbox', icon: 'mail' },
    { title: 'Outbox', url: '/folder/Outbox', icon: 'paper-plane' },
    { title: 'Favorites', url: '/folder/Favorites', icon: 'heart' },
    { title: 'Archived', url: '/folder/Archived', icon: 'archive' },
    { title: 'Trash', url: '/folder/Trash', icon: 'trash' },
    { title: 'Spam', url: '/folder/Spam', icon: 'warning' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders']
  // Options to show in the SideMenuComponent
  public options: Array<MenuOptionModel> = [
    {
      iconName: 'ionic',
      displayName: 'SEARCH_STATIONS',
      component: FolderPage
    },
    {
      iconName: 'paper',
      displayName: 'ELOADED_ORDERS',
      custom: () => {
        this.events.publish('eloadedorders')
      }
    }
  ]
  // Settings for the SideMenuComponent
  public sideMenuSettings: SideMenuSettings = {
    accordionMode: true,
    showSelectedOption: true,
    selectedOptionClass: 'active-side-menu-option',
    subOptionIndentation: { md: '56px', ios: '64px', wp: '56px' }
  }
  constructor(public events: Events) {}

  selectOption (option: any) {
    console.log(option)
  }
}
