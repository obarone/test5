import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output } from '@angular/core'

@Component({
  selector: 'infowindow-tabs-header',
  templateUrl: 'infowindow-tabs-header.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class InfowindowTabsHeaderComponent {
  @Input() tabList: Array<{ name: string, condition: () => boolean }> = []
  @Input() current: number
  @Input() prefix: string
  @Input() autoHide = false

  @Output() jumpTo: EventEmitter<number> = new EventEmitter<number>()
  @Output() close: EventEmitter<any> = new EventEmitter<any>()
  constructor (private cd: ChangeDetectorRef, public ref: ElementRef) {}

  get validTabs (): Array<{ name: string, condition: () => boolean }> {
    return this.tabList.filter(({ condition }) => condition())
  }

  get height (): number {
    return this.ref.nativeElement.offsetHeight
  }

  get shown (): boolean {
    return this.validTabs.length > 1 || !this.autoHide
  }
}
