import { Coords } from './coords.class'

export class Address {
  coords: Coords
  country: string
  city: string
  zip: string
  street: string
  number: string
  other: string

  constructor(obj: any = {}) {
    const properties = ['country', 'city', 'zip', 'street', 'number', 'other']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.coords) {
      this.coords = new Coords(obj.coords)
    }
  }

  get address (): string {
    return `${this.street ? `${this.street}, ` : ``}` +
    `${this.number ? `${this.number}. ` : ``}` +
    `${this.city}${this.zip ? ` ${this.zip}` : ``}, ` +
    `${this.country}${this.other ? ` - ${this.other}` : ``}`
  }

  isEqual (addr: Address): boolean {
    return this.address === addr.address && this.coords.isEqual(addr.coords)
  }
}

export class FavouriteLocation extends Address {
  _id: string
  name = ''

  constructor(obj: any = {}) {
    super(obj)
    if (obj._id) {
      this._id = obj._id
    }

    if (obj.name) {
      this.name = obj.name
    }
  }

  toString (): string {
    return `${this.name}: ${this.address}`
  }

  isEqual (fl: FavouriteLocation): boolean {
    return super.isEqual(fl) && this._id === fl._id && this.name === fl.name
  }
}
