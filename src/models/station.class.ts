import { Coords } from './coords.class'
import { Plug } from './plug.class'

export class StationPlug {
  plug: Plug
  number: number
  power: number
  phases: number

  constructor (obj: any = {}) {
    const properties = ['number', 'power', 'phases']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
    if (obj.plug) {
      this.plug = new Plug(obj.plug)
    }
  }
}

export class Station {
  _id: string
  countryID: string
  name: string
  coords: Coords
  formattedAddress: string
  providerHTML: string
  providerName: string
  address: any
  info: any
  plugCount: number
  plugs: StationPlug[]
  favourite: boolean

  chargers: any[]
  zones: Zone[]
  remoteControl: boolean
  route: any
  fast: string

  constructor (obj: any = {}) {
    const properties = ['_id', 'countryID', 'name', 'formattedAddress', 'providerHTML',
      'providerName', 'address', 'info', 'plugCount', 'remoteControl', 'favourite', 'fast']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.coords) {
      this.coords = new Coords(obj.coords)
    }

    if (obj.plugs) {
      this.plugs = obj.plugs.map(p => new StationPlug(p))
    }
    if (obj.zones && Array.isArray(obj.zones)) {
      this.zones = obj.zones.map(z => new Zone(z))
    }

    if (obj.route) {
      this.route = obj.route
    }
  }

  get guns (): any[] {
    return this.zones
    ? this.zones.reduce((acc, el) => acc = acc.concat(el.guns), [])
    : []
  }
}

export class Zone {
  _id: string
  continuingEchargePower: number
  name: string
  offerEnergy: boolean
  parkName: string
  peakEchargePower: number
  peakParkPower: number
  peakZonePower: number
  echargers: Echarger[]

  constructor (obj: any = {}) {
    const properties = ['_id', 'continuingEchargePower', 'name', 'offerEnergy',
      'parkName', 'peakEchargePower', 'peakParkPower', 'peakZonePower']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.echargers && Array.isArray(obj.echargers)) {
      this.echargers = obj.echargers.map(e => new Echarger(e))
    }
  }

  get guns (): any[] {
    return this.echargers
      ? this.echargers.reduce((acc, el) => acc = acc.concat(el.guns), [])
      : []
  }
}

export class Echarger {
  _id: string
  code: string
  name: string
  plugs: EchargerGun[]
  vendor: string

  constructor (obj: any = {}) {
    const properties = ['_id', 'code', 'name', 'vendor']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
    if (obj.plugs && Array.isArray(obj.plugs)) {
      this.plugs = obj.plugs.map(p => new EchargerGun(p))
    }
  }
  get guns (): any[] {
    return this.plugs
      ? this.plugs.map((p, i) => ({ ...p, evseid: `${this.code}${i}`}))
      : []
  }
}

export class EchargerGun {
  number: number
  phases: number
  plug: Plug
  power: number

  constructor (obj: any = {}) {
    const properties = ['number', 'phases', 'power']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
    if (obj.plug) {
      this.plug = new Plug(obj.plug)
    }
  }
}
