import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx'
import { TranslateService } from '@ngx-translate/core'

import { AccountService } from '../../services/account.service'
import { EchargeService } from '../../services/echarge.service'
import { ErrorService } from '../../services/error.service'
import { Events } from '../../services/events.service'
import { FilterService } from '../../services/filter.service'
import { GeopositionService } from '../../services/geoposition.service'
import { MapService } from '../../services/map.service'
import { PaymentService } from '../../services/payment.service'
import { PermissionsService } from '../../services/permissions.service'
import { StationService } from '../../services/station.service'
import { StorageService } from '../../services/storage.service'
import { UtilityService } from '../../services/utility.service'

import { FiltersOnMapComponent } from '../../oldcomponents/filters-on-map/filters-on-map.component'
import { InfowindowComponent } from '../../oldcomponents/infowindow/infowindow.component'

import { Address, Car, FavouriteLocation, UserCar, Bounds, Coords, Plug, User, Order, ActiveTransaction } from '../../models/eloaded-core.models'
import { Error, Filters } from '../../models/eloaded-app.models'

import { AdyenResultCode, errorType } from '../../enumerators'

@Component({
  selector: 'page-homepage',
  templateUrl: 'homepage.html',
  providers: []
})

export class HomepageComponent implements OnInit {
  @ViewChild('infowindow') infowindow: ElementRef
  @ViewChild('invisibleRow') headerRef: ElementRef
  @ViewChild('topCornerElem') topRef: ElementRef
  @ViewChild(FiltersOnMapComponent) filtersOnMapComponent: FiltersOnMapComponent

  @ViewChild(InfowindowComponent) infowindowComponent: InfowindowComponent

  filters: Filters = new Filters()
  systemCars: Car[] = []
  systemPlugs: Plug[] = []
  user: User = new User()

  views = { keyboard: false }

  isInfowindowCollapsed = true
  pageHeight = window.innerHeight

  map: any
  markers: {
    favouriteLocations: {},
    favouriteStations: {},
    stations: {},
    position: { marker: any, element: any }
    user: { marker: any, element: any }
  } = {
    favouriteLocations: {},
    favouriteStations: {},
    stations: {},
    position: undefined,
    user: undefined
  }
  markerCluster: any

  constructor (
    public cd: ChangeDetectorRef,
    public router: Router,
    public _account: AccountService,
    public _echarge: EchargeService,
    public _error: ErrorService,
    private _events: Events,
    public _filter: FilterService,
    public _inAppBrowser: InAppBrowser,
    public _map: MapService,
    public _payment: PaymentService,
    private _permissions: PermissionsService,
    private _position: GeopositionService,
    private _station: StationService,
    private _storage: StorageService,
    public _translate: TranslateService
  ) { }

  get infowindowHeight (): { min: number, max: number } {
    return this.infowindowComponent && this.infowindowComponent.height
      ? this.infowindowComponent.height : { min: 0, max: 0 }
  }

  get headerHeight (): number {
    return this.headerRef.nativeElement.offsetHeight
  }

  get defaultAvatar (): string {
    if (this.user && this.user.genre) {
      return `./assets/img/default_avatars/avatar_${this.user.genre}.png`
    }
    return './assets/img/default_avatars/avatar_M.png'
  }

  get activeCharge(): ActiveTransaction {
    if (!this.infowindowComponent || !this.infowindowComponent.chargeInfowindowComponent) {
      return
    }
    if (!this.infowindowComponent.chargeInfowindowComponent.activeCharge) {
      return
    }
    return this.infowindowComponent.chargeInfowindowComponent.activeCharge
  }

  collapseInfowindow () {
    this.topRef.nativeElement.scrollIntoView({ behavior: 'smooth' })
  }

  filtersUpdate ($event) {
    if ($event.mapStyle) {
      this._map.setMapStyle(this.map, this.filters.mapStyle)
      this._events.publish('mapStyle', this.filters.mapStyle)
    }
    if ($event.filtered || $event.remoteOnly) {
      if (this.markerCluster) {
        this.markerCluster.clearMarkers()
      }
      Object.keys(this.markers.stations)
        .forEach(key => this.markers.stations[key].marker.setMap(null))
      this.markers.stations = {}
      this.requestStations()
    }
    if ($event.traffic) {
      this._map.setTrafficLayer(this.map, this.filters.traffic)
    }
    if ($event.aqi) {
      // @TODO: handle aqi insert
    }
    if ($event.charge) {
      this.views.keyboard = false
    }

    this._storage.setFilters(this.filters)
      .catch(error => this.standardErrorHandling(error))

    this.cd.detectChanges()
  }

  collapseFilters () {
    this.filtersOnMapComponent.collapseFilters()
  }

  async ngOnInit () {
    try {
      const res = await this._account.login({ email: 'a.barone@eloaded.eu', password: 'a.barone' })
      this._storage.setToken(res.data)
      this._events.subscribe('eloadedorders', () => this.openOrdersInfowindow())
      this._permissions.requestLocationPermission()
      await this.loadUser()
      await this.loadCars()
      await this.loadPlugs()

      const filters = await this._storage.getFilters()
      if (!filters) {
        await this._storage.setFilters(new Filters())
        this.filters = new Filters()
      } else {
        this.filters = filters
      }
      const listeners = {
        idle: () => this.requestStations(),
        click: () => this.collapseFilters()
      }

      this.map = await this._map.initMap(
        'homepagemap',
        {
          ...this.filters,
          style: this.filters.mapStyle,
          minZoom: 6,
          listeners
        },
        [])

      this.markerCluster = this._map.getMarkerCluster(
        this.map,
        [],
        {
          gridSize: 40,
          averageCenter: true,
          minimumClusterSize: 4,
          imageSizes: [53, 56, 65, 78, 90]
        })
      this.renderFavouriteLocationsMarkers()
      this.renderFavouriteStationsMarkers()
      if (this._position.getPosition().invalid) {
        const sub = this._events.subscribe('position:found', (position) => {
          sub.unsubscribe()
          this.setCenterOnMyPosition()
          this.createMyPositionMarker()
        })
      } else {
        this.createMyPositionMarker()
        this.setCenterOnMyPosition()
      }

      this._events.publish('loading:stop')
      this._events.publish('splashscreen:hide')
      this.cd.detectChanges()
    } catch (error) {
      console.log(error)
      this.standardErrorHandling(error)
      this._events.publish('loading:stop')
      this._events.publish('splashscreen:hide')
      this.cd.detectChanges()
    }
  }

  async createMyPositionMarker () {
    const icon = {
      ...this._map.markerIcons.positionMarker,
      scaledSize: { height: 16, width: 16 }
    }
    const options = {
      icon,
      clickable: true,
      position: this._position.getPosition(),
      listeners: {
        click: () => {
          this.setCenterOnMyPosition()
          this.infowindowComponent.type = 'cars'
        },
      },
      events: {
        'position:found': (marker, position) =>
          marker.setPosition(position),
        mapStyle: (marker, style) =>
            marker.setIcon({ ...marker.getIcon(), url: options.icon[style] })
      }
    }
    const marker = await this._map.createMarker(this.map, options)
    this.markers.user = { marker, element: this._position.getPosition() }
  }

  async getStationGuns (stationId: string) {
    try {
      const result = await this._echarge.getStationGuns(stationId)
      if (result.invalid) {
        throw new Error(result.message, errorType.BAD_REQUEST)
      }
      this.infowindowComponent.stationInfowindowComponent
        .getStationGunsSuccess(result.data)
    } catch (error) {
      this.infowindowComponent.stationInfowindowComponent
        .getStationGunsError()
      this.standardErrorHandling(error)
    }
  }

  async getChargeDetail (transactionId) {
    try {
      const result = await this._echarge.getEloadedChargeDetails(transactionId)
      if (result.invalid) {
        throw new Error(result.message, errorType.WARNING)
      }
      if (result.valid) {
        this.infowindowComponent.chargeInfowindowComponent
          .getChargeDetailsSuccess(result.data)
        if (this.activeCharge) {
          const circle = document.getElementById('mini-progress-circle')
          if (circle) {
            circle.setAttribute(
              'data-progress',
              `${this.activeCharge.soc || 0}`)
          }
        }
      }
    } catch (error) {
      console.log(error)
      // do nothing, it could fail because is in background
      // this.standardErrorHandling(error)
    }
  }

  async requestEchargeOrderPreviews (form) {
    try {
      const { evseid, targetSoC, time } = form
      if (time) {
        const results = await this._echarge
          .requestEchargeOrderPreviews(evseid, targetSoC, time)

        if (results.invalid) {
          throw new Error(results.message, errorType.BAD_REQUEST)
        }
        return this.infowindowComponent.chargeInfowindowComponent
          .requestCustomOrderPreviewSuccess(results.data)
      }
      const results = await this._echarge
        .requestEchargeOrderPreviews(evseid, targetSoC)
      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.infowindowComponent.chargeInfowindowComponent
        .requestOrderSuccess(results.data)
    } catch (error) {
      this.infowindowComponent.chargeInfowindowComponent
        .requestOrderPreviewError()
      this.standardErrorHandling(error)
    }
  }

  async payOrder (paymentData: any) {
    try {
      const { paymentReference, adyenData } = paymentData
      const result = await this._payment.payOrder(paymentReference, adyenData)
      if (result.resultCode !== AdyenResultCode.AUTHORISED) {
        throw new Error(result.resultCode, errorType.BAD_REQUEST)
      }
      this.infowindowComponent.chargeInfowindowComponent.orderPaymentSuccess()
      console.log(result)
    } catch (error) {
      this.infowindowComponent.chargeInfowindowComponent.orderPaymentError()
      this.standardErrorHandling(error)
    }
  }

  async requestStopCharge (transactionId) {
    try {
      const result = await this._echarge.stopEloadedCharge(transactionId)
    } catch (error) {
      this.standardErrorHandling(error)
    }
  }

  async payOrderWebDropIn (orderPreviewId: string) {
    try {
      const orderResponse = await this._payment.createOrder(orderPreviewId)
      if (orderResponse.invalid) {
        throw new Error(orderResponse.message, errorType.BAD_REQUEST)
      }

      this.displayWebDropIn(orderResponse.data)
    } catch (error) {
      this.infowindowComponent.chargeInfowindowComponent.orderPaymentError()
      this.standardErrorHandling(error)
    }
  }

  async payOrderWithTerminal (form: any) {
    try {
      const { orderPreview, terminal } = form

      const orderResponse = await this._payment.createOrder(orderPreview)
      if (orderResponse.invalid) {
        throw new Error(orderResponse.message, errorType.BAD_REQUEST)
      }

      const paymentResponse = await this._payment
        .payOrderWithTerminal(orderResponse.data.paymentConfig.id, terminal)

      console.log(paymentResponse)
    } catch (error) {
      this.standardErrorHandling(error)
    }
  }

  displayWebDropIn (order: Order) {
    const iap = this._inAppBrowser.create(`https://gateway.mobile-dev.eloaded.eu/order${order.paymentConfig.webDropIn}`, '_blank', 'location=no,footer=no,zoom=no,fullscreen=no')
    const token = localStorage.getItem('token')
    let closeInterval

    const sub = iap.on('loadstop').subscribe((success) => {
      sub.unsubscribe()
      iap.executeScript({
        code: `setTitle('${this._translate.instant('ADYEN.WEBDROPIN.TITLE')}')`
      })
      iap.executeScript({
        code: `setDescription('${this._translate.instant('ADYEN.WEBDROPIN.DESCRIPTION')}')`
      })

      iap.executeScript({ code: `triggerCheckout('${token}');` })

      if (closeInterval) {
        window.clearInterval(closeInterval)
        closeInterval = null
      }
      closeInterval = window.setInterval(() => {
        iap.executeScript({
            code: 'X_WINDOW_COMM'
        }).then((values: any[]) => {
          const xWindowComm = values[0]
          if (xWindowComm && xWindowComm.SHOULD_CLOSE) {
            if (!xWindowComm.RESULT) {
              // close request detected
              iap.close()
              this.infowindowComponent.chargeInfowindowComponent.orderPaymentAbort()
              return
            }
            // close request detected
            iap.close()
            // remove interval
            window.clearInterval(closeInterval)
            closeInterval = null
            console.log(xWindowComm)
            // read out the result and pass it to the provided callback function
            const result: any = {
              paymentSuccesfull: xWindowComm.RESULT.resultCode.toLowerCase() === 'authorised',
              additionalResultData: xWindowComm.RESULT
            }
            if (result.paymentSuccesfull) {
              this.getChargeDetail(this.user.activeTransaction)
              this.user.activeTransaction = order.transactionId
              this.infowindowComponent.chargeInfowindowComponent.orderPaymentSuccess()
              this.cd.detectChanges()
            } else {
              this.infowindowComponent.chargeInfowindowComponent.orderPaymentError()
            }
          }
        })
      }, 500)
    })
  }

  // Save the given car in the user's profile
  async saveCar (car: UserCar) {
    try {
      const results = car._id
        ? await this._account.patchCar(car)
        : await this._account.addCar(car)

      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.user.cars = results.data

      this.infowindowComponent.carsInfowindowComponent.saveCarSuccess()
      this.cd.detectChanges()
    } catch (error) {
      this.infowindowComponent.carsInfowindowComponent.saveCarError()
      this.standardErrorHandling(error)
    }
  }

  // Remove the given car from user's profile
  async removeCar (car: UserCar) {
    try {
      const results = await this._account.removeCar(car)
      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.user.cars = results.data
      this.infowindowComponent.carsInfowindowComponent.removeCarSuccess()
      this.cd.detectChanges()
    } catch (error) {
      this.infowindowComponent.carsInfowindowComponent.removeCarError()
      this.standardErrorHandling(error)
    }
  }

  // Set the choosen car as default
  async setDefaultCar (car: UserCar) {
    try {
      const results = await this._account.setDefaultCar(car)

      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.user.cars = results.data
      if (this.filters.filtered !== 'all') {
        if (this.markerCluster) {
          this.markerCluster.clearMarkers()
        }
        Object.keys(this.markers.stations).forEach(key =>
          this.markers.stations[key].marker.setMap(null))
        this.markers.stations = {}
        this.requestStations()
      }
      this.collapseInfowindow()
      this.cd.detectChanges()
    } catch (error) {
      this.standardErrorHandling(error)
    }
  }

  // Remove the given favourite form user's profile
  async removeFavouriteLocation (favourite: FavouriteLocation) {
    try {
      const results = await this._account
        .removeFavouriteLocation(favourite)

      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.user.favouriteLocations = results.data
      this.renderFavouriteLocationsMarkers()
      this.cd.detectChanges()
      this.infowindowComponent.favouriteInfowindowComponent.removeSuccess()
    } catch (error) {
      this.infowindowComponent.favouriteInfowindowComponent.removeError()
      this.standardErrorHandling(error)
    }
  }

  // Save the given favourite in nte user's profile
  async saveFavouriteLocation (favourite: FavouriteLocation) {
    try {
      const results = favourite._id
        ? await this._account.patchFavouriteLocation(favourite)
        : await this._account.addFavouriteLocation(favourite)

      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.user.favouriteLocations = results.data
      this.renderFavouriteLocationsMarkers()

      if (!favourite._id && this.markers.position) {
        this.markers.position.marker.setMap(null)
        delete this.markers.position
      }
      this.infowindowComponent.favouriteInfowindowComponent.saveSuccess()
      this.cd.detectChanges()
    } catch (error) {
      this.infowindowComponent.favouriteInfowindowComponent.saveError()
      this.standardErrorHandling(error)
    }
  }

  async removeFavouriteStation (station) {
    try {
      const results = await this._account.removeFavouriteStation(station)

      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.user.favouriteStations = results.data
        .map(st => ({ ...st, favourite: true }))
      this.renderFavouriteStationsMarkers()

      this.infowindowComponent.stationInfowindowComponent
        .removeFavouriteSuccess()
      this.cd.detectChanges()
    } catch (error) {
      this.infowindowComponent.stationInfowindowComponent.removeFavouriteError()
      this.standardErrorHandling(error)
    }
  }

  async saveFavouriteStation (station) {
    try {
      const results = await this._account.addFavouriteStation(station)

      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }

      this.user.favouriteStations = results.data
        .map(st => ({ ...st, favourite: true }))
      this.renderFavouriteStationsMarkers()

      this.infowindowComponent.stationInfowindowComponent.saveFavouriteSuccess()
      this.cd.detectChanges()
    } catch (error) {
      this.infowindowComponent.stationInfowindowComponent.saveFavouriteError()
      this.standardErrorHandling(error)
    }
  }

  setCenterOnMyPosition () {
    const position = this._position.getPosition()
    if (position.valid) {
      this.map.panTo(position)
    }
  }

  async requestStations () {
    try {
      const { filtered, remoteOnly } = await this._storage.getFilters()
      const { ne, sw } = this.extractCorners()
      await this.loadStationMarkers(ne, sw, filtered, remoteOnly)
    } catch (error) {
      this.standardErrorHandling(error)
    }
  }

  async loadStationMarkers (ne: Coords, sw: Coords, filtered: string, remoteOnly: boolean): Promise<void> {
    let page = 0
    let complete = false

    while (!complete) {
      const results = await this._station
        .getStations(ne, sw, filtered, remoteOnly, page)

      if (results.invalid) {
        throw new Error(results.message, errorType.WARNING)
      }
      const data = results.data
      data.data.forEach(station => this.createStationMarker(station))
      page += 1
      complete = !data.hasNext
    }
    this.markerCluster.redraw()
  }

  async createStationMarker (station) {
    const id = station._id
    if (this.markers.stations[id] || this.markers.favouriteStations[id]) {
      return false
    }

    let extra = station.fast ? `-${station.fast}` : ``
    extra += !station.remoteControl && station.remoteControl !== undefined
      ? `-not-remote` : ``
    const icon = `assets/img/pins/station/station${extra}.svg`
    const options = {
      icon: { sunny: icon, moon: icon, scaledSize: { height: 28, width: 28 } },
      position: station.coords,
      clickable: true,
      listeners: {
        click: async (marker) => this.stationMarkerClick(marker, station)
      },
      events: {
        mapStyle: (marker, style) =>
          marker.setIcon({ ...marker.getIcon(), url: options.icon[style] })
      }
    }

    const marker = await this._map.createMarker(this.map, options)

    this.markers.stations[id] = { marker, element: station }

    this.markerCluster.addMarker(this.markers.stations[id].marker)
    return true
  }

  async openOrdersInfowindow () {
    try {
      await this.loadOrders()
      this.infowindowComponent.type = 'orderHistory'
      this.cd.detectChanges()

    } catch (error) {

    }
  }

  openUserInfowindow () {
    this.infowindowComponent.type = 'profile'
    this.cd.detectChanges()
  }

  openChargeInfowindow () {
    this.infowindowComponent.type = 'charge'
    this.cd.detectChanges()
  }

  async stationMarkerClick (marker, station) {
    try {
      this.collapseFilters()
      this.infowindowComponent.type = 'station'
      this.cd.detectChanges()
      const position = this._position.getPosition()
      const results = position.valid
        ? await this._station.getStationDetailOnMap(position, station._id)
        : await this._station.getStationDetail(station._id)

      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      results.data.favourite = station.favourite

      this.map.panTo(results.data.coords)
      this.infowindowComponent.focusOnStation(results.data)
      this.infowindow.nativeElement.scrollIntoView({ behavior: 'smooth' })
      this.cd.detectChanges()
    } catch (error) {
      this.standardErrorHandling(error)
    }
  }

  setFavouriteIntoInfowindow (place?: { favourite: FavouriteLocation, setZoom: boolean }) {
    if (place && place.setZoom) {
      this.map.setZoom(17)
    }
    if (place && place.favourite) {
      this.infowindowComponent.focusOnFavourite(place.favourite)
      this.map.panTo(place.favourite.coords)
    }
    this.map.panBy(0, Math.abs(this.infowindowHeight.max - this.headerHeight) / 2)
    this.cd.detectChanges()
  }

  async setLocationIntoInfowindow (place: { location: Address, bounds: any }) {
    try {
      const { location, bounds } = place
      const icon = this._map.markerIcons.yellow

      const options = {
        icon,
        position: location.coords,
        clickable: true,
        listeners: {
          click: () => {
            this.map.setCenter(location.coords)
            this.infowindowComponent.focusOnLocation(location)
            this.cd.detectChanges()
          }
        },
        events: {
          mapStyle: (marker, style) =>
            marker.setIcon({ ...marker.getIcon(), url: options.icon[style] })
        }
      }

      if (this.markers.position) {
        this.markers.position.marker.setMap(null)
        this.markers.position = undefined
      }
      this.infowindowComponent.focusOnLocation(location)

      const marker = await this._map
        .createMarker(this.map, options)
      this.markers.position = { marker, element: location }


      this.map.fitBounds(
        bounds, {
          bottom: this.infowindowHeight.min,
          top: this.headerHeight
        })
      this.cd.detectChanges()
    } catch (error) {
      this.standardErrorHandling(error)
    }
  }

  panToAllFavourites () {
    if (this.user.favourites.length) {
      const fav = this.user.favourites
      const bounds = this._map.getBoundsFromLatLng(fav[0].coords)
      fav.forEach(({ coords }) => bounds.extend(coords))
      this.map.fitBounds(
        bounds, {
          bottom: this.infowindowHeight.min,
          top: this.headerHeight
        })
    }
  }

  logout () {
    this._events.publish('loading:start')

    this._events.subscribe('loading:error', () =>
      this._events.publish('loading:start'))

    const sub = this._events.subscribe('loading:done', () => {
      sub.unsubscribe()
      this._storage.deleteToken()
        .catch(() => this.router.navigate(['/login']))
      this._storage.getLoginInfo()
        .then((loginInfo: any) => {
          loginInfo.password = null
          loginInfo.rememberMe = false
          return this._storage.setLoginInfo(loginInfo)
        })
        .then(() => this.router.navigate(['/login']))
        .catch(() => this.router.navigate(['/login']))
    })
  }

  async updateUser (user: any) {
    try {
      user.avatar = user.avatar && UtilityService
        .base64ToBlob(user.avatar, 'image/jpeg')

      const results = await this._account.updateProfile(user)
      if (results.invalid)  {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      if (results.data.locale !== this.user.locale) {
        this._translate.use(results.data.locale)
      }
      // @TODO: check after DEMO
      if (this.user.orders.length) {
        results.data.orders = this.user.orders
      }
      if (this.user.transactions.length) {
        results.data.transactions = this.user.transactions
      }
      this.user = results.data
      this.infowindowComponent.profileInfowindowComponent.saveProfileSuccess()
      this.cd.detectChanges()
    } catch (error) {
      this.infowindowComponent.profileInfowindowComponent
        .saveProfileError(this._translate.instant(error.message))
      this.standardErrorHandling(error)
    }
  }

  async changePassword (passwords: any) {
    try {
      const { oldPassword, newPassword } = passwords
      const results = await this._account
        .changePassword(oldPassword, newPassword)

      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.infowindowComponent.profileInfowindowComponent
        .changePasswordSuccess()
    } catch (error) {
      this.infowindowComponent.profileInfowindowComponent
        .changePasswordError(this._translate.instant(error.message))
      this.standardErrorHandling(error)
    }
  }

  async confirmProfile (form: any) {
    try {
      const results = await this._account.confirmProfile(form.confirmationCode)
      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.user.authn.confirmation.status = true
      this.infowindowComponent
        .profileInfowindowComponent.confirmationCodeSuccess()
    } catch (error) {
      this.infowindowComponent.profileInfowindowComponent
        .confirmationCodeError(this._translate.instant(error.message))
      this.standardErrorHandling(error)
    }
  }

  async newConfirmationCode () {
    try {
      const results = await this._account.resendConfirmCode()
      if (results.invalid) {
        throw new Error(results.message, errorType.BAD_REQUEST)
      }
      this.infowindowComponent
        .profileInfowindowComponent.sendConfirmationMailSuccess()
    } catch (error) {
      this.infowindowComponent.profileInfowindowComponent
        .confirmationCodeError(this._translate.instant(error.message))
      this.standardErrorHandling(error)
    }
  }

  async loadUser () {
    const accountResult = await this._account.getProfile()
    if (accountResult.invalid) {
      throw new Error(accountResult.message, errorType.BAD_REQUEST)
    }
    this.user = accountResult.data
    this.cd.detectChanges()
  }

  async loadCars () {
    const results = await this._filter.getCars()
    if (results.invalid) {
      throw new Error(results.message, errorType.BAD_REQUEST)
    }
    this.systemCars = results.data
    this.cd.detectChanges()
  }

  async loadOrders () {
    const orderResult = await this._payment.getOrderHistory()
    if (orderResult.invalid) {
      throw new Error(orderResult.message, errorType.BAD_REQUEST)
    }
    if (this.user) {
      this.user.orders = orderResult.data.sort((a, b) =>
        b.creationDate.getTime() - a.creationDate.getTime())
    }
    this.cd.detectChanges()
  }

  async loadPlugs () {
    const results = await this._filter.getPlugs()
    if (results.invalid) {
      throw new Error(results.message, errorType.BAD_REQUEST)
    }
    this.systemPlugs = results.data
    this.cd.detectChanges()
  }

  async renderFavouriteStationsMarkers () {
    const stations = this.user.favouriteStations

    for (const station of stations) {
      if (this.markers.favouriteStations[station._id]) {
        continue
      }

      let extra = station.fast ? `-${station.fast}` : ``
      extra += !station.remoteControl && station.remoteControl !== undefined
        ? `-not-remote` : ``
      const icon = `assets/img/pins/favourite/favourite-station.svg`

      const options = {
        icon: { sunny: icon, moon: icon, scaledSize: { height: 28, width: 28 } },
        position: station.coords,
        clickable: true,
        listeners: {
          click: async (marker) => this.stationMarkerClick(marker, station)
        },
        events: {
          mapStyle: (marker, style) =>
            marker.setIcon({ ...marker.getIcon(), url: options.icon[style] })
          // mapZoom: (marker, )
        }
      }

      if (this.markers.stations[station._id]) {
        this.markerCluster
          .removeMarker(this.markers.stations[station._id].marker)
        this.markers.stations[station._id].marker.setMap(null)
        delete this.markers.stations[station._id]
      }
      const marker = await this._map.createMarker(this.map, options)

      this.markers.favouriteStations[station._id] = { marker, element: station }

      this.markerCluster
        .addMarker(this.markers.favouriteStations[station._id].marker)
    }

    const keys = Object.keys(this.markers.favouriteStations)
    const usfs = this.user.favouriteStations.map(({ _id }) => _id)
    const keysLeft = keys.filter(k => usfs.indexOf(k) === -1)
    for (const key of keysLeft) {
      this.markerCluster
        .removeMarker(this.markers.favouriteStations[key].marker)
      this.markers.favouriteStations[key].marker.setMap(null)
      delete this.markers.favouriteStations[key]
    }
  }

  async renderFavouriteLocationsMarkers () {
    const icon = {
      ...this._map.markerIcons.favouriteLocations,
      scaledSize: { height: 28, width: 28 }
    }
    if (!this.user || !this.user.favouriteLocations) { return }
    for (const location of this.user.favouriteLocations) {
      const exists = this.markers.favouriteLocations[location._id]
      if (exists && location.isEqual(exists.element as FavouriteLocation)) {
        continue
      }

      const options = {
        icon,
        position: location.coords,
        clickable: true,
        listeners: {
          click: (marker) => this.favouriteMarkerClick(marker, location)
        },
        events: {
          mapStyle: (marker, style) =>
            marker.setIcon({ ...marker.getIcon(), url: options.icon[style] })
        }
      }
      const marker = await this._map.createMarker(this.map, options)
      this.markers.favouriteLocations[location._id] = {
        element: location,
        marker
      }
    }
    const keys = Object.keys(this.markers.favouriteLocations)
    const usfl = this.user.favouriteLocations.map(({ _id }) => _id)
    const keysLeft = keys.filter(k => usfl.indexOf(k) === -1)
    for (const key of keysLeft) {
      this.markers.favouriteLocations[key].marker.setMap(null)
      delete this.markers.favouriteLocations[key]
    }
  }

  favouriteMarkerClick(marker, favourite) {
    this.setFavouriteIntoInfowindow({ favourite, setZoom: false })
  }

  extractCorners (): Bounds {
    const b = this.map.getBounds()
    const n = b.getNorthEast()
    const s = b.getSouthWest()
    return new Bounds({
      ne: { lat: n.lat(), lng: n.lng() }, sw: { lat: s.lat(), lng: s.lng() }
    })
  }

  closeInfowindow () {
    if (this.markers.position) {
      this.markers.position.marker.setMap(null)
      this.markers.position = undefined
    }
    this.collapseInfowindow()
    this.cd.detectChanges()
  }

  toggleKeyboard () {
    this.views.keyboard = !this.views.keyboard
    this.cd.detectChanges()
  }

  // Handle all errors with a common strategy
  standardErrorHandling (error: Error) {
    console.log(`ERROR: ${error.status}, ${error.message}`)
    if (error.status === errorType.KO) {
      this._error.showError(error)
      // this.navCtrl.pop() ??
    } else if (error.status === errorType.WARNING) {
      console.log(`ERROR: ${error.status}, ${error.message}`)
    } else {
      this._error.showError(error)
    }
  }
}
