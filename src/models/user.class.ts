import { UserCar } from './car.class'
import { Address, FavouriteLocation } from './address.class'
import { Station } from './station.class'
import { Order } from './order.class'
import { EloadedTransaction } from './eloadedTransaction.class'

class Authn {
  _id: string
  email: string
  confirmation: { status: boolean }

  constructor(obj: any) {
    if (obj._id) {
      this._id = obj._id
    }
    if (obj.email) {
      this.email = obj.email
    }
    if (obj.confirmation && obj.confirmation.status !== undefined) {
      this.confirmation = { status: obj.confirmation.status }
    }
  }
}

export class User {
  _id: string
  firstname: string
  lastname: string
  birthday: Date
  avatar: string
  genre: string
  locale = 'en'
  newsletter: boolean
  cars: UserCar[] = []
  authn: Authn
  activeTransaction: string
  orders: Order[] = []
  transactions: EloadedTransaction[] = []
  favouriteLocations: FavouriteLocation[] = []
  favouriteStations: Array<Station> = []
  addresses: FavouriteLocation[] = []
  address: Address

  constructor (obj: any = {}) {
    const properties = ['_id', 'email', 'firstname', 'lastname',
      'avatar', 'genre', 'locale', 'newsletter', 'activeTransaction']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }

    if (obj.birthday) {
      this.birthday = new Date(obj.birthday)
    }

    if (obj.cars && Array.isArray(obj.cars)) {
      this.cars = obj.cars.map(c => new UserCar(c))
    } else {
      this.cars = []
    }

    if (obj.authn) {
      this.authn = new Authn(obj.authn)
    }

    if (obj.address) {
      this.address = new Address(obj.address)
    }

    if (obj.addresses && Array.isArray(obj.addresses)) {
      this.addresses = obj.addresses.map(a => new FavouriteLocation(a))
    }

    if (obj.favouriteLocations && Array.isArray(obj.favouriteLocations)) {
      this.favouriteLocations = obj.favouriteLocations
        .map((a: any) => new FavouriteLocation(a))
    }

    if (obj.favouriteStations && Array.isArray(obj.favouriteStations)) {
      this.favouriteStations = obj.favouriteStations
        .map(fs => new Station({ ...fs, favourite: true }))
    }
    if (obj.orders && Array.isArray(obj.orders)) {
      this.orders = obj.orders.map(o => new Order(o))
    }
    if (obj.transactions && Array.isArray(obj.transactions)) {
      this.transactions = obj.transactions.map(t => new EloadedTransaction(t))
    }
  }

  get defaultCar (): UserCar {
    return this.cars.find(car => car.default)
  }

  set defaultCar (car: UserCar) {
    this.cars.forEach(c => c.default = c._id === car._id)
  }

  get scrAvatar (): String {
    return this.avatar ? `data:image/jpeg;base64,${this.avatar}` : undefined
  }

  get favourites(): FavouriteLocation[] {
    return this.favouriteLocations || []
  }

  get car(): UserCar {
    return this.cars.find(car => car.default)
  }

  get email(): string {
    return this.authn ? this.authn.email : undefined
  }

  logged (): boolean {
    return !!this.authn?._id
  }
}
