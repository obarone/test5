import { ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core'

@Component({
  selector: 'keypad',
  templateUrl: 'keypad.component.html'
})

export class KeypadComponent {

  @Input() value: string
  @Output() valueChange: EventEmitter<String> = new EventEmitter<String>()
  @Input() min: number
  @Input() max: number

  @Output() confirm = new EventEmitter<any>()

  firstInput = true

  constructor (private cd: ChangeDetectorRef) {}

  reset () {
    this.firstInput = true
  }

  keyboardDelete() {
    this.value = `${this.value}`.slice(0, this.value.length - 1)
    if (this.value === '') {
      this.value = '0'
    }
    this.valueChange.emit(this.value)
    this.cd.detectChanges()
  }

  keyboardAdd (n: string) {
    if (this.firstInput) {
      this.firstInput = false
      this.value = (n === '.') ? '0' : ''
    }
    if (parseFloat(`${this.value}${n}`) > 100 || parseFloat(`${this.value}${n}`) < 0) {
      return
    }
    if (this.value.includes('.') && this.value.split('.')[1].length >= 1) {
      return
    }
    this.value = `${parseFloat(`${this.value}${n}`)}`
    if (n === '.' && !this.value.includes('.')) {
      this.value = `${this.value}${n}`
    }
    this.valueChange.emit(this.value)
    this.cd.detectChanges()
  }

  valid () {
    return parseFloat(this.value) < this.max || parseFloat(this.value) > this.min
  }

  save () {
    if (!this.valid()) { return }
    this.firstInput = false
    this.valueChange.emit(this.value)
    this.reset()
    this.cd.detectChanges()
    return this.confirm.emit(this.value)
  }
}
