﻿import { Observable, of } from 'rxjs'

import { LogEntry } from './log-entry'

export abstract class LogPublisher {
  location: string

  abstract log (record: LogEntry): Observable<any>
  abstract clear (): Observable<any>
}

export class LogConsole extends LogPublisher {
  log (entry: LogEntry): Observable<any> {
    console.log(entry.buildLogString())

    return of()
  }

  clear (): Observable<any> {
    console.clear()

    return of()
  }
}
