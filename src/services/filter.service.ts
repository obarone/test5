import { Injectable } from '@angular/core'

import { ApiService } from './api.service'

import { Car, Plug, Response } from '../models/eloaded-core.models'

@Injectable()
export class FilterService {

  getPlugs = (): Promise<Response<Plug[]>> => this._api
    .getAllPlugs()
    .toPromise<Response<Plug[]>>()

  getCars = (): Promise<Response<Car[]>> => this._api
    .getAllCars()
    .toPromise<Response<Car[]>>()

  constructor (public _api: ApiService) {}
}
