import { Amount } from './amount.class'

export class OrderProduct {
  product: string
  quantity: number
  price: Amount
  details: OrderProductDetails

  constructor (obj: any = {}) {
    if (obj.product) {
      this.product = obj.product
    }
    if (obj.quantity) {
      this.quantity = obj.quantity
    }
    if (obj.price) {
      this.price = new Amount(obj.price)
    }
    if (obj.details) {
      this.details = new OrderProductDetails(obj.details)
    }
    console.log(this)
  }

  equals (b: OrderProduct): boolean {
    return this.product === b.product &&
      this.price.equals(b.price) &&
      this.quantity === b.quantity
  }

  get description (): string {
    if (this.details.current) {
      return this.price.toString() + ' per Kw'
    } else {
      return 'Fixed price'
    }
  }
}

export class OrderProductDetails {
  type: string
  transaction: string
  current: number
  time: number
  targetSoC: number

  constructor (obj: any = {}) {
    const properties = ['type', 'transaction', 'current', 'time', 'targetSoC']

    for (const property in obj) {
      if (properties.find((p) => p === property)) {
        this[property] = obj[property]
      }
    }
  }
}
