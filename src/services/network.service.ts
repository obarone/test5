import { Injectable, OnDestroy } from '@angular/core'
import { Network } from '@ionic-native/network/ngx'
import { Subscription } from 'rxjs'
import { LogService } from './log.service'

declare var navigator: any
declare var Connection: any

@Injectable()
export class NetworkService implements OnDestroy {
  connected: boolean
  con: any
  dis: any
  public subscriptions: Subscription [] = []
  constructor (public _logger: LogService, private network: Network) {
    this.onReconnect()
    this.onDisconnect()
    this.checkConnection()
  }

  checkConnection () {
    this._logger.info('call checkConnection() - NetworkService')

    if (navigator.connection.type) {
      this.connected = (navigator.connection.type !== Connection.NONE)
        ? true
        : false
    } else if (navigator.onLine) {
      this.connected = true
    } else {
      this.connected = false
    }
  }

  stopMonitor () {
    this._logger.info('call stopMonitor() - NetworkService')
    this.con.unsubscribe()
    this.dis.unsubscribe()
  }

  onReconnect (next?) {
    this._logger.info('call onReconnect() - NetworkService')
    return this.con = this.network.onConnect().subscribe(() => {
      this.connected = true
      if (next) {
        next()
      }
    })
  }

  onDisconnect (next?) {
    this._logger.info('call onDisconnect() - NetworkService')
    return this.dis = this.network.onDisconnect().subscribe(() => {
      this.connected = false
      if (next) {
        next()
      }
    })
  }

  isConnected () {
    this._logger.info('call isConnected() - NetworkService')
    if (!this.connected) {
      this.checkConnection()
    }
    return this.connected
  }

  ngOnDestroy () {
    this.stopMonitor()
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }

}
