import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RouteReuseStrategy } from '@angular/router'

import { IonicModule, IonicRouteStrategy } from '@ionic/angular'

import { Diagnostic } from '@ionic-native/diagnostic/ngx'
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx'
import { Network } from '@ionic-native/network/ngx'
import { Storage } from '@ionic/storage'
import { IonicStorageModule } from '@ionic/storage-angular'
import { Geolocation } from '@ionic-native/geolocation/ngx'

import { AppComponent } from './app.component'
import { AppRoutingModule } from './app-routing.module'

import { SideMenuContentComponent } from 'src/oldcomponents/side-menu-content/side-menu-content.component'
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { TranslateLoader, TranslateModule } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { AccountService } from 'src/services/account.service'
import { AlertService } from 'src/services/alert.service'
import { ApiService } from 'src/services/api.service'
import { ConnectorService } from 'src/services/connector.service'
import { EchargeService } from 'src/services/echarge.service'
import { ErrorService } from 'src/services/error.service'
import { Events } from 'src/services/events.service'
import { FilterService } from 'src/services/filter.service'
import { GeopositionService } from 'src/services/geoposition.service'
import { LoaderService } from 'src/services/loader.service'
import { LogPublishersService } from 'src/services/log-publishers.service'
import { LogService } from 'src/services/log.service'
import { MapService } from 'src/services/map.service'
import { NetworkService } from 'src/services/network.service'
import { PaymentService } from 'src/services/payment.service'
import { PermissionsService } from 'src/services/permissions.service'
import { SplashscreenService } from 'src/services/splashscreen.service'
import { StationService } from 'src/services/station.service'
import { StorageService } from 'src/services/storage.service'
import { ToastService } from 'src/services/toast.service'
import { UtilityService } from 'src/services/utility.service'
import { InfowindowComponent } from 'src/oldcomponents/infowindow/infowindow.component'
import { InfowindowTabsHeaderComponent } from 'src/oldcomponents/infowindow/infowindow-tabs-header/infowindow-tabs-header.component'
import { CarsInfowindowComponent } from 'src/oldcomponents/infowindow/cars-infowindow/cars-infowindow.component'
import { LocationInfowindowComponent } from 'src/oldcomponents/infowindow/location-infowindow/location-infowindow.component'
import { ChargeInfowindowComponent } from 'src/oldcomponents/infowindow/charge-infowindow/charge-infowindow.component'
import { FavouritesInfowindowComponent } from 'src/oldcomponents/infowindow/favourites-infowindow/favourites-infowindow.component'
import { OrderHistoryInfowindowComponent } from 'src/oldcomponents/infowindow/orderhistory-infowindow/orderhistory-infowindow.component'
import { ProfileInfowindowComponent } from 'src/oldcomponents/infowindow/profile-infowindow/profile-infowindow.component'
import { StationInfowindowComponent } from 'src/oldcomponents/infowindow/station-infowindow/station-infowindow.component'
import { FiltersOnMapComponent } from 'src/oldcomponents/filters-on-map/filters-on-map.component'
import { KeypadComponent } from 'src/oldcomponents/keypad/keypad.component'
import { JwtInterceptor } from 'src/helpers/jwt.interceptor'
import { HomePage } from './home/home.page'
import { SearchbarComponent } from 'src/app/components/searchbar/searchbar.component'
import { FormsModule } from '@angular/forms'

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HomePage,
    SearchbarComponent,
    SideMenuContentComponent,
    InfowindowTabsHeaderComponent,
    InfowindowComponent,
    CarsInfowindowComponent,
    ChargeInfowindowComponent,
    FavouritesInfowindowComponent,
    LocationInfowindowComponent,
    OrderHistoryInfowindowComponent,
    ProfileInfowindowComponent,
    StationInfowindowComponent,
    FiltersOnMapComponent,
    KeypadComponent
  ],
  entryComponents: [],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    AccountService,
    AlertService,
    ApiService,
    ConnectorService,
    EchargeService,
    ErrorService,
    Events,
    FilterService,
    GeopositionService,
    LoaderService,
    LogPublishersService,
    LogService,
    MapService,
    NetworkService,
    PaymentService,
    PermissionsService,
    SplashscreenService,
    StationService,
    StorageService,
    ToastService,
    UtilityService,
    Diagnostic,
    Geolocation,
    InAppBrowser,
    Network,
    Storage
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
