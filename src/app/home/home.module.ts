import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { IonicModule } from '@ionic/angular'

import { HomePageRoutingModule } from './home-routing.module'

import { HomePage } from './home.page'
import { TranslateService } from '@ngx-translate/core'
import { KeypadComponent } from 'src/oldcomponents/keypad/keypad.component'
import { BrowserModule } from '@angular/platform-browser'
import { SearchbarComponent } from '../components/searchbar/searchbar.component'
import { FiltersOnMapComponent } from 'src/oldcomponents/filters-on-map/filters-on-map.component'

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    TranslateService,
    KeypadComponent,
    SearchbarComponent,
    FiltersOnMapComponent
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
